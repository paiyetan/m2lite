# Introduction
M2Lite is a lightweight, easily pluggable, and extremely fast conversion software tool. M2Lite converts msf files to the proteomics community standard mzIdentML file format

# Configuration

### SYSTEM REQUIREMENT ###

- Java 6 or above. You may download the latest version from http://www.java.com/en/download/index.jsp
- R 2.15.2 or above. You may download the latest version from http://www.r-project.org/ 
- 2GB RAM available memory for program to run

See project wiki https://bitbucket.org/paiyetan/m2lite/wiki for details 

Also, please cite the paper **"M2Lite: an Open-Source, Light-Weight, Pluggable and Fast Proteome Discoverer MSF to mzIdentML Tool"** http://www.bowenpublishing.com/jbi/paperInfo.aspx?PaperID=15750 if you found M2Lite useful.