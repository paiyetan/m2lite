/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfparser;

import ios.readers.MSFRoutNonTermModFileReader;
import ios.readers.MSFRoutTermModFileReader;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import msfpsms.MSFPSMModification;
import rconnect.RCaller;

/**
 *
 * @author paiyeta1
 */
public class MSFModificationsExtractor {

    private HashMap<Integer,LinkedList<MSFPSMModification>> peptideID2ModificationsMap;
    private HashMap<Integer,LinkedList<MSFPSMModification>> peptideID2TermModificationMap;


    public MSFModificationsExtractor(String msfFile,String tmp, String rQueryScriptLibrary) {
        
        peptideID2ModificationsMap = new HashMap<Integer,LinkedList<MSFPSMModification>>();
        peptideID2TermModificationMap = new HashMap<Integer,LinkedList<MSFPSMModification>>();
        
        // *** instantiate a sqlite db connection ***
        System.out.println("\tQuerying file for Non-Terminal Modification results' properties..."); 
        String rFilePath = rQueryScriptLibrary + File.separator + "MSFPSMNonTermModifxnsDBQuery.R";
        RCaller rcaller = new RCaller(); 
        // Get modifications
        //make output file
        String msfRoutNonTermModifications = tmp + File.separator + "msfRoutNonTermModifications";
        //make query - delegated to R script...
        //make command
        String rcmd = "Rscript " + rFilePath + " " + msfFile + " "  + msfRoutNonTermModifications;
        //execute R
        rcaller.execute(rcmd);
        //read back
        System.out.println("\tRetrieving Non-Terminal Modification results' properties query-returned results...");    
        MSFRoutNonTermModFileReader reader = new MSFRoutNonTermModFileReader();
        peptideID2ModificationsMap = reader.read(msfRoutNonTermModifications);
        
        //new File(msfRoutNonTermModifications).delete();
        // *************************************************************************** //
        // retrieve terminal modifications attributes/properties
        System.out.println("\tQuerying file for Terminal Modification results' properties...");    
        //make output file
        String msfRoutTermModifications = tmp + File.separator + "msfRoutTermModifications";
        //make query - delegated to R...
        //make command
        rFilePath =  rQueryScriptLibrary + File.separator + "MSFPSMTermModifxnsDBQuery.R";
        rcmd = "Rscript " + rFilePath + " " + msfFile + " " + msfRoutTermModifications;
        //execute R
        rcaller.execute(rcmd);
        //read back
        System.out.println("\tRetrieving Terminal Modification results' properties query-returned results...");    
        MSFRoutTermModFileReader reader2 = new MSFRoutTermModFileReader();
        peptideID2TermModificationMap = reader2.read(msfRoutTermModifications);  
        
        //new File(msfRoutTermModifications).delete();
    }

    public HashMap<Integer, LinkedList<MSFPSMModification>> getPeptideID2ModificationsMap() {
        return peptideID2ModificationsMap;
    }

    public HashMap<Integer, LinkedList<MSFPSMModification>> getPeptideID2TermModificationMap() {
        return peptideID2TermModificationMap;
    }
    
    
    
}
