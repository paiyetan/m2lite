/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfparser;

import java.sql.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfproteins.MSFPSMProteinsExtractor;

/**
 *
 * @author paiyeta1
 */
class MSFModificationTermsExtractor {

    public HashMap<String, String> extractModfxnTerms(String msfFile) {
        //throw new UnsupportedOperationException("Not yet implemented");
        HashMap<String,String> modTermMap = new HashMap<String,String>();
        Connection connection;  
        try {  
            Class.forName("org.sqlite.JDBC"); //Deprecated in recent JDBC implementation
            connection = DriverManager.getConnection("jdbc:sqlite:" + msfFile);  
            Statement statement = connection.createStatement();  
            System.out.println("\tQuerying file for Modification terms...");    
            String query = "SELECT * FROM AminoAcidModifications;";
            ResultSet resultSet = statement.executeQuery(query); 
            System.out.println("\tRetrieving modification terms returned..."); 
            int noOfResultsRetrieved = 0;
            while(resultSet.next()){
                noOfResultsRetrieved++;
                String modName = resultSet.getString("ModificationName");
                String uniModAcc = resultSet.getString("UnimodAccession");
                modTermMap.put(modName, uniModAcc);               
            }
            System.out.println("\t" + noOfResultsRetrieved + " modification entries found.");
            statement.close();
            resultSet.close();
            connection.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MSFPSMProteinsExtractor.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(MSFPSMProteinsExtractor.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }               
        return modTermMap;        
    }
    
}
