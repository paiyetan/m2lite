/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *        Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * 
 * 
 */
package msfparser;

import database.Peptide;
import enumtypes.OutputFormatType;
import enumtypes.OutputObjectType;
import enumtypes.SearchDatabaseType;
import ios.printers.MSF2AllMZIdStAXPrinter;
import ios.printers.MSF2PepXMLPrinter;
import ios.printers.MSF2TextFilePrinter;
import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import msfproteins.MSFPSMDecoyProteinsExtractor;
import msfproteins.MSFPSMProtein;
import msfproteins.MSFPSMProteinsExtractor;
import msfpsms.MSFPSMDecoyPeptidesExtractor;
import msfpsms.MSFPSMPeptide;
import msfpsms.MSFPSMPeptidesExtractor;
import utilities.MultiFileChooser;

/**
 *
 * @author paiyeta1
 */
public class M2Lite {

    
    private boolean done = false;
    
    public void parse(String[] msfFiles, OutputFormatType outputFormat, OutputObjectType outputObjs,
                      SearchDatabaseType dbtype, 
                      String searchDBPath,
                      String rQueryScriptLibrary,
                      boolean includeiTRAQ4plexValues,
                      boolean includeProteinsInTextOutput,
                      boolean tppCompatibleOutput) throws FileNotFoundException, IOException{
        
        System.out.println(msfFiles.length + " input file(s) selected");
        System.out.println("File names are: ");
        for(int i = 0; i < msfFiles.length; i++)
            System.out.println("\t" + i + ". " + new File(msfFiles[i]).getName());
        
        Date start_time = new Date();
        long start = start_time.getTime();
        
        //Database db = new Database(searchDBPath);
                
        switch(outputFormat){
            case MZIDENTML:
                for(int i = 0; i < msfFiles.length; i++){            
                    System.out.println("Parsing File " + i + ": " + new File(msfFiles[i]).getName());
                    //set output mzid filename
                    String mzidMLOutFile = msfFiles[i].replaceAll("\\.msf","\\.mzid");
                    
                    MSFPSMPeptidesExtractor pepsExtractor;
                    MSFPSMDecoyPeptidesExtractor decoyPepsExtractor;
                    
                    MSFPSMProteinsExtractor protsExtractor;
                    MSFPSMDecoyProteinsExtractor decoyPSMProtsExtractor;
                    
                    MSFPSMPeptide[] msfPeps;
                    MSFPSMPeptide[] decoyMSFPeps;
                    
                    MSFPSMProtein[] msfProts;
                    MSFPSMProtein[] decoyMSFProts;
                    
                    MSFSetUpParamsExtractor paramsExtractor;
                    MSFModificationTermsExtractor termsExtractor;
                    
                    HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap;
                    
                    HashMap<String,String> parametersMap;
                    HashMap<String,String> modTermMap;
                    HashMap<Integer,LinkedList<Integer>> spectID2PepIDsMap;
                    
                    HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap;
                    
                    switch(outputObjs){
                        //parses all peptides
                        default: // defaults to all PEPTIDES "real" and "decoys" only (i.e. no protein inference information is outputted in .mzid file
                            
                            pepID2PeptideInstancesMap = new HashMap<Integer,LinkedList<Peptide>>();
                            pepsExtractor = new MSFPSMPeptidesExtractor(msfFiles[i], false, rQueryScriptLibrary);
                            msfPeps = pepsExtractor.getMsfPeps();
                            
                            decoyPepsExtractor = new MSFPSMDecoyPeptidesExtractor(msfFiles[i], rQueryScriptLibrary);
                            decoyMSFPeps = decoyPepsExtractor.getDecoyMSFPeps();
                            
                            pepID2MSFPeptideMap = mapPeptideID2MSFPeptide(msfPeps,decoyMSFPeps);
                            
                            protsExtractor = new MSFPSMProteinsExtractor(msfFiles[i], dbtype, rQueryScriptLibrary);
                            msfProts = protsExtractor.getProteins();
                            pepID2PeptideInstancesMap = updateInstancesMap(msfProts,
                                                            pepID2PeptideInstancesMap,pepID2MSFPeptideMap);
                            
                            decoyPSMProtsExtractor = new MSFPSMDecoyProteinsExtractor(msfFiles[i], dbtype, rQueryScriptLibrary);
                            decoyMSFProts = decoyPSMProtsExtractor.getDecoyPSMProteins();
                            pepID2PeptideInstancesMap = updateInstancesMap(decoyMSFProts,
                                                            pepID2PeptideInstancesMap,pepID2MSFPeptideMap);
                            
                            paramsExtractor = new MSFSetUpParamsExtractor();
                            parametersMap = paramsExtractor.extractParams(msfFiles[i]);
                           
                            termsExtractor = new MSFModificationTermsExtractor();
                            modTermMap = termsExtractor.extractModfxnTerms(msfFiles[i]);
                            
                            spectID2PepIDsMap = mapSpectra2PeptideIDs(msfPeps,decoyMSFPeps);
                            //MSF2AllMZIdentMLPrinter allMzidMLPrinter = new MSF2AllMZIdentMLPrinter();
                            MSF2AllMZIdStAXPrinter allMzidStAXPrinter = new MSF2AllMZIdStAXPrinter();
                            
                            allMzidStAXPrinter.write(pepID2MSFPeptideMap,
                                                   parametersMap, 
                                                   modTermMap,
                                                   spectID2PepIDsMap,
                                                   mzidMLOutFile, //mzidMLOutFile
                                                   //db, 
                                                   searchDBPath, 
                                                   dbtype, 
                                                   true,
                                                   pepID2PeptideInstancesMap);                            
                            break;                        
                    }


                }
                break;
                
            case PEPXML: 
                for(int i = 0; i < msfFiles.length; i++){            
                    System.out.println("Parsing File " + i + ": " + new File(msfFiles[i]).getName());
                    //set output mzid filename
                    String pepXMLOutFile;
                    if(tppCompatibleOutput)
                        pepXMLOutFile = msfFiles[i].replaceAll("\\.msf","\\.pep.xml");
                    else 
                        pepXMLOutFile = msfFiles[i].replaceAll("\\.msf","\\.pepXML");
                    
                    MSFPSMPeptidesExtractor pepsExtractor;
                    MSFPSMDecoyPeptidesExtractor decoyPepsExtractor;
                    
                    MSFPSMProteinsExtractor protsExtractor;
                    MSFPSMDecoyProteinsExtractor decoyPSMProtsExtractor;
                    
                    MSFPSMPeptide[] msfPeps;
                    MSFPSMPeptide[] decoyMSFPeps;
                    
                    MSFPSMProtein[] msfProts;
                    MSFPSMProtein[] decoyMSFProts;
                    
                    MSFSetUpParamsExtractor paramsExtractor;
                    MSFModificationTermsExtractor termsExtractor;
                    
                    HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap;
                    //HashMap<Integer,String> pepID2SeqMap;
                    
                    HashMap<String,String> parametersMap;
                    HashMap<String,String> modTermMap;
                    HashMap<Integer,LinkedList<Integer>> spectID2PepIDsMap;
                    
                    MSF2PepXMLPrinter pepXMLPrinter;
                    
                    HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap;
                    
                    
                    switch(outputObjs){
                        
                        default: // defaults to all PEPTIDES "real" and "decoys" only (i.e. no protein inference information is outputted in .pepXML file 
                            
                            pepID2PeptideInstancesMap = new HashMap<Integer,LinkedList<Peptide>>();
                            pepsExtractor = new MSFPSMPeptidesExtractor(msfFiles[i],false, rQueryScriptLibrary);
                            msfPeps = pepsExtractor.getMsfPeps();
                            
                            decoyPepsExtractor = new MSFPSMDecoyPeptidesExtractor(msfFiles[i], rQueryScriptLibrary);
                            decoyMSFPeps = decoyPepsExtractor.getDecoyMSFPeps();
                            
                            pepID2MSFPeptideMap = mapPeptideID2MSFPeptide(msfPeps,decoyMSFPeps);
                            
                            protsExtractor = new MSFPSMProteinsExtractor(msfFiles[i], dbtype, rQueryScriptLibrary);
                            msfProts = protsExtractor.getProteins();
                            pepID2PeptideInstancesMap = updateInstancesMap(msfProts,
                                                            pepID2PeptideInstancesMap,pepID2MSFPeptideMap);
                            
                            decoyPSMProtsExtractor = new MSFPSMDecoyProteinsExtractor(msfFiles[i], dbtype, rQueryScriptLibrary);
                            decoyMSFProts = decoyPSMProtsExtractor.getDecoyPSMProteins();
                            pepID2PeptideInstancesMap = updateInstancesMap(decoyMSFProts,
                                                            pepID2PeptideInstancesMap,pepID2MSFPeptideMap);
                            
                            paramsExtractor = new MSFSetUpParamsExtractor();
                            parametersMap = paramsExtractor.extractParams(msfFiles[i]);
                           
                            termsExtractor = new MSFModificationTermsExtractor();
                            modTermMap = termsExtractor.extractModfxnTerms(msfFiles[i]);
                            
                            spectID2PepIDsMap = mapSpectra2PeptideIDs(msfPeps,decoyMSFPeps);
                                                
                            pepXMLPrinter = new MSF2PepXMLPrinter();
                            pepXMLPrinter.write(pepID2MSFPeptideMap,
                                                   parametersMap, 
                                                   modTermMap,
                                                   spectID2PepIDsMap,
                                                   pepXMLOutFile,
                                                   //db, 
                                                   searchDBPath, 
                                                   dbtype, 
                                                   true,
                                                   pepID2PeptideInstancesMap,
                                                   tppCompatibleOutput);
                            
                            break;

                        
                    }


                }
                break;
                               
            default: // a tab_delimited txt output
                //System.out.println("\nParsing... ");
                for(int i = 0; i < msfFiles.length; i++){            
                    System.out.println("Parsing File " + i + ": " + new File(msfFiles[i]).getName());
                    MSF2TextFilePrinter printer = new MSF2TextFilePrinter();

                    switch(outputObjs){
                        //parses only specified information
                        default: //extracts or parses only the peptide with quantification information alone
                            MSFPSMPeptidesExtractor pepsExtractor = new MSFPSMPeptidesExtractor(msfFiles[i], includeiTRAQ4plexValues, rQueryScriptLibrary);
                            MSFPSMPeptide[] msfPeps = pepsExtractor.getMsfPeps();
                            
                            MSFPSMProteinsExtractor protsExtractor;
                            MSFPSMProtein[] msfProts;
                            HashMap<Integer,LinkedList<MSFPSMProtein>> peptideIDToProteinsMap = null;
                            
                            if(includeProteinsInTextOutput){
                                protsExtractor = new MSFPSMProteinsExtractor(msfFiles[i], dbtype, rQueryScriptLibrary);
                                msfProts = protsExtractor.getProteins(); 
                                peptideIDToProteinsMap = protsExtractor.getPeptideID2ProteinsMap();
                                        //new HashMap<String,LinkedList<MSFPSMProtein>>();
                                
                            }
                            // Print to File
                            System.out.println("\tPrinting ouput...");
                            String parsed_output = msfFiles[i].replace(".msf", ".m2lite.psms");
                            if(includeProteinsInTextOutput){
                                printer.print(msfPeps, parsed_output, 2, includeiTRAQ4plexValues, peptideIDToProteinsMap); // +/- Filter properties
                            } else{
                                printer.print(msfPeps, parsed_output, 1, includeiTRAQ4plexValues, peptideIDToProteinsMap);
                            }
                            
                            break;
                    }
                }
                break;            
        }    

        System.out.println("\n...Done!!!");
        Date end_time = new Date();
        done = true;
        long end = end_time.getTime();
        System.out.println("End: " + end + ": " + end_time.toString());
        System.out.println("Total time: " + (end - start) + " milliseconds; " + 
                        TimeUnit.MILLISECONDS.toMinutes(end - start) + " min(s), "
                        + (TimeUnit.MILLISECONDS.toSeconds(end - start) - 
                           TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(end - start))) + " seconds.");
    }
    
    private HashMap<Integer, LinkedList<Integer>> mapSpectra2PeptideIDs(MSFPSMPeptide[] msfPeps, 
                                                                        MSFPSMPeptide[] decoyMSFPeps) {
        System.out.println("\tMapping Spectra ids to Peptide ids...");
        HashMap<Integer, LinkedList<Integer>> spectID2PepIDsMap = new HashMap<Integer, LinkedList<Integer>>();
        
        //map non-decoys
        //Iterator<MSFPSMPeptide> itr  =  msfPeps.iterator();
        System.out.println("\t\tMapping non-decoys...");
        for(MSFPSMPeptide pep : msfPeps){
        //while(itr.hasNext()){
            //MSFPSMPeptide pep = itr.next();
            int spectrumID = pep.getSpectrumID();
            int peptideID = pep.getPeptideID();
            if(spectID2PepIDsMap.containsKey(spectrumID)){
                LinkedList<Integer> mappedPepIDs = spectID2PepIDsMap.remove(spectrumID);
                if(inList(mappedPepIDs, peptideID) == false){
                    mappedPepIDs.add(peptideID);
                }
                spectID2PepIDsMap.put(spectrumID,mappedPepIDs);
            } else {
                LinkedList<Integer> mappedPepIDs = new LinkedList<Integer>();
                mappedPepIDs.add(peptideID);
                spectID2PepIDsMap.put(spectrumID,mappedPepIDs);
            }
        }
        //map decoys
        //Iterator<MSFPSMPeptide> itr2  =  decoyMSFPeps.iterator();
        System.out.println("\t\tMapping decoys...");
        //while(itr2.hasNext()){
        for(MSFPSMPeptide pep : decoyMSFPeps){
            //MSFPSMPeptide pep = itr2.next();
            int spectrumID = pep.getSpectrumID();
            int peptideID = pep.getPeptideID();
            if(spectID2PepIDsMap.containsKey(spectrumID)){
                LinkedList<Integer> mappedPepIDs = spectID2PepIDsMap.remove(spectrumID);
                if(inList(mappedPepIDs, peptideID) == false){
                    mappedPepIDs.add(peptideID);
                }
                spectID2PepIDsMap.put(spectrumID,mappedPepIDs);
            } else {
                LinkedList<Integer> mappedPepIDs = new LinkedList<Integer>();
                mappedPepIDs.add(peptideID);
                spectID2PepIDsMap.put(spectrumID,mappedPepIDs);
            }
        }
        
        return spectID2PepIDsMap;
    }

    private boolean inList(LinkedList<Integer> mappedPepIDs, int peptideID) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean in = false;
        Iterator<Integer> itr = mappedPepIDs.iterator();
        while(itr.hasNext()){
            int pepID = itr.next();
            if(pepID == peptideID){
                in = true;
                break;
            }
        }
        return in;
    }

    private HashMap<Integer, MSFPSMPeptide> mapPeptideID2MSFPeptide(MSFPSMPeptide[] msfPeps, 
                                                                    MSFPSMPeptide[] decoyMSFPeps) {
        //throw new UnsupportedOperationException("Not yet implemented");
        HashMap<Integer, MSFPSMPeptide> mapPeptideID2MSFPeptide = new HashMap<Integer, MSFPSMPeptide>();
        System.out.println("\tMapping Peptide ids to MSFPSMPeptides...");
        
       //Iterator<MSFPSMPeptide> itr1 = msfPeps.iterator();
        //while(itr1.hasNext()){
        for(MSFPSMPeptide pep : msfPeps){
            //MSFPSMPeptide pep = itr1.next();
            int id = pep.getPeptideID();
            mapPeptideID2MSFPeptide.put(id, pep);
        }
        //Iterator<MSFPSMPeptide> itr2 = decoyMSFPeps.iterator();
        //while(itr2.hasNext()){
        for(MSFPSMPeptide pep : decoyMSFPeps){
            //MSFPSMPeptide pep = itr2.next();
            int id = pep.getPeptideID();
            mapPeptideID2MSFPeptide.put(id, pep);
        }
        
        return mapPeptideID2MSFPeptide;
    }
    
    private HashMap<Integer, LinkedList<Peptide>> updateInstancesMap(MSFPSMProtein[] msfProts, 
                                            HashMap<Integer, LinkedList<Peptide>> pepID2PeptideInstancesMap,
                                                HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap) {
        System.out.println(" Updating pepID2PepInstance map...");
        int reversals = 0;
        //Iterator<MSFPSMProtein> itr = msfProts.iterator();
        //while(itr.hasNext()){
        for(MSFPSMProtein prot : msfProts){
            //MSFPSMProtein prot = itr.next();
            int pepID = prot.getMappedPeptideID();
            //get peptideInstance in mapped protein
            Peptide peptideInstance;
            MSFPSMPeptide msfPep = pepID2MSFPeptideMap.get(pepID);
            if(msfPep != null){
                String pepSeq = msfPep.getSequence();
                if(prot.getSequence().contains(pepSeq)){
                    peptideInstance = prot.getPeptideInstance(pepSeq);
                } else { // expectedly the protein should contain, otherwise reverse the sequence
                    prot.reverseSequence();// reverse the protein sequence and get instance
                    reversals++;
                    if(prot.getSequence().contains(pepSeq)){ //if reversed protein sequence contains peptide get instance                                                            
                        peptideInstance = prot.getPeptideInstance(pepSeq);
                    } else { // otherwise it is a jumbled/shuffled decoy hit
                        peptideInstance = new Peptide(pepSeq, prot, 0 + 1, 
                                        0 + 1, (new StringBuffer().append("-.").append(pepSeq).append(".-")).toString());
                        reversals--;
                    }
                    //peptideInstance = prot.getPeptideInstance(pepSeq);                    
                }
                if(pepID2PeptideInstancesMap.containsKey(pepID)){
                    LinkedList<Peptide> instances = pepID2PeptideInstancesMap.remove(pepID);
                    instances.add(peptideInstance);
                    pepID2PeptideInstancesMap.put(pepID, instances);
                } else {
                    LinkedList<Peptide> instances = new LinkedList<Peptide>();
                    instances.add(peptideInstance);
                    pepID2PeptideInstancesMap.put(pepID, instances);
                }
            }            
        }
        //System.out.println("   Number of protein sequences: " + msfProts.length);
        //System.out.println("   Number of reversed matches: " + reversals);
        return pepID2PeptideInstancesMap;
    }  
    
    public boolean isDone(){
        return done;
    }
    
    /*
     * parse(String[] msfFiles, OutputFormatType outputFormat, OutputObjectType outputObjs,
                      SearchDatabaseType dbtype, String searchDBPath)
       * String[] msfFiles, 
       * OutputFormatType outputFormat, 
       * OutputObjectType outputObjs,
       * SearchDatabaseType dbtype, 
       * String searchDBPath                
     * 
     */
    public static void main(String[] args) throws FileNotFoundException, IOException{
        
        // check that the correct arguments are inputed
        if(args.length < 2){
            System.out.println(" --------- ERROR: Two input arguments required --------- \n" +
                               "USAGE: java -Xmx1G -cp ./M2Lite.jar msfparser.M2Lite [FILE|DIRECTORY] [mzid|pepXML|text]\n" +
                               "see documentation file for additional information....\n" +
                               " ------------------------------------------------------- ");
            System.exit(1); // exit JVM
        }
        String input = args[0]; //input file/folder
        String output = args[1]; //outputFormat
        HashMap<String,String> config = new HashMap<String,String>();
        
        String[] inFiles = null;
        OutputFormatType outputFormat;
        OutputObjectType outputObjs;
        SearchDatabaseType dbtype = null;
        String searchDBPath;
        boolean includeiTRAQ4plexValues = false;
        boolean includeProteinsInTextOutput = false;
        
        boolean tppCompatibleOutput = false;
        
         // ********** get configurations *********** //
        BufferedReader reader = new BufferedReader(new FileReader("M2Lite.config"));
        int configFileLine = 0;
        String line;
        while((line = reader.readLine())!=null){
            configFileLine++;
            if(line.charAt(0)=='#'){
                //do nothing, because it is a comment line...
            }else{
                // separate config statement from line comment
                String[] lineElements = line.split("#");
                //String[] lineArr = line.split("=");
                //config.put(lineArr[0], lineArr[1]);
                String[] lineArr = lineElements[0].split("=");
                config.put(lineArr[0].trim(), lineArr[1].trim());
            }
        }
               
        //get input(s)
        File file = new File(input);
        if(file.isFile()){
            inFiles = new String[1];
            inFiles[0] = file.getAbsolutePath();
        } else if(file.isDirectory()){
            //get config file's FileChooserOption
            if(config.get("FileChooserOption").equalsIgnoreCase("TRUE")){ //use FileChooser to select input file(s) 
                MultiFileChooser inFilesChooser = new MultiFileChooser("Select input file(s)...", input);
                inFiles = inFilesChooser.getInputFiles();
            } else {
                //get .msf files in the directory
                File[] filesInDir = file.listFiles();
                //String[] filesInDir = file.list();
                //inFiles = getMSFFiles(filesInDir);
                ArrayList<String> filesInD = new ArrayList<String>();
                for(int i = 0; i < filesInDir.length; i++){
                    if(filesInDir[i].getAbsolutePath().endsWith("msf")){
                        filesInD.add(filesInDir[i].getAbsolutePath());
                    }
                }
                inFiles = new String[filesInD.size()];
                for(int i = 0; i < inFiles.length; i++){
                    inFiles[i] = filesInD.get(i);
                }
            }
        } else {
            System.out.println("ERROR: First argument requires an \"msf\" file or an \"msf-containing\" directory input...");
            System.exit(1);
        }
        
        //get outputFormat
        if(output.equalsIgnoreCase("pepXML")){
            outputFormat = OutputFormatType.PEPXML;
        } else if(output.equalsIgnoreCase("mzid")){
            outputFormat = OutputFormatType.MZIDENTML;
        } else {
            outputFormat = OutputFormatType.TAB_DELIMITED_TXT;
            // get if iTRAQ4plex values should be included
            if(config.get("IncludeiTRAQ4plexValues").equalsIgnoreCase("TRUE")){
                includeiTRAQ4plexValues = true;
            }
            if(config.get("includeProteinsInTextOutput").equalsIgnoreCase("TRUE")){
                includeProteinsInTextOutput = true;
            }
            
        }
               
        /*
         *  
         */
        // get search database file path
        searchDBPath = config.get("SearchDBPath");
        
        // get search database file type
        String searchDBType = config.get("SearchDatabaseType");
        if(searchDBType.equalsIgnoreCase("REFSEQ")){
            dbtype = SearchDatabaseType.REFSEQ; //default
        } else if(searchDBType.equalsIgnoreCase("UNIPRO")){
           dbtype = SearchDatabaseType.UNIPRO; 
        } else if(searchDBType.equalsIgnoreCase("IPIv387")){
           dbtype = SearchDatabaseType.IPIv387;  // deprecated
        }
        
        // get output object type [defaults to peptides only]
        outputObjs = OutputObjectType.PEPTIDES_ONLY; // // defaults to all PEPTIDES "real" and "decoys" only (i.e. no protein inference information is outputted in .pepXML file
        
        // get rQueryScript
        String rQueryScriptLibrary = config.get("RDBQueryFilesLibrary");
        
        // get tpp compatibility option (applies to pepXML output)
        String tppC = config.get("TPPCompatibleOutput");
        if(tppC.equalsIgnoreCase("TRUE")){
            tppCompatibleOutput = true;
        } else {
            tppCompatibleOutput = false;
        }
                
        //Parse File(s)
        M2Lite m2Lite = new M2Lite();
        m2Lite.parse(inFiles, outputFormat, outputObjs, dbtype, searchDBPath, 
                        rQueryScriptLibrary, includeiTRAQ4plexValues,
                            includeProteinsInTextOutput, tppCompatibleOutput);
    }

}
