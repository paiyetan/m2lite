/*
 * To change this template, choose Tools | Templates
 * and open the template in the 
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfparser;

import java.sql.*;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfproteins.MSFPSMProteinsExtractor;

/**
 *
 * @author paiyeta1
 */
public class MSFSetUpParamsExtractor {
    
    public HashMap<String,String> extractParams(String msfFile){
        HashMap<String,String> parametersMap = new HashMap<String,String>();
        Connection connection;  
        try {  
            Class.forName("org.sqlite.JDBC"); //Deprecated in recent JDBC implementation
            connection = DriverManager.getConnection("jdbc:sqlite:" + msfFile);  
            Statement statement = connection.createStatement();  
            System.out.println("\tQuerying file for search parameters...");    
            String query = "SELECT * FROM ProcessingNodeParameters;";
            ResultSet resultSet = statement.executeQuery(query); 
            System.out.println("\tRetrieving file search parameters returned..."); 
            int noOfResultsRetrieved = 0;
            while(resultSet.next()){
                noOfResultsRetrieved++;
                String parameterName = resultSet.getString("ParameterName");
                String parameterValueDisplayString = resultSet.getString("ValueDisplayString");
                parametersMap.put(parameterName, parameterValueDisplayString);               
            }
            System.out.println("\t" + noOfResultsRetrieved + " parameter entries found.");
            
            //autoExtract PDVersion
            System.out.println("\tQuerying file to auto-extract PD version...");    
            query = "SELECT * FROM SchemaInfo;";
            resultSet = statement.executeQuery(query); 
            System.out.println("\tRetrieving file auto-extract PD version returned..."); 
            while(resultSet.next()){
                String parameterName = "PDVersion";
                String parameterValueDisplayString = resultSet.getString("SoftwareVersion");
                parametersMap.put(parameterName, parameterValueDisplayString);               
            }
            
            //autoExtract search engine (Mascot or SEQUEST...
            System.out.println("\tQuerying file to auto-extract search engine...");    
            query = "SELECT * FROM ProcessingNodes;";
            resultSet = statement.executeQuery(query); 
            System.out.println("\tRetrieving ProcessingNodes info returned..."); 
            while(resultSet.next()){
                String parameterName = resultSet.getString("NodeName");
                String parameterValueDisplayString = resultSet.getString("FriendlyName");
                parametersMap.put(parameterName, parameterValueDisplayString);                
            }
            
            //autoExtract fileName
            System.out.println("\tQuerying file to auto-extract file name...");    
            query = "SELECT * FROM FileInfos;";
            resultSet = statement.executeQuery(query); 
            System.out.println("\tRetrieving file infos returned..."); 
            while(resultSet.next()){
                String parameterName = "SearchedRawFileName";
                String parameterValueDisplayString = resultSet.getString("FileName"); //NOTE: this is the full file path name...
                parametersMap.put(parameterName, parameterValueDisplayString);                
            }
            
            //autoExtract searched Database ---- already filled in config file...
            System.out.println("\tQuerying file to auto-extract searched database...");    
            query = "SELECT * FROM FastaFiles;";
            resultSet = statement.executeQuery(query); 
            System.out.println("\tRetrieving ProcessingNodes info returned..."); 
            while(resultSet.next()){
                //String parameterName = "DatabaseFilePath";
                //String parameterValueDisplayString = resultSet.getString("FileName"); //path to Thermo PD indexed database
                //parametersMap.put(parameterName, parameterValueDisplayString);
                
                String parameterName = "DatabaseName";
                //String parameterValueDisplayString = resultSet.getString("VirtualFileName"); //absolute name
                String parameterValueDisplayString = resultSet.getString("FileName"); //full path to PD-configured fasta file...
                parametersMap.put(parameterName, parameterValueDisplayString);
            }
            
            
            statement.close();
            resultSet.close();
            connection.close();
            
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MSFPSMProteinsExtractor.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (SQLException ex) {
            Logger.getLogger(MSFPSMProteinsExtractor.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }       
        return parametersMap;
    }
    
}
