/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 */
package ios.readers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfpsms.MSFPSMPeptide;

/**
 *
 * @author paiyeta1
 */
public class MSFRoutPSMPeptidesFileReader {
    
    @SuppressWarnings("CallToThreadDumpStack")
    public MSFPSMPeptide[]  read(String inFile){
        
        MSFRoutTableEntriesCounter counter = new MSFRoutTableEntriesCounter();
        int fileEntries = counter.countEntries(inFile);
        MSFPSMPeptide[] msfPeps = new MSFPSMPeptide[fileEntries];
        int count = 0;
        int entry = 0;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inFile));
            String line;
            while((line = reader.readLine())!=null){
                count++;
                if(count == 1){ // get header indices
                    //insert code of what to do
                }
                
                if(count > 1){ // skip header line
                    if ( line.trim().length() == 0 ) {  
                        continue;  // Skip blank lines  
                    } else {
                        String[] lineArr = line.split("\t");
                        /*
                        [0] PeptideID	
                        [1]ProcessingNodeNumber	
                        [2]Sequence	
                        [3]SpectrumID	
                        [4]TotalIonsCount	
                        [5]MatchedIonsCount	
                        [6]ConfidenceLevel	
                        [7]SearchEngineRank	
                        [8]Annotation	
                        [9]UniquePeptideSequenceID	
                        [10]MissedCleavages	
                        [11]ScanNumbers	
                        [12]RetentionTime	
                        [13]Mass	
                        [14]Charge	
                        [15]Intensity	
                        [16]PercentIsolationInterference	
                        [17]IonInjectTime	
                        [18]MZ	
                        [19]MSLevel	
                        [20]ActivationType	
                        [21]FileName
                         * 
                         */
                        int ProcessingNodeNumber = Integer.parseInt(lineArr[1]);
                        int PeptideID = Integer.parseInt(lineArr[0]);
                        int SpectrumID = Integer.parseInt(lineArr[3]);
                        int TotalIonsCount = Integer.parseInt(lineArr[4]);
                        int MatchedIonsCount = Integer.parseInt(lineArr[5]);
                        int ConfidenceLevel = Integer.parseInt(lineArr[6]);
                        int SearchEngineRank = Integer.parseInt(lineArr[7]);
                        String Sequence = lineArr[2];
                        String Annotation = lineArr[8];
                        int UniquePeptideSequenceID = Integer.parseInt(lineArr[9]);
                        int MissedCleavages = Integer.parseInt(lineArr[10]);
                        double RetentionTime = Double.parseDouble(lineArr[12]);
                        int FirstScan = getFirstScan(lineArr[11]);
                        int LastScan = getLastScan(lineArr[11]);
                        int Charge = Integer.parseInt(lineArr[14]);
                        double Mass = Double.parseDouble(lineArr[13]);
                        //double Score = Double.parseDouble(lineArr[16]);
                        double Intensity = Double.parseDouble(lineArr[15]);
                        float PercentIsolationInterference;
                        if(!lineArr[16].equalsIgnoreCase("NA")){
                            PercentIsolationInterference = Float.parseFloat(lineArr[16]);
                        } else {
                            PercentIsolationInterference = 0; }
                        int IonInjectTime;
                        if(!lineArr[17].equalsIgnoreCase("NA")){
                            IonInjectTime = Integer.parseInt(lineArr[17]);
                        } else {
                            IonInjectTime = 0; }
                        int MSLevel = Integer.parseInt(lineArr[19]);
                        int ActivationType = Integer.parseInt(lineArr[20]);                                             
                        String FileName = lineArr[21];
                        double MZ = Double.parseDouble(lineArr[18]);
                        
                        msfPeps[entry] = new MSFPSMPeptide(ProcessingNodeNumber, PeptideID, SpectrumID, 
                                                TotalIonsCount, MatchedIonsCount, ConfidenceLevel, SearchEngineRank, 
                                                Sequence, Annotation, UniquePeptideSequenceID, MissedCleavages, 
                                                RetentionTime, FirstScan, LastScan, Charge, Intensity, 
                                                Mass, MZ, PercentIsolationInterference, IonInjectTime, 
                                                MSLevel, ActivationType, FileName);    
                        entry++;
                    }
                }
                
            }
            reader.close();
                       
        } catch (IOException ex) {
            Logger.getLogger(MSFRoutPSMPeptidesFileReader.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (NumberFormatException ex){
            System.out.println("Can't read a content in line " + count + "...");
            Logger.getLogger(MSFRoutPSMPeptidesFileReader.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }        
        return msfPeps;        
    }

    private int getFirstScan(String string) {
        int first;
        String[] strArr = string.split(";");
        int[] scanNumbers = new int[strArr.length];
        for(int i = 0; i < strArr.length; i++){          
            scanNumbers[i] = Integer.parseInt(strArr[i]);
        }
        Arrays.sort(scanNumbers);
        first = scanNumbers[0];
        return first;
    }

    private int getLastScan(String string) {
        int last;
        String[] strArr = string.split(";");
        int[] scanNumbers = new int[strArr.length];
        for(int i = 0; i < strArr.length; i++){          
            scanNumbers[i] = Integer.parseInt(strArr[i]);
        }
        Arrays.sort(scanNumbers);
        last = scanNumbers[scanNumbers.length - 1];
        return last;
    }

}
