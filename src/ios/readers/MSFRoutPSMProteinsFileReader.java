/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package ios.readers;

import enumtypes.SearchDatabaseType;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfproteins.MSFPSMProtein;

/**
 *
 * @author paiyeta1
 */
public class MSFRoutPSMProteinsFileReader {
    
    public MSFPSMProtein[] read(String inFile,SearchDatabaseType dbtype){
        
        MSFRoutTableEntriesCounter counter = new MSFRoutTableEntriesCounter();
        int fileEntries = counter.countEntries(inFile);
        
        MSFPSMProtein[] psmProteins = new MSFPSMProtein[fileEntries];
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inFile));
            String line;
            int entry = 0;
            int count = 0; //count of lines reader
            while((line = reader.readLine())!=null){
                count++;
                if(count == 1){ // get header indices
                    //insert code of what to do should I decide to automatically figure the column indeces of attributes
                }
                
                if(count > 1){ // skip header line
                    if ( line.trim().length() == 0 ) {  
                        continue;  // Skip blank lines  
                    } else {
                        
                        String[] lineArr = line.split("\t");
                        /*
                         * 
                         *   p.Sequence AS PeptideSequence," + 
                            "pprot.PeptideID," +
                            "pprot.ProteinID," +
                            "prot.Sequence," +
                            "prot.IsMasterProtein," +
                            "protAnnot.Description," +
                            "protPrGrp.ProteinGroupID," +
                            "pscores.ProteinScore " +
                                
                         * 
                         */
                         String mappedPeptideSequence = lineArr[0];
                         int PeptideID = Integer.parseInt(lineArr[1]);
                         int ProteinID = Integer.parseInt(lineArr[2]);
                         String Sequence = lineArr[3];
                         int IsMasterProtein = Integer.parseInt(lineArr[4]);
                         String Description = lineArr[5];
                         //int  ProteinGroupID = Integer.parseInt(lineArr[6]);
                         double ProteinScore = Double.parseDouble(lineArr[6]);
                        
                         MSFPSMProtein psmProtein = new MSFPSMProtein(ProteinID, Description, 
                                                        //ProteinGroupID, 
                                                        Sequence, IsMasterProtein, ProteinScore, dbtype, mappedPeptideSequence, PeptideID);
                         psmProteins[entry] = psmProtein;
                         entry++;
                    }
                }
                
            }
            reader.close();
            
        } catch (IOException ex) {
            Logger.getLogger(MSFRoutPSMPeptidesFileReader.class.getName()).log(Level.SEVERE, null, ex);
        }         
        return psmProteins;
    }
    
}
