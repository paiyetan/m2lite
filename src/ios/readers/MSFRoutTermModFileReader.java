/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package ios.readers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfpsms.MSFPSMModification;

/**
 *
 * @author paiyeta1
 */
public class MSFRoutTermModFileReader {
    
    public HashMap<Integer,LinkedList<MSFPSMModification>> read(String inFile){
        HashMap<Integer,LinkedList<MSFPSMModification>> peptideID2TermModificationMap = 
                new HashMap<Integer,LinkedList<MSFPSMModification>>();
        
        try {
            BufferedReader reader = new BufferedReader(new FileReader(inFile));
            String line;
            int count = 0; //count of lines reader
            while((line = reader.readLine())!=null){
                count++;
                if(count == 1){ // get header indices
                    //insert code of what to do
                }
                
                if(count > 1){ // skip header line
                    if ( line.trim().length() == 0 ) {  
                        continue;  // Skip blank lines  
                    } else {
                        String[] lineArr = line.split("\t");
                        /*
                         * pTMods.PeptideID," +
                            "pTMods.TerminalModificationID," +
                            "aAMods.ModificationName," +
                            "aAMods.DeltaMass " +
                         */
                        int PeptideID = Integer.parseInt(lineArr[0]);
                        //int TerminalModificationID = Integer.parseInt(lineArr[1]);
                        String ModificationName = lineArr[2];
                        double DeltaMass = Double.parseDouble(lineArr[3]);
                        
                        MSFPSMModification modification = 
                                new MSFPSMModification(0, ModificationName, DeltaMass,true);//Nterm ==position 0 [0+1]
                        //if PeptideID isn't a key in the Map, 
                            // instantiate a LinkedList of modifications, 
                            // insert 'modification' into list, and 
                            // map PeptideID with instantiaited list
                        if(!peptideID2TermModificationMap.containsKey(PeptideID)){
                            LinkedList<MSFPSMModification> peptideModifications = new LinkedList<MSFPSMModification>();
                            peptideModifications.add(modification);
                            peptideID2TermModificationMap.put(PeptideID, peptideModifications);
                        } else {
                            // retrieve/remove the mapping - 
                            LinkedList<MSFPSMModification> mappedPeptideModifications = peptideID2TermModificationMap.remove(PeptideID);
                            // findout if modification hasn't been previously mapped to peptide, if not, add to list
                            //if(!inModificationLinkedList(modification,mappedPeptideModifications)){
                            if(!mappedPeptideModifications.contains(modification)){
                                mappedPeptideModifications.add(modification);
                            }
                            // reinsert mapping with updated mappedPeptideProteins
                            peptideID2TermModificationMap.put(PeptideID, mappedPeptideModifications);
                        }
                            }
                        }
                
            }
            
            reader.close();
            
        } catch (IOException ex) {
            Logger.getLogger(MSFRoutPSMPeptidesFileReader.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } 
        
        return peptideID2TermModificationMap; 
    }
}
