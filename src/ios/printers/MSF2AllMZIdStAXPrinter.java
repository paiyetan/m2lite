/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package ios.printers;

import database.Peptide;
import enumtypes.SearchDatabaseType;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import msfparser.MSFSearchModificationParam;
import msfproteins.MSFPSMProtein;
import msfpsms.MSFPSMMasses;
import msfpsms.MSFPSMModification;
import msfpsms.MSFPSMPeptide;
import msfpsms.MSFPSMPeptideRankQueue;
import org.w3c.dom.Element;

/**
 *
 * @author paiyeta1
 */
public class MSF2AllMZIdStAXPrinter {
    
    
    @SuppressWarnings("CallToThreadDumpStack")
    public void write(HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap,
                      HashMap<String,String> parametersMap, 
                      HashMap<String,String> modTermMap, 
                      HashMap<Integer,LinkedList<Integer>> spectID2PepIDsMap,
                      String mzidMLOutFile, 
                      //Database db,
                      String searchDBPath, 
                      SearchDatabaseType dbtype, 
                      boolean includeProteinsInferred,
                      HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap) {
        try {
            
            
            // resource allocation
            // namespace management
            // attribute management
            // error-handling
            // writing streams (
            System.out.println("\tWriting .mzid file...");
            XMLOutputFactory xMLOutfactory = XMLOutputFactory.newInstance();
            XMLStreamWriter writer = 
                    xMLOutfactory.createXMLStreamWriter(new BufferedWriter(new FileWriter(mzidMLOutFile)));
            
            //DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            //DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            writer.writeStartDocument("ISO-8859-1","1.0");
            writer.writeCharacters("\n");
            /*
             * id, MPC_MzIdML
             * creationDate: 2013-10-09T23:14:49
             * version, 1.1.0
             * xmnlns, http://psidev.info/psi/pi/mzIdentML/1.1
             * xmlns:xsi, http://www.w3.org/2001/XMLSchema-instance
             * xsi:schemaLocation, http://psidev.info/psi/pi/mzIdentML/1.1 http://www.psidev.info/files/mzIdentML1.1.0.xsd
             * 
             */
            String docID = mzidMLOutFile + " " + searchDBPath + " " + "PD version " + parametersMap.get("PDVersion");
            writer.writeStartElement("MzIdentML");
            writer.writeAttribute("id", docID);
            writer.writeAttribute("creationDate",new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()));
            writer.writeAttribute("version","1.1.0");
            writer.writeAttribute("xsi:schemaLocation","http://psidev.info/psi/pi/mzIdentML/1.1 http://www.psidev.info/sites/default/files/mzIdentML1.1.0.xsd");
            writer.writeAttribute("xmlns","http://psidev.info/psi/pi/mzIdentML/1.1");
            writer.writeAttribute("xmlns:xsi","http://www.w3.org/2001/XMLSchema-instance");
            writer.writeCharacters("\n");
            
            
            /* level One elements
                cvList, 
                AnalysisSoftwareList, 
                +/- AnalysisSampleCollection
                Provider, 
                AuditCollection, 
                SequenceCollection,
                AnalysisCollection
                AnalysisProtocolCollection
                DataCollection
            * 
            */
            int indentFactor = 2;
            
            // basic iDPicker-required objects
            writeCVList(indentFactor, writer);
            writeAnalysisSoftwareList(indentFactor, writer, parametersMap);
            writeSequenceCollection(indentFactor, writer, pepID2PeptideInstancesMap,
                                        parametersMap, pepID2MSFPeptideMap, modTermMap);
            writeAnalysisCollection(indentFactor, writer, parametersMap);
            writeAnalysisProtocolCollection(indentFactor, writer, parametersMap, modTermMap);
            writeDataCollection(indentFactor, writer, pepID2MSFPeptideMap, parametersMap, 
                                        spectID2PepIDsMap, //mzidMLOutFile, 
                                        searchDBPath, pepID2PeptideInstancesMap);
            
            // mzidentML standard documents
            //writeAnalysisSampleCollection(indentFactor, writer);
            //writeProvider(indentFactor, writer);
            //writeAuditCollection(indentFactor, writer);
            
            
            writer.writeEndElement(); //</MzIdenML>
            writer.writeEndDocument();
            
            //writer.flush();
            writer.close();
            
            System.out.println("...mzid file saved!");
 
        } catch (XMLStreamException ex) {
            Logger.getLogger(MSF2AllMZIdStAXPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MSF2AllMZIdStAXPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(MSF2AllMZIdStAXPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        
    }

    
    /* level One elements' [writers]
        cvList, 
        AnalysisSoftwareList, 
        +/- AnalysisSampleCollection
        Provider, 
        AuditCollection, 
        SequenceCollection,
        AnalysisCollection
        AnalysisProtocolCollection
        DataCollection
     * 
     */
    // *********************** //
    //  Writer Methods
    // *********************** //
    
    private void writeCVList(int indentFactor, XMLStreamWriter writer) throws XMLStreamException{
        
        // ******************************************* //
        // cvList 
        // Used controlled vocabularies - these three usually are the only ones required thus can be hard-coded
        // ******************************************* //    
        //Create cvList elements
        System.out.println("\tWriting 'cvList' elements...");
        writer.writeCharacters(indent(indentFactor));
        writer.writeStartElement("cvList");
        //writer.writeAttribute("xmlns", "http://psidev.info/psi/pi/mzIdentML/1.1");
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeEmptyElement("cv");
        writer.writeAttribute("id", "PSI-MS");
        writer.writeAttribute("fullName", "Proteomics Standards Initiative Mass Spectrometry Ontology");
        writer.writeAttribute("version", "3.54.0");
        writer.writeAttribute("uri", "http://psidev.cvs.sourceforge.net/viewvc/psidev/psi/psi-ms/mzML/controlledVocabulary/psi-ms.obo");
        writer.writeCharacters("\n");
        
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeEmptyElement("cv");
        writer.writeAttribute("id", "UNIMOD");
        writer.writeAttribute("fullName", "UNIMOD");
        writer.writeAttribute("version", "1.2");
        writer.writeAttribute("uri", "http://obo.cvs.sourceforge.net/obo/obo/ontology/phenotype/unit.obo");
        writer.writeCharacters("\n");
        
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeEmptyElement("cv");
        writer.writeAttribute("id", "UO");
        writer.writeAttribute("fullName", "UNIT-ONTOLOGY");
        writer.writeAttribute("version", "1.2");
        writer.writeAttribute("uri", "http://obo.cvs.sourceforge.net/obo/obo/ontology/phenotype/unit.obo");
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor));
        writer.writeEndElement(); //</cvList>
        writer.writeCharacters("\n");
        
        
    }
    
    private void writeAnalysisSoftwareList(int indentFactor, XMLStreamWriter writer, 
                                HashMap<String,String> parametersMap) throws XMLStreamException{
                    
        // ******************************************* //
        // AnalysisSoftwareList
        // Software(s), sourced from PSI-MS CV 
        // ******************************************* //
        System.out.println("\tWriting 'AnalysisSoftwareList' elements...");
        //AnaysisSoftwareList
        writer.writeCharacters(indent(indentFactor));
        writer.writeStartElement("AnalysisSoftwareList");
        writer.writeCharacters("\n");
        
        //AnalysisSoftwareList/AnalysisSoftware
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeStartElement("AnalysisSoftware");
        writer.writeAttribute("id", "PD");
        writer.writeAttribute("version", parametersMap.get("PDVersion"));
        writer.writeAttribute("uri", "http://www.thermoscientific.com/en/product/proteome-discoverer-software.html");
        writer.writeCharacters("\n");
        
        //AnalysisSoftwareList/AnalysisSoftware/SoftwareName
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("SoftwareName");
        //writer.writeCharacters("\n");
        
        //AnalysisSoftwareList/AnalysisSoftware/SoftwareName/cvParam
        //writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1000650");
        writer.writeAttribute("name", "Proteome Discoverer");
        writer.writeAttribute("value", "");
        //writer.writeCharacters("\n");
        
        //writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement(); // </SoftwareName>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeEndElement(); // </AnalysisSoftware>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor));
        writer.writeEndElement(); // </AnalysisSoftwareList>
        writer.writeCharacters("\n");
        
    }
    
    // NB: +/- AnalysisSampleCollection is ommitted, as is ommitted in MyriMatch-derived mzid
    private void writeAnalysisSampleCollection(int indentFactor, 
                                                XMLStreamWriter writer){
        
    }
    
    // NB: this is ommmitted in MyriMatch-derived mzid
    private void writeProvider(int indentFactor, XMLStreamWriter writer){
                  
        // ******************************************* //
        // Provider - hard-coded; may be retrieved through some interface
        // Provider of the document, can be hard-coded, with a reference to a Person in "AuditCollection"
        // ******************************************* //
        
        
    }
    
    // NB: this is ommmitted in MyriMatch-derived mzid
    private void writeAuditCollection(int indentFactor, XMLStreamWriter writer){
        /* ******************************************* //
            AuditCollection
            Minimally insert contact details in here for the Provider of 
            the document if known, otherwise provide dummy values
        // ******************************************* //
        * 
        */
        
    }
    
    
    private void writeSequenceCollection(int indentFactor, XMLStreamWriter writer,
                                            HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap,
                                            HashMap<String,String> parametersMap,
                                            HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap,
                                            HashMap<String,String> modTermMap) throws XMLStreamException{
           
        // ******************************************* //
        // SequenceCollection
        // ******************************************* //
        System.out.println("\tWriting 'SequenceCollection' document elements...");
        writer.writeCharacters(indent(indentFactor));
        writer.writeStartElement("SequenceCollection");//<SequenceCollection>
        writer.writeCharacters("\n");
        
        // SequenceCollection/DBSequence/
        /*
         * List all protein sequences to be referenced from elsewhere
           get all unique peptide [Unique by modifications]
         */
        ArrayList<MSFPSMProtein> mappedMSFProteins =  getUniqueMappedMSFProteins(pepID2PeptideInstancesMap);
        Iterator<MSFPSMProtein> itr = mappedMSFProteins.iterator();
        //String searchedDB = parametersMap.get("FastaDatabase");
        System.out.println("\t  Writing 'DBSequence' elements...");
        while(itr.hasNext()){
            MSFPSMProtein protein = itr.next();
            //SequenceCollection/DBSequence/
            writer.writeCharacters(indent(indentFactor + 2));
            writer.writeEmptyElement("DBSequence");
            writer.writeAttribute("id", (new StringBuilder()).append("DBSeq_").append(protein.getMyriMatchDescription()).toString());
            writer.writeAttribute("accession", protein.getMyriMatchDescription());
            writer.writeAttribute("searchDatabase_ref", "SDB_1");
            writer.writeCharacters("\n");           
        }

        //SequenceCollection/Peptide/
        /* 
         * List all peptide (unique) sequences and modifications to be referenced from elsewhere
         * This pretty much correponds to identified psms.
         */
        Set<Integer> pepIDs = pepID2MSFPeptideMap.keySet();
        Iterator<Integer> itr2 = pepIDs.iterator();
        System.out.println("\t  Writing 'Peptide' elements...");
        while(itr2.hasNext()){
            int peptideID = itr2.next();
            MSFPSMPeptide msfPep = pepID2MSFPeptideMap.get(peptideID);
            //SequenceCollection/Peptide/
            writer.writeCharacters(indent(indentFactor + 2));
            writer.writeStartElement("Peptide");
            writer.writeAttribute("id", (new StringBuilder()).append("PEP_").append(msfPep.getPeptideID()).toString());
            writer.writeCharacters("\n");
            
            //SequenceCollection/Peptide/PeptideSequence
            writer.writeCharacters(indent(indentFactor + 4));
            writer.writeStartElement("PeptideSequence");
            writer.writeCharacters(msfPep.getSequence());
            writer.writeEndElement(); //</PeptideSequence>
            writer.writeCharacters("\n"); 

            LinkedList<MSFPSMModification> modfxns = msfPep.getModifications();
            for(int i = 0; i < modfxns.size(); i++){
                //SequenceCollection/Peptide/Modification
                writer.writeCharacters(indent(indentFactor + 4));
                writer.writeStartElement("Modification");
                MSFPSMModification modfxn = modfxns.get(i);
                String modfxnLocation = String.valueOf(modfxn.getPosition()+1);
                // ***** MyriMatch-like/IDPicker compartible object model ******
                if(modfxn.isTerminal() & modfxn.getModificationName().equals("iTRAQ4plex"))
                    modfxnLocation = "0"; // that means it's a terminal modification and IDPicker would read it as location 0;
                // ************************************************************* //
                writer.writeAttribute("location", modfxnLocation);
                //writer.writeAttribute("residues", String.valueOf(msfPep.getSequence().charAt(modfxn.getPosition())));
                writer.writeAttribute("avgMassDelta", String.valueOf(modfxn.getDeltaMass()));
                writer.writeAttribute("monoisotopicMassDelta", String.valueOf(modfxn.getDeltaMass()));
                writer.writeCharacters("\n");
                
                //SequenceCollection/Peptide/Modification
                writer.writeCharacters(indent(indentFactor + 6));
                writer.writeEmptyElement("cvParam");
                if(modfxn.isTerminal()){
                    // ***** MyriMatch-like/IDPicker compartible object model ******
                    //<cvParam cvRef="MS" accession="MS:1001460" name="unknown modification" value=""/>
                    writer.writeAttribute("cvRef", "PSI-MS");
                    writer.writeAttribute("accession", "MS:1001460");
                    writer.writeAttribute("name","unknown modification"); //myrimatch reads terminal modifications as unknown
                    writer.writeAttribute("value","");
                    // ************************************************************* //
                } else {
                    writer.writeAttribute("cvRef", "UNIMOD");
                    writer.writeAttribute("accession", (new StringBuilder()).append("UNIMOD:").append(modTermMap.get(modfxn.getModificationName())).toString());
                    writer.writeAttribute("name", modfxn.getModificationName());
                    writer.writeAttribute("value",""); //myriMatch-like IDPicker-compatible empty value
                }
                writer.writeCharacters("\n"); 
                
                writer.writeCharacters(indent(indentFactor + 4));
                writer.writeEndElement(); //</Modification>
                writer.writeCharacters("\n"); 
            }                
            writer.writeCharacters(indent(indentFactor + 2));
            writer.writeEndElement();//</Peptide>
            writer.writeCharacters("\n"); 
            
        }            
        //SequenceCollection/PeptideEvidence
        /*
         * List the location of all peptide sequences within protein sequences here, assuming digestion has 
           taken place according to the specifed enzyme e.g. for full tryptic digestion,
           insert, give the locations of all peptides listed above (termininating with R or K) 
           with a pre= "R" or "K" or "-" for N-terminus 
           A somewhat peptide to protein mapping
         */
        Set<Integer> mappedIDs = pepID2PeptideInstancesMap.keySet();
        Iterator<Integer> itr3 = mappedIDs.iterator();
        HashMap<String, Element> peptideEvidences = new HashMap<String, Element>(); //using a map to speed up look up process.
        System.out.println("\t  Writing 'PeptideEvidence' elements...");
        while(itr3.hasNext()){
            int peptideID = itr3.next();
            LinkedList<Peptide> pepInstances = pepID2PeptideInstancesMap.get(peptideID);
            boolean isDecoy = pepID2MSFPeptideMap.get(peptideID).isDecoy();
            //For each uniquely modified peptide,
            //get the mapped peptides(ProteinInstances) returned for the sequence and 
            //print the mapped proteins.
            if(pepInstances != null){
                for(int i = 0; i < pepInstances.size(); i++){
                    Peptide pepInstance = pepInstances.get(i);
                    String refPeptideID = (new StringBuilder()).append("PEP_").append(peptideID).toString();
                    String referenceProtID = (new StringBuilder()).append("DBSeq_").append(pepInstance.getMSFProteinParent().getMyriMatchDescription()).toString();
                    String pepEvidenceID = (new StringBuilder()).append(referenceProtID).append("_").append(refPeptideID).toString();
                    String SequenceWithNeighbors = pepInstance.getSeqWithNeighbors();
                    
                    if(peptideEvidences.containsKey(pepEvidenceID)==false){
                        writer.writeCharacters(indent(indentFactor + 2));
                        writer.writeEmptyElement("PeptideEvidence");
                        writer.writeAttribute("id", pepEvidenceID);                
                        writer.writeAttribute("peptide_ref", refPeptideID);
                        writer.writeAttribute("dBSequence_ref", referenceProtID);
                        writer.writeAttribute("pre", String.valueOf(pepInstance.getSeqWithNeighbors().charAt(0)));
                        writer.writeAttribute("post", String.valueOf(SequenceWithNeighbors.charAt(SequenceWithNeighbors.length()-1)));
                        //writer.writeAttribute("start", String.valueOf(pepInstance.getStart()));
                        //writer.writeAttribute("end", String.valueOf(pepInstance.getEnd()));
                        writer.writeAttribute("isDecoy", String.valueOf(isDecoy));
                        writer.writeCharacters("\n");
                        peptideEvidences.put(pepEvidenceID, null);
                        
                    }                      
                }
            }
        }
        peptideEvidences = null; //free-up...
        
        writer.writeCharacters(indent(indentFactor));
        writer.writeEndElement();//</SequenceCollection>
        writer.writeCharacters("\n");
                
    }
    
    private void writeAnalysisCollection(int indentFactor, XMLStreamWriter writer, 
                                            HashMap<String,String> parametersMap) throws XMLStreamException{
         
        // ******************************************* //
        // AnalysisCollection
        // ******************************************* //
        
        System.out.println("\tWriting 'AnalysisCollection' document elements...");
        //AnalysisCollection
        writer.writeCharacters(indent(indentFactor));
        writer.writeStartElement("AnalysisCollection");
        writer.writeCharacters("\n");
        //AnalysisCollection/SpectrumIdentification
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeStartElement("SpectrumIdentification");
        writer.writeAttribute("id", "SI_1");
        writer.writeAttribute("spectrumIdentificationProtocol_ref", "SIP_1");
        writer.writeAttribute("spectrumIdentificationList_ref", "SIL_1");
        writer.writeAttribute("activityDate", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date()));
        writer.writeCharacters("\n");
        
        //AnalysisCollection/SpectrumIdentification/InputSpectra
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEmptyElement("InputSpectra");
        writer.writeAttribute("spectraData_ref", "SD_1");
        writer.writeCharacters("\n");
        
        //AnalysisCollection/SpectrumIdentification/SearchDatabaseRef
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEmptyElement("SearchDatabaseRef");
        writer.writeAttribute("searchDatabase_ref", "SDB_1");// parametersMap.get("FastaDatabase"));
        writer.writeCharacters("\n");

        writer.writeCharacters(indent(indentFactor + 2) );
        writer.writeEndElement();//</SpectrumIdentification>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor));
        writer.writeEndElement();//</AnalysisCollection>
        writer.writeCharacters("\n");
        
        
        // ********** skip ProteinDetection [default mode] ************ //
        
            
    }
    
    private void writeAnalysisProtocolCollection(int indentFactor, XMLStreamWriter writer,
                                                    HashMap<String,String> parametersMap,
                                                    HashMap<String,String> modTermMap) throws XMLStreamException{
        // ******************************************* //
        // AnalysisProtocolCollection: Protocol used for spectrum identification, most typical parameters
        // ******************************************* //
        System.out.println("\tWriting 'AnalysisProtocolCollection' document elements...");

        writer.writeCharacters(indent(indentFactor));
        writer.writeStartElement("AnalysisProtocolCollection");//</AnalysisCollection>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeStartElement("SpectrumIdentificationProtocol");//</AnalysisCollection>
        writer.writeAttribute("id", "SIP_1");
        writer.writeAttribute("analysisSoftware_ref", "PD");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/SearchType
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("SearchType");//<SearchType>
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef","PSI-MS");
        writer.writeAttribute("accession","MS:1001083");
        writer.writeAttribute("name","ms-ms search");
        writer.writeAttribute("value","");
        writer.writeEndElement();//</SearchType>
        writer.writeCharacters("\n");

        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("AdditionalSearchParams");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/cvParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001211");
        writer.writeAttribute("name", "parent mass type mono");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/cvParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001256");
        writer.writeAttribute("name", "fragment mass type mono");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/cvParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001118");
        writer.writeAttribute("name", "param: b ion");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/cvParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001262");
        writer.writeAttribute("name", "param: y ion");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: AvgPrecursorMzTolerance");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: ClassSizeMultiplier");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: CleavageRules");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: ComputeXCorr");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: DecoyPrefix");
        writer.writeAttribute("value","rev_");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: DynamicMods");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: EstimateSearchTimeOnly");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: FragmentMzTolerance");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: FragmentationAutoRule");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: FragmentationRule");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: KeepUnadjustedPrecursorMz");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxDynamicMods");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxFragmentChargeState");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxMissedCleavages");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxPeakCount");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxPeptideLength");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxPeptideMass");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxPeptideVariants");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MaxResultRank");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MinMatchedFragments");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MinPeptideLength");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MinPeptideMass");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MinResultScore");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MinTerminiCleavages");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MonoPrecursorMzTolerance");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: MonoisotopeAdjustmentSet");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: NumBatches");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: NumChargeStates");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: NumIntensityClasses");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: NumMzFidelityClasses");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: OutputFormat");
        writer.writeAttribute("value","mzIdentML");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: OutputSuffix");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: PrecursorMzToleranceRule");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: PreferIntenseComplements");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: ProteinDatabase");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: ProteinListFilters");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: ProteinSamplingTime");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: ResultsPerBatch");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: SpectrumListFilters");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: StaticMods");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: StatusUpdateFrequency");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: TicCutoffPercentage");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: UseMultipleProcessors");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: UseSmartPlusThreeModel");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "Config: WorkingDirectory");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: 1stQuartile: Filtered");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: 1stQuartile: Original");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: 2ndQuartile: Filtered");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: 2ndQuartile: Original");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: 3rdQuartile: Filtered");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: 3rdQuartile: Original");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: Mean: Filtered");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: Mean: Original");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: Min/Max: Filtered");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "PeakCounts: Min/Max: Original");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "SearchEngine: Name");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "SearchEngine: Version");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "SearchStats: Nodes");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "SearchStats: Overall");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "SearchTime: Duration");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "SearchTime: Started");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/userParams
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "SearchTime: Stopped");
        writer.writeAttribute("value","");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//</AdditionalSearchParams>
        writer.writeCharacters("\n");
        
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("ModificationParams");
        writer.writeCharacters("\n");
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification
        //getDynamicModifications
        ArrayList<MSFSearchModificationParam> dModfxns = getSearchModificationParams(parametersMap,"Dyn");
        //  for each dynamic modification returned, 
        //  make a modification node with the attending properties
        Iterator<MSFSearchModificationParam> modsItr = dModfxns.iterator();
        while(modsItr.hasNext()){
            MSFSearchModificationParam mod = modsItr.next();
            writer.writeCharacters(indent(indentFactor + 6));
            writer.writeStartElement("SearchModification");

            String residue = mod.getResidue();
            if(residue.equalsIgnoreCase("AnyN-Terminus")){
                residue = "";
            }
            writer.writeAttribute("fixedMod", "false"); // dynamic
            writer.writeAttribute("massDelta", String.valueOf(mod.getDeltaMass()));
            writer.writeAttribute("residues", residue);
            writer.writeCharacters("\n");
            if(residue.equalsIgnoreCase("AnyN-Terminus")){
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification/SpecificityRules
                writer.writeCharacters(indent(indentFactor + 8));
                writer.writeStartElement("SpecificityRules");
                writer.writeEmptyElement("cvParam");
                writer.writeAttribute("cvRef","PSI-MS");
                writer.writeAttribute("accession","MS:1001189");
                writer.writeAttribute("name","modification specificity N-term");
                writer.writeAttribute("value","");
                writer.writeEndElement(); //</SpecificityRules>
                writer.writeCharacters("\n");
            }
            
            //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification/cvParam
            writer.writeCharacters(indent(indentFactor + 8));
            writer.writeEmptyElement("cvParam");
            writer.writeAttribute("cvRef", "UNIMOD");
            writer.writeAttribute("accession",(new StringBuilder()).append("UNIMOD:").append(modTermMap.get(mod.getName())).toString());
            writer.writeAttribute("name", mod.getName());
            writer.writeAttribute("value","");
            writer.writeCharacters("\n"); 
            
            writer.writeCharacters(indent(indentFactor + 6));
            writer.writeEndElement(); //</SearchModification>
            writer.writeCharacters("\n"); 
       
        }                    
        //getStaticModifications;
        ArrayList<MSFSearchModificationParam> sModfxns = getSearchModificationParams(parametersMap,"Stat");
        //  for each static modification returned, 
        //  make a modification node with the attending properties
        Iterator<MSFSearchModificationParam> statModsItr = sModfxns.iterator();
        while(statModsItr.hasNext()){
            MSFSearchModificationParam mod = statModsItr.next();
            writer.writeCharacters(indent(indentFactor + 6));
            writer.writeStartElement("SearchModification");

            String residue = mod.getResidue();
            if(residue.equalsIgnoreCase("AnyN-Terminus")){
                residue = "";
            }
            writer.writeAttribute("fixedMod", "true"); // dynamic
            writer.writeAttribute("massDelta", String.valueOf(mod.getDeltaMass()));
            writer.writeAttribute("residues", residue);
            writer.writeCharacters("\n");
            if(residue.equalsIgnoreCase("AnyN-Terminus")){
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification/SpecificityRules
                writer.writeCharacters(indent(indentFactor + 8));
                writer.writeStartElement("SpecificityRules");
                writer.writeEmptyElement("cvParam");
                writer.writeAttribute("cvRef","PSI-MS");
                writer.writeAttribute("accession","MS:1001189");
                writer.writeAttribute("name","modification specificity N-term");
                writer.writeAttribute("value","");
                writer.writeEndElement(); //</SpecificityRules>
                writer.writeCharacters("\n");
            }
            
            //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification/cvParam
            writer.writeCharacters(indent(indentFactor + 8));
            writer.writeEmptyElement("cvParam");
            writer.writeAttribute("cvRef", "UNIMOD");
            writer.writeAttribute("accession",(new StringBuilder()).append("UNIMOD:").append(modTermMap.get(mod.getName())).toString());
            writer.writeAttribute("name", mod.getName());
            writer.writeAttribute("value","");
            writer.writeCharacters("\n"); ; 
            
            writer.writeCharacters(indent(indentFactor + 6));
            writer.writeEndElement(); //</SearchModification>
            writer.writeCharacters("\n"); 
        }
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement(); //</ModificationParams>
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Enzymes
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("Enzymes");
        writer.writeAttribute("independent","false"); //defaults to this **** may need to re-adjust code to accommodate multi-enzyme PD searches 
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Enzymes/Enzyme
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeStartElement("Enzyme");
        writer.writeAttribute("id","ENZ_1"); // optionally increase the digit for subsequent enzymes reported
        //for Myrimatch-like mzid compatibility
        writer.writeAttribute("cTermGain","OH"); //defaults
        writer.writeAttribute("nTermGain","H"); //defaults
        writer.writeAttribute("missedCleavages", getMissedCleavages(parametersMap));
        writer.writeAttribute("minDistance","1");//defaults
        String enzSpecific = "false";
        if(enzymeIsSpecific(parametersMap)){
            enzSpecific = "true";
        }
        writer.writeAttribute("semiSpecific",enzSpecific);
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Enzymes/Enzyme/SiteRegexp
        // --------------------------------------- //
        String enzname = getEnzyme(parametersMap);
        if(enzname.equalsIgnoreCase("Trypsin")){
            enzname = "Trypsin/P"; //it appears thermoMSF files Trypsin actually means Trypsin/P
        }
        // --------------------------------------- //
        
        writer.writeCharacters(indent(indentFactor + 8));
        writer.writeStartElement("SiteRegexp");
        writer.writeCharacters(getSiteRegExp(enzname)); //a place holder for enzyme site specificity - defaults to Trypsin
        writer.writeEndElement();//</SiteRegexp>
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Enzymes/Enzyme/EnzymeName
        writer.writeCharacters(indent(indentFactor + 8));
        writer.writeStartElement("EnzymeName");
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef","PSI-MS");
        writer.writeAttribute("accession",getEnzPSIAcc(enzname));
        writer.writeAttribute("name",enzname);
        writer.writeAttribute("value","");
        writer.writeEndElement();//</EnzymeName>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEndElement();//<Enzyme>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//<Enzymes>
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/MassTable
        /*
         * MS:1001494 (no threshold) only once
           MS:1001448 (pep:FDR threshold)
         */
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("MassTable");
        writer.writeAttribute("id", "MT");
        writer.writeAttribute("msLevel", "1 2 3");
        writer.writeCharacters("\n");
        // ----------------------------------------------------------------------------------- // 
        MSFPSMMasses msfMasses = new MSFPSMMasses();
        HashMap<Character,Double> massTable = msfMasses.getMassTable();
        Set<Map.Entry<Character,Double>> mTMapEntries = massTable.entrySet();
        Iterator<Map.Entry<Character,Double>> mTMapEntriesItr = mTMapEntries.iterator();
        // ----------------------------------------------------------------------------------- // 
        while(mTMapEntriesItr.hasNext()){
            Map.Entry<Character,Double> mTMapEntry = mTMapEntriesItr.next();
            char residue = mTMapEntry.getKey();
            double monoisotopicMass = mTMapEntry.getValue();
            //AnalysisProtocolCollection/SpectrumIdentificationProtocol/MassTable/Residue
            writer.writeCharacters(indent(indentFactor + 6));
            writer.writeEmptyElement("Residue");
            writer.writeAttribute("code", String.valueOf(residue));
            writer.writeAttribute("mass", String.valueOf(monoisotopicMass));
            writer.writeCharacters("\n");
        }
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//</MassTable>
        writer.writeCharacters("\n");
        
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("FragmentTolerance");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001412");
        writer.writeAttribute("name", "search tolerance plus value");
        writer.writeAttribute("value", getIonTolerance(parametersMap,"FragmentTolerance"));
        writer.writeAttribute("unitCvRef", "UO");
        writer.writeAttribute("unitAccession", "UO:0000221");
        writer.writeAttribute("unitName", "dalton"); //may need to automatically figure this out from map
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001413");
        writer.writeAttribute("name", "search tolerance minus value");
        writer.writeAttribute("value", getIonTolerance(parametersMap,"FragmentTolerance"));
        writer.writeAttribute("unitCvRef", "UO");
        writer.writeAttribute("unitAccession", "UO:0000221");
        writer.writeAttribute("unitName", "dalton"); //may need to automatically figure this out from map
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//</FragmentTolerance>
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ParentTolerance
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("ParentTolerance");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001412");
        writer.writeAttribute("name", "search tolerance plus value");
        writer.writeAttribute("value", getIonTolerance(parametersMap,"PeptideTolerance"));
        writer.writeAttribute("unitCvRef", "UO");
        writer.writeAttribute("unitAccession", "UO:0000169"); //may need to automatically figure this out from map
        writer.writeAttribute("unitName", "parts per million"); //may need to automatically figure this out from map
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001413");
        writer.writeAttribute("name", "search tolerance minus value");
        writer.writeAttribute("value", getIonTolerance(parametersMap, "PeptideTolerance"));
        writer.writeAttribute("unitCvRef", "UO");
        writer.writeAttribute("unitAccession", "UO:0000169"); //may need to automatically figure this out from map
        writer.writeAttribute("unitName", "parts per million"); //may need to automatically figure this out from map
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//</PeptideTolerance>
        writer.writeCharacters("\n");
        
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Threshold - Threshold value must be included, even if no threshold. This can include FDR, p-value etc
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("Threshold");
        writer.writeCharacters("\n");
        
        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Threshold/cvParam 
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001494");
        writer.writeAttribute("name", "no threshold");
        writer.writeAttribute("value", "");
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//</Threshold>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeEndElement();//</SpectrumIdentificationProtocol>
        writer.writeCharacters("\n");
        
        
        
        // ************* 'ProteinDetectionProtocol' ************************* //
        // by default however, if (includeProteinsInferred) is 'true',
        /*
        if(includeProteinsInferred){

            //module stump
            //AnalysisProtocolCollection/ProteinDetectionProtocol
            ProteinDetectionProtocol.setAttribute("analysisSoftware_ref", "PD");
            ProteinDetectionProtocol.setAttribute("id", "PDP_1"); //Protein Detection Protocol 1
                //AnalysisProtocolCollection/ProteinDetectionProtocol/AnalysisParams
                Element AnalysisParams = doc.createElement("AnalysisParams");
                    //AnalysisProtocolCollection/ProteinDetectionProtocol/AnalysisParams/cvParam
                    Element pDPAnalysisParCVParam = doc.createElement("cvParam");
                    pDPAnalysisParCVParam.setAttribute("cvRef", "PSI-MS");
                    pDPAnalysisParCVParam.setAttribute("accession", "MS:1001676");
                    pDPAnalysisParCVParam.setAttribute("name", "ProteomeDiscoverer:Maximum Protein References Per Peptide");
                    pDPAnalysisParCVParam.setAttribute("value", "100");
                AnalysisParams.appendChild(pDPAnalysisParCVParam);
            ProteinDetectionProtocol.appendChild(AnalysisParams);

                Element Threshold = doc.createElement("Threshold");
                    Element thresholdCVParam = doc.createElement("cvParam");
                    thresholdCVParam.setAttribute("cvRef", "PSI-MS");
                    thresholdCVParam.setAttribute("accession", "MS:1001447");
                    thresholdCVParam.setAttribute("name", "prot:FDR threshold");
                    thresholdCVParam.setAttribute("value", "1.5"); //hard-coded
                Threshold.appendChild(thresholdCVParam);
                ProteinDetectionProtocol.appendChild(Threshold);                      

            AnalysisProtocolCollection.appendChild(ProteinDetectionProtocol);

        }
        * 
        */  
        
        writer.writeCharacters(indent(indentFactor));
        writer.writeEndElement();//</AnalysisProtocolCollection>
        writer.writeCharacters("\n");
            
    }
    
    private void writeDataCollection(int indentFactor, XMLStreamWriter writer,
                                        HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap,
                                        HashMap<String,String> parametersMap, 
                                        HashMap<Integer,LinkedList<Integer>> spectID2PepIDsMap,
                                        //String mzidMLOutFile, 
                                        String searchDBPath, 
                                        HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap) throws XMLStreamException{
        // ******************************************* //
        // DataCollection
        // ******************************************* //
        System.out.println("\tWriting 'DataCollection' document elements...");
        
        //DataCollection/
        writer.writeCharacters(indent(indentFactor));
        writer.writeStartElement("DataCollection");//</AnalysisProtocolCollection>
        writer.writeCharacters("\n");

        //DataCollection/Inputs
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeStartElement("Inputs");
        writer.writeCharacters("\n");
        
        //DataCollection/Inputs/SearchDatabase
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("SearchDatabase");
        writer.writeAttribute("id", "SDB_1");
        writer.writeAttribute("name", new File(searchDBPath).getName());
        writer.writeAttribute("location", searchDBPath); 
        writer.writeCharacters("\n");
        //writer.writeAttribute("numDatabaseSequences", String.valueOf(db.getProteins().size()));
        
        //DataCollection/Inputs/SearchDatabase/FileFormat
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeStartElement("FileFormat");
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1001348");
        writer.writeAttribute("name", "FASTA format");
        writer.writeAttribute("value", "");
        writer.writeEndElement(); //</FileFormat>
        writer.writeCharacters("\n");
        
        
        /*
            //DataCollection/Inputs/SearchDatabase/cvParam/@accession
        */
        
        
        /*
            //DataCollection/Inputs/SourceFile/
            
        * 
        */
          
        //DataCollection/Inputs/SearchDatabase/DatabaseName
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeStartElement("DatabaseName");
        writer.writeEmptyElement("userParam");
        writer.writeAttribute("name", "database name");
        writer.writeAttribute("value", new File(searchDBPath).getName());
        writer.writeAttribute("type", "xsd:string");
        writer.writeEndElement(); //</DatabaseName>
        writer.writeCharacters("\n");
        
        //DataCollection/Inputs/SearchDatabase/cvParam
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef","PSI-MS");
        writer.writeAttribute("accession","MS:1001073");
        writer.writeAttribute("name","database type amino acid");
        writer.writeAttribute("value","");
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement(); //</SearchDatabase>
        writer.writeCharacters("\n");
                

        //DataCollection/Inputs/SpectraData
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("SpectraData");
        writer.writeAttribute("id","SD_1"); //spectra ID as the .RAW file name
        writer.writeAttribute("name", new File(parametersMap.get("SpectrumFileNames")).getName()); //spectra ID as the .RAW file name
        writer.writeAttribute("location",parametersMap.get("SpectrumFileNames"));
        writer.writeCharacters("\n");
        
        //DataCollection/Inputs/SpectraData/FileFormat
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeStartElement("FileFormat");
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1000563");
        writer.writeAttribute("name", "Thermo RAW file");
        writer.writeAttribute("value", "");
        writer.writeEndElement(); //</FileFormat>
        writer.writeCharacters("\n");
        
        //DataCollection/Inputs/SpectraData/SpectrumIDFormat
        writer.writeCharacters(indent(indentFactor + 6));
        writer.writeStartElement("SpectrumIDFormat");
        writer.writeEmptyElement("cvParam");
        writer.writeAttribute("cvRef", "PSI-MS");
        writer.writeAttribute("accession", "MS:1000768");
        writer.writeAttribute("name", "Thermo nativeID format");
        writer.writeAttribute("value", "");
        writer.writeEndElement(); //</SpectrumIDFormat>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//</SpectraData>
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeEndElement();//</Inputs>
        writer.writeCharacters("\n");
        
        //DataCollection/AnalysisData/
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeStartElement("AnalysisData");
        writer.writeCharacters("\n");

        

        //DataCollection/AnalysisData/SpectrumIdentificationList
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeStartElement("SpectrumIdentificationList");
        writer.writeAttribute("id", "SIL_1"); //SIL - Spectrum identification List.
        //writer.writeAttribute("numSequencesSearched", "");
        writer.writeCharacters("\n");
        
        Set<Integer> spectraIDs = spectID2PepIDsMap.keySet();
        // order spectra IDs 
        ArrayList<Integer> spectraIDsArrList = new ArrayList<Integer>(spectraIDs);
        Collections.sort(spectraIDsArrList);
        // iterate through spectraIDs
        Iterator<Integer> itr5 = spectraIDs.iterator();
        while(itr5.hasNext()){
            int spectrumID = itr5.next();
            String spectrumIdResultID = (new StringBuilder()).append("SIR_").append(spectrumID).toString();
            MSFPSMPeptide sampleSpectraPeptide = null;
            
            if(spectID2PepIDsMap.get(spectrumID) != null){
                
                LinkedList<Integer> mappedPepIDs = spectID2PepIDsMap.get(spectrumID);
                LinkedList<MSFPSMPeptide> mappedPeps = extractMappedPeptides(mappedPepIDs, pepID2MSFPeptideMap);
                MSFPSMPeptideRankQueue mappedPepsQueue = new MSFPSMPeptideRankQueue(mappedPeps);
                
                sampleSpectraPeptide = mappedPepsQueue.peek(); // the toped ranked peptide
                int spectraScanNumber = sampleSpectraPeptide.getFirstScan();
                String SIRSpectrumID = 
                        (new StringBuilder()).append("controllerType=0 controllerNumber=1 scan=").append(spectraScanNumber).toString();
                
                //DataCollection/AnalysisData/SpectrumIdentificationList/SpectrumIdentificationResult
                //if at least one spectra-mapped peptide has at least a peptideEvidence mapping
                LinkedList<Integer> mappedPeptideIDs = spectID2PepIDsMap.get(spectrumID);
                boolean areNulls = true;
                for(int i = 0; i < mappedPeptideIDs.size(); i++){
                    //int mappedPepID = mappedPeptideIDs.get(i);
                    //MSFPSMPeptide msfPep = pepID2MSFPeptideMap.get(mappedPeptideIDs.get(i));
                    //LinkedList<Peptide> pepinstances = pepID2PeptideInstancesMap.get(pepID2MSFPeptideMap.get(mappedPeptideIDs.get(i)).getPeptideID());
                    if(pepID2PeptideInstancesMap.get(pepID2MSFPeptideMap.get(mappedPeptideIDs.get(i)).getPeptideID()) != null){
                        areNulls = false;
                        break;
                    }
                }
               
                if(areNulls==false){
                    writer.writeCharacters(indent(indentFactor + 6));
                    writer.writeStartElement("SpectrumIdentificationResult");
                    writer.writeAttribute("id", spectrumIdResultID);
                    writer.writeAttribute("spectrumID", SIRSpectrumID);
                    writer.writeAttribute("spectraData_ref", "SD_1");
                    writer.writeCharacters("\n");

                    MSFPSMPeptide[] mappedPepsQueueMappedPepsArr = mappedPepsQueue.getPriorityQueue();
                    for(int i = 0; i < mappedPepsQueueMappedPepsArr.length; i++){
                        //int mappedPepID = mappedPeptideIDs.get(i);
                        MSFPSMPeptide msfPep = mappedPepsQueueMappedPepsArr[i];
                        String referencePepID = (new StringBuilder()).append("PEP_").append(msfPep.getPeptideID()).toString();


                        //Get peptide evidence references
                        LinkedList<Peptide> pepinstances = pepID2PeptideInstancesMap.get(msfPep.getPeptideID());
                        if(pepinstances != null){
                            //DataCollection/AnalysisData/SpectrumIdentificationList/SpectrumIdentificationResult/SpectrumIdentificationItem
                            writer.writeCharacters(indent(indentFactor + 8));
                            writer.writeStartElement("SpectrumIdentificationItem");
                            boolean passThreshold = true; // default value for no threshold filtering...
                            MSFPSMMasses msfPepMasses = new MSFPSMMasses(msfPep);

                            writer.writeAttribute("id",(new StringBuilder()).append(spectrumIdResultID).append("_").append("SII_").append(i).toString());
                            writer.writeAttribute("rank",String.valueOf(msfPep.getSearchEngineRank()));
                            writer.writeAttribute("chargeState", String.valueOf(msfPep.getCharge()));
                            writer.writeAttribute("peptide_ref",referencePepID);
                            writer.writeAttribute("experimentalMassToCharge",String.valueOf(msfPepMasses.getObservedMassToCharge()));
                            writer.writeAttribute("calculatedMassToCharge",String.valueOf(msfPepMasses.getTheoreticalMassToCharge()));
                            writer.writeAttribute("passThreshold",String.valueOf(passThreshold));
                            writer.writeAttribute("massTable_ref","MT");
                            writer.writeCharacters("\n");


                        //Get peptide evidence references
                        //LinkedList<Peptide> pepinstances = pepID2PeptideInstancesMap.get(msfPep.getPeptideID());
                        //if(pepinstances != null){
                            for(int j = 0; j < pepinstances.size(); j++){
                                Peptide pepinstance = pepinstances.get(j);
                                String referenceProtID = (new StringBuilder()).append("DBSeq_").append(pepinstance.getMSFProteinParent().getMyriMatchDescription()).toString();

                                //DataCollection/AnalysisData/SpectrumIdentificationList/SpectrumIdentificationResult/SpectrumIdentificationItem/PeptideEvidenceRef
                                writer.writeCharacters(indent(indentFactor + 10));
                                writer.writeEmptyElement("PeptideEvidenceRef");
                                writer.writeAttribute("peptideEvidence_ref", 
                                        (new StringBuilder()).append(referenceProtID).append("_").append(referencePepID).toString());
                                writer.writeCharacters("\n");

                            }
                        //}

                            //add PD derived scores
                            //DataCollection/AnalysisData/SpectrumIdentificationList/SpectrumIdentificationResult/SpectrumIdentificationItem/cvParam
                            writer.writeCharacters(indent(indentFactor + 10));
                            writer.writeEmptyElement("cvParam");
                            writer.writeAttribute("cvRef", "PSI-MS");
                            writer.writeAttribute("accession", "MS:1001155");
                            writer.writeAttribute("name", "SEQUEST:xcorr");
                            writer.writeAttribute("value", String.valueOf(msfPep.getXCorr()));
                            writer.writeCharacters("\n");

                            //DataCollection/AnalysisData/SpectrumIdentificationList/SpectrumIdentificationResult/SpectrumIdentificationItem/cvParam
                            writer.writeCharacters(indent(indentFactor + 10));
                            writer.writeEmptyElement("cvParam");
                            writer.writeAttribute("cvRef", "PSI-MS");
                            writer.writeAttribute("accession", "MS:1001215");
                            writer.writeAttribute("name", "SEQUEST:PeptideSp");
                            writer.writeAttribute("value", String.valueOf(msfPep.getSpScore()));
                            writer.writeCharacters("\n");

                            writer.writeCharacters(indent(indentFactor + 8));
                            writer.writeEndElement();//</"SpectrumIdentificationItem">
                            writer.writeCharacters("\n");

                        }

                    }

                    writer.writeCharacters(indent(indentFactor + 6));
                    writer.writeEndElement();//</"SpectrumIdentificationResult">
                    writer.writeCharacters("\n");
                }
                               
            }               
        }
        
        writer.writeCharacters(indent(indentFactor + 4));
        writer.writeEndElement();//</"SpectrumIdentificationList">    
        writer.writeCharacters("\n");
        
        // ***************** Skipping Proteins [default] *********************** //
        
        writer.writeCharacters(indent(indentFactor + 2));
        writer.writeEndElement();//</"AnalysisData">    
        writer.writeCharacters("\n");
        
        writer.writeCharacters(indent(indentFactor));
        writer.writeEndElement();//</"DataCollection">    
        writer.writeCharacters("\n");

    }
    
    // *********************** //
    //  Helper Methods
    // *********************** //
    
    private ArrayList<MSFSearchModificationParam> getSearchModificationParams(HashMap<String,String> parametersMap,
                                                                            String pattern) {
        //throw new UnsupportedOperationException("Not yet implemented");
        ArrayList<MSFSearchModificationParam> mods = new ArrayList<MSFSearchModificationParam>();
        Set<String> keys = parametersMap.keySet();
        //Iterate through parameterMap keys, if a keymatches DynMod, get its value from the parameters map
        Iterator<String> itr = keys.iterator();
        while(itr.hasNext()){
            String key = itr.next();
            if(key.startsWith(pattern)){
                String mappedValue = parametersMap.get(key); // true modification value is represented by 
                            // modificationName/massDifference Da (modificationAminoAcid(s) separated by comma)    
                // check if the returned value contains '/' and the 'Da' character strings
                if(mappedValue.contains("/") && mappedValue.contains("Da")){
                    String[] valueArr = mappedValue.split("/"); //splits string to moficationName and massDiff+Residue(s)
                    String modName = valueArr[0].replaceAll("\\s+", ""); //removes all white spaces, get the modification name
                    String[] dMassNresidues = valueArr[1].split("Da");
                    double dMass = Double.parseDouble(dMassNresidues[0].replaceAll("\\s+",""));
                    String residues = dMassNresidues[1].replace("(","").replace(")","").replaceAll("\\s+","");
                    //in situations where more than one residues are associated with a modification,
                    String[] residueArr = residues.split(",");
                    for(String residue : residueArr){
                        mods.add(new MSFSearchModificationParam(modName,dMass,residue));
                    }
                }
            }
        }
        return mods;          
    }

    private String getIonTolerance(HashMap<String, String> parametersMap, String key) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String ionTolerance = 
                parametersMap.get(key).replace("Da","").replace("ppm","").replaceAll("\\s+","");       
        return ionTolerance;
    }

    private ArrayList<MSFPSMProtein> getUniqueMappedMSFProteins(HashMap<Integer, LinkedList<Peptide>> pepID2PeptideInstancesMap) {
        ArrayList<MSFPSMProtein> mappedProts = new ArrayList<MSFPSMProtein>();
        
        Set<Map.Entry<Integer,LinkedList<Peptide>>> mappings = pepID2PeptideInstancesMap.entrySet();
        Iterator<Map.Entry<Integer, LinkedList<Peptide>>> itr = mappings.iterator();
        while(itr.hasNext()){
            Map.Entry<Integer, LinkedList<Peptide>> pepID2Instances = itr.next();
            LinkedList<Peptide> instances = pepID2Instances.getValue();
            Iterator<Peptide> itr2 = instances.iterator();
            while(itr2.hasNext()){
                Peptide pep = itr2.next();
                MSFPSMProtein msfProt = pep.getMSFProteinParent();
                if(inList(msfProt, mappedProts)==false){
                    mappedProts.add(msfProt);
                }
            }
        }       
        return mappedProts;
    }
    
    private boolean inList(MSFPSMProtein msfProt, ArrayList<MSFPSMProtein> mappedProts) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean in = false;
        Iterator<MSFPSMProtein> itr = mappedProts.iterator();
        while(itr.hasNext()){
            MSFPSMProtein msfProt2 = itr.next();
            if(msfProt2.getDescription().equals(msfProt.getDescription())){
                in = true;
                return in;                
            }
        }        
        return in;
    }

    private String indent(int indentation) {
        //throw new UnsupportedOperationException("Not yet implemented");
        StringBuilder ind = new StringBuilder();
        for(int i = 0; i < indentation; i++){
            ind.append(" ");
        }
        return ind.toString();       
    }
    
    private boolean enzymeIsSpecific(HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean specific = false;
        String enz = parametersMap.get("Enzyme");
        enz = enz.split(" ")[1];
        if(enz.equalsIgnoreCase("(Full)")){
            specific = true;
        }
        return specific;
    }
    
    
    
    private String getSiteRegExp(String enz){
        String regExp = "";
        HashMap<String, String> siteRegExps = new HashMap<String,String>();
        siteRegExps.put("Trypsin", "(?<=[KR])(?!P)");
        siteRegExps.put("TrypChymo", "(?<=[FYWLKR])(?!P)");
        siteRegExps.put("Trypsin/P", "(?<=[KR])"); //it appears thermoMSF files Trypsin actually means Trypsin/P
        siteRegExps.put("V8-DE", "(?<=[BDEZ])(?!P)");
        siteRegExps.put("V8-E", "(?<=[EZ])(?!P)");
        siteRegExps.put("PepsinA", "(?<=[FL])");
        siteRegExps.put("Lys-C/P", "(?<=K)");
        siteRegExps.put("Lys-C", "((?<=K)(?!P)");
        siteRegExps.put("Formic_acid", "((?<=D))|((?=D))");
        siteRegExps.put("CNBr", "(?<=M)");
        siteRegExps.put("ChymoTrypsin", "(?<=[FYWL])(?!P)");
        siteRegExps.put("Asp-N_ambic", "(?=[DE])");
        siteRegExps.put("Asp-N", "(?=[BD])");
        siteRegExps.put("Arg-C", "(?<=R)(?!P)");
               
        regExp = siteRegExps.get(enz);       
        return regExp;
    }
    
    private String getEnzyme(HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String enz = parametersMap.get("Enzyme");
        enz = enz.split(" ")[0];
        return enz;
    }

    private String getEnzPSIAcc(String enz) {
        String acc = "";
        HashMap<String, String> psioboAcc = new HashMap<String,String>();
        psioboAcc.put("Trypsin", "MS:1001251");
        psioboAcc.put("TrypChymo", "MS:1001312");
        psioboAcc.put("Trypsin/P", "MS:1001313"); //it appears thermoMSF files Trypsin actually means Trypsin/P
        psioboAcc.put("V8-DE", "MS:1001314");
        psioboAcc.put("V8-E", "MS:1001315");
        psioboAcc.put("PepsinA", "MS:1001311");
        psioboAcc.put("Lys-C/P", "MS:1001310");
        psioboAcc.put("Lys-C", "MS:1001309");
        psioboAcc.put("Formic_acid", "MS:1001308");
        psioboAcc.put("CNBr", "MS:1001307");
        psioboAcc.put("ChymoTrypsin", "MS:1001306");
        psioboAcc.put("Asp-N_ambic", "MS:1001305");
        psioboAcc.put("Asp-N", "MS:1001304");
        psioboAcc.put("Arg-C", "MS:1001303");
               
        acc = psioboAcc.get(enz);       
        return acc;
    }
    
    private LinkedList<MSFPSMPeptide> extractMappedPeptides(LinkedList<Integer> mappedPepIDs, 
                                                    HashMap<Integer, MSFPSMPeptide> pepID2MSFPeptideMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        LinkedList<MSFPSMPeptide> mappedPeps = new LinkedList<MSFPSMPeptide>();
        for(int mappedPepID: mappedPepIDs){
            MSFPSMPeptide mappedPep = pepID2MSFPeptideMap.get(mappedPepID);
            mappedPeps.add(mappedPep);
        }
        return mappedPeps;
    }
    
    private String getMissedCleavages(HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String mC = "";
        if(parametersMap.containsKey("MissedCleavages")){
            mC = parametersMap.get("MissedCleavages");           
        }else if(parametersMap.containsKey("MaxMissedCleavages")){
            mC = parametersMap.get("MaxMissedCleavages");  
        }       
        return mC;
    }

    private String getSearchEngine(HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String sE = "";
        if(parametersMap.containsKey("Mascot")){
            sE = parametersMap.get("Mascot");           
        }else if(parametersMap.containsKey("SequestNode")){
            sE = parametersMap.get("SequestNode");  
        }else if(parametersMap.containsKey("IseNode")){
            sE = parametersMap.get("IseNode");
        }            
        return sE;
    }

    private String getSearchedDB(String searchDBPath, HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String sDb = "";
        String sDbName = new File(searchDBPath).getName();
        String msfSpecifiedDBName = parametersMap.get("DatabaseName");
        if(sDbName.equalsIgnoreCase(msfSpecifiedDBName)){
            sDb = searchDBPath; // cross-checks to ascertain the database specified in config file....
        } else {
            System.out.println("      WARNING: Database specified in 'M2Lite.config' file\n" +
                               "         doesn't match that found in MSF file...");
        }        
        return sDb;
    }
    
}
