/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package ios.printers;

import enumtypes.SearchDatabaseType;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfproteins.MSFPSMProtein;
import msfpsms.MSFPSMPeptide;
/**
 *
 * @author paiyeta1
 * @date 2013-Aug-09
 * 
 */
public class MSF2TextFilePrinter {
    
    //private final int NO_PROT_INFO = 1;
    private final int WITH_PROT_INFO = 2;
    
    public void print(MSFPSMPeptide[] msfPeps, String outFile, 
                            int peps2Print, boolean includeiTRAQ4plexValues,
                                HashMap<Integer,LinkedList<MSFPSMProtein>> peptideIDToProteinsMap){
        //PrintWriter printer = null;      
        try {
            
            PrintWriter printer;
            printer = new PrintWriter(outFile);
            //Iterator<MSFPSMPeptide> itr  = msfPeps.iterator();
            
            switch(peps2Print){
                
                case WITH_PROT_INFO: // case NO_PROT_INFO:
                    //Print header
                    if(includeiTRAQ4plexValues){
                        printer.println("Confidence Level" + "\t" +
                                            "Search ID" + "\t" + 
                                            "Processing Node No" + "\t" +
                                            "Sequence" + "\t" +
                                            "Unique Sequence ID" + "\t" +
                                            "Modifications" + "\t" +
                                            "Activation Type" + "\t" +
                                            "Search Engine Rank" + "\t" +
                                            "114" + "\t" +
                                            "115" + "\t" +
                                            "116" + "\t" +
                                            "117" + "\t" +
                                            "Score" + "\t" +
                                            "XCorr" + "\t" +
                                            //"SpScore" + "\t" + 
                                            "# Missed Cleavages" + "\t" +
                                            "Isolation Interference [%]" + "\t" +
                                            "Ion Injection Time [ms]" + "\t" +
                                            "Intensity" + "\t" +
                                            "Charge" + "\t" +
                                            "Mass" + "\t" +
                                            "MH+ [Da]" + "\t" + 
                                            "RT [min]" + "\t" +
                                            "First Scan" + "\t" +
                                            "Last Scan" + "\t" +
                                            "MS Order" + "\t" +
                                            "Matched Ions" + "\t" +
                                            "Total Ions" + "\t" +
                                            "Spectrum File" + "\t" +
                                            //"Annotation" + "\t" +
                                            "Proteins [Number]"+ "\t" +
                                            //"Master Protein"+ "\t" +
                                            "Proteins [Description(s)]");
                        //while (itr.hasNext()){
                        for(MSFPSMPeptide msfPep: msfPeps){
                            //MSFPSMPeptide msfPep = (MSFPSMPeptide) itr.next();
                            int peptideID = msfPep.getPeptideID();
                            LinkedList<MSFPSMProtein> mappedProts = peptideIDToProteinsMap.get(peptideID);
                                    

                            printer.println(msfPep.getConfidenceLevel() + 
                                            "\tA\t" + 
                                            msfPep.getProcessingNodeNumber() + "\t" +  
                                            msfPep.getSequence() + "\t" +  
                                            msfPep.getUniquePeptideSequenceID() + "\t" +  
                                            msfPep.toStringPepModifications() + "\t" +  
                                            msfPep.getActivationType() + "\t" + 
                                            msfPep.getSearchEngineRank() + "\t" +  
                                            msfPep.getChannel114() + "\t" + 
                                            msfPep.getChannel115() + "\t" + 
                                            msfPep.getChannel116() + "\t" + 
                                            msfPep.getChannel117() + "\t" + 
                                            msfPep.getScore() + "\t" +  
                                            msfPep.getXCorr() + "\t" +  
                                            //msfPep.getSpScore() + "\t" +  
                                            msfPep.getMissedCleavages() + "\t" + 
                                            msfPep.getPercentIsolationInterference() + "\t" +  
                                            msfPep.getIonInjectTime() + "\t" + 
                                            msfPep.getIntensity() + "\t" +  
                                            msfPep.getCharge() + "\t" + 
                                            msfPep.getMass() + "\t" + 
                                            msfPep.getMZ() + "\t" + 
                                            msfPep.getRetentionTime() + "\t" +  
                                            msfPep.getFirstScan() + "\t" + 
                                            msfPep.getLastScan() + "\t" + 
                                            msfPep.getMSLevel() + "\t" +
                                            msfPep.getMatchedIonsCount() + "\t" +  
                                            msfPep.getTotalIonsCount() + "\t" +  
                                            msfPep.getFileName() + "\t" + 
                                            //msfPep.getAnnotation() + "\t" + 
                                            //"Proteins (Number)"+ "\t" +
                                            //"Master Protein"+ "\t" +
                                            //"Proteins"
                                            getNumberOfMappedProteins(mappedProts) + "\t" + 
                                            //getMasterProteins(mappedProts) + "\t" +
                                            getMappedProteinsAsString(mappedProts)                         
                                    );
                        }
                    } else {
                        printer.println("Confidence Level" + "\t" +
                                            "Search ID" + "\t" + 
                                            "Processing Node No" + "\t" +
                                            "Sequence" + "\t" +
                                            "Unique Sequence ID" + "\t" +
                                            "Modifications" + "\t" +
                                            "Activation Type" + "\t" +
                                            "Search Engine Rank" + "\t" +
                                            //"114" + "\t" +
                                            //"115" + "\t" +
                                            //"116" + "\t" +
                                            //"117" + "\t" +
                                            "Score" + "\t" +
                                            "XCorr" + "\t" +
                                            //"SpScore" + "\t" + 
                                            "# Missed Cleavages" + "\t" +
                                            "Isolation Interference [%]" + "\t" +
                                            "Ion Injection Time [ms]" + "\t" +
                                            "Intensity" + "\t" +
                                            "Charge" + "\t" +
                                            "Mass" + "\t" +
                                            "MH+ [Da]" + "\t" + 
                                            "RT [min]" + "\t" +
                                            "First Scan" + "\t" +
                                            "Last Scan" + "\t" +
                                            "MS Order" + "\t" +
                                            "Matched Ions" + "\t" +
                                            "Total Ions" + "\t" +
                                            "Spectrum File" + "\t" +
                                            //"Annotation" + "\t" +
                                            "Proteins [Number]"+ "\t" +
                                            //"Master Protein"+ "\t" +
                                            "Proteins [Description(s)]");
                        //while (itr.hasNext()){
                        for(MSFPSMPeptide msfPep: msfPeps){
                            //MSFPSMPeptide msfPep = (MSFPSMPeptide) itr.next();
                            int peptideID = msfPep.getPeptideID();
                            LinkedList<MSFPSMProtein> mappedProts = peptideIDToProteinsMap.get(peptideID);
                            printer.println(msfPep.getConfidenceLevel() + 
                                            "\tA\t" + 
                                            msfPep.getProcessingNodeNumber() + "\t" +  
                                            msfPep.getSequence() + "\t" +  
                                            msfPep.getUniquePeptideSequenceID() + "\t" +  
                                            msfPep.toStringPepModifications() + "\t" +  
                                            msfPep.getActivationType() + "\t" + 
                                            msfPep.getSearchEngineRank() + "\t" +  
                                            //msfPep.getChannel114() + "\t" + 
                                            //msfPep.getChannel115() + "\t" + 
                                            //msfPep.getChannel116() + "\t" + 
                                            //msfPep.getChannel117() + "\t" + 
                                            msfPep.getScore() + "\t" +  
                                            msfPep.getXCorr() + "\t" +  
                                            //msfPep.getSpScore() + "\t" +  
                                            msfPep.getMissedCleavages() + "\t" + 
                                            msfPep.getPercentIsolationInterference() + "\t" +  
                                            msfPep.getIonInjectTime() + "\t" + 
                                            msfPep.getIntensity() + "\t" +  
                                            msfPep.getCharge() + "\t" + 
                                            msfPep.getMass() + "\t" + 
                                            msfPep.getMZ() + "\t" + 
                                            msfPep.getRetentionTime() + "\t" +  
                                            msfPep.getFirstScan() + "\t" + 
                                            msfPep.getLastScan() + "\t" + 
                                            msfPep.getMSLevel() + "\t" +
                                            msfPep.getMatchedIonsCount() + "\t" +  
                                            msfPep.getTotalIonsCount() + "\t" +  
                                            msfPep.getFileName() + "\t" + 
                                            //msfPep.getAnnotation()  + "\t" +
                                            //"Proteins (Number)"+ "\t" +
                                            //"Master Protein"+ "\t" +
                                            //"Proteins"
                                            getNumberOfMappedProteins(mappedProts) + "\t" + 
                                            //getMasterProteins(mappedProts) + "\t" +
                                            getMappedProteinsAsString(mappedProts)                                  
                                    );
                        }
                        
                    }
                    break;
                         
                default: // case NO_PROT_INFO:
                    //Print header
                    if(includeiTRAQ4plexValues){
                        printer.println("Confidence Level" + "\t" +
                                            "Search ID" + "\t" + 
                                            "Processing Node No" + "\t" +
                                            "Sequence" + "\t" +
                                            "Unique Sequence ID" + "\t" +
                                            "Modifications" + "\t" +
                                            "Activation Type" + "\t" +
                                            "Search Engine Rank" + "\t" +
                                            "114" + "\t" +
                                            "115" + "\t" +
                                            "116" + "\t" +
                                            "117" + "\t" +
                                            "Score" + "\t" +
                                            "XCorr" + "\t" +
                                            //"SpScore" + "\t" + 
                                            "# Missed Cleavages" + "\t" +
                                            "Isolation Interference [%]" + "\t" +
                                            "Ion Injection Time [ms]" + "\t" +
                                            "Intensity" + "\t" +
                                            "Charge" + "\t" +
                                            "Mass" + "\t" +
                                            "MH+ [Da]" + "\t" + 
                                            "RT [min]" + "\t" +
                                            "First Scan" + "\t" +
                                            "Last Scan" + "\t" +
                                            "MS Order" + "\t" +
                                            "Matched Ions" + "\t" +
                                            "Total Ions" + "\t" +
                                            "Spectrum File" + "\t" +
                                            "Annotation");
                        //while (itr.hasNext()){
                        for(MSFPSMPeptide msfPep: msfPeps){
                            //MSFPSMPeptide msfPep = (MSFPSMPeptide) itr.next();

                            printer.println(msfPep.getConfidenceLevel() + 
                                            "\tA\t" + 
                                            msfPep.getProcessingNodeNumber() + "\t" +  
                                            msfPep.getSequence() + "\t" +  
                                            msfPep.getUniquePeptideSequenceID() + "\t" +  
                                            msfPep.toStringPepModifications() + "\t" +  
                                            msfPep.getActivationType() + "\t" + 
                                            msfPep.getSearchEngineRank() + "\t" +  
                                            msfPep.getChannel114() + "\t" + 
                                            msfPep.getChannel115() + "\t" + 
                                            msfPep.getChannel116() + "\t" + 
                                            msfPep.getChannel117() + "\t" + 
                                            msfPep.getScore() + "\t" +  
                                            msfPep.getXCorr() + "\t" +  
                                            //msfPep.getSpScore() + "\t" +  
                                            msfPep.getMissedCleavages() + "\t" + 
                                            msfPep.getPercentIsolationInterference() + "\t" +  
                                            msfPep.getIonInjectTime() + "\t" + 
                                            msfPep.getIntensity() + "\t" +  
                                            msfPep.getCharge() + "\t" + 
                                            msfPep.getMass() + "\t" + 
                                            msfPep.getMZ() + "\t" + 
                                            msfPep.getRetentionTime() + "\t" +  
                                            msfPep.getFirstScan() + "\t" + 
                                            msfPep.getLastScan() + "\t" + 
                                            msfPep.getMSLevel() + "\t" +
                                            msfPep.getMatchedIonsCount() + "\t" +  
                                            msfPep.getTotalIonsCount() + "\t" +  
                                            msfPep.getFileName() + "\t" + 
                                            msfPep.getAnnotation());
                        }
                    } else {
                        printer.println("Confidence Level" + "\t" +
                                            "Search ID" + "\t" + 
                                            "Processing Node No" + "\t" +
                                            "Sequence" + "\t" +
                                            "Unique Sequence ID" + "\t" +
                                            "Modifications" + "\t" +
                                            "Activation Type" + "\t" +
                                            "Search Engine Rank" + "\t" +
                                            //"114" + "\t" +
                                            //"115" + "\t" +
                                            //"116" + "\t" +
                                            //"117" + "\t" +
                                            "Score" + "\t" +
                                            "XCorr" + "\t" +
                                            //"SpScore" + "\t" + 
                                            "# Missed Cleavages" + "\t" +
                                            "Isolation Interference [%]" + "\t" +
                                            "Ion Injection Time [ms]" + "\t" +
                                            "Intensity" + "\t" +
                                            "Charge" + "\t" +
                                            "Mass" + "\t" +
                                            "MH+ [Da]" + "\t" + 
                                            "RT [min]" + "\t" +
                                            "First Scan" + "\t" +
                                            "Last Scan" + "\t" +
                                            "MS Order" + "\t" +
                                            "Matched Ions" + "\t" +
                                            "Total Ions" + "\t" +
                                            "Spectrum File" + "\t" +
                                            "Annotation");
                        //while (itr.hasNext()){
                        for(MSFPSMPeptide msfPep: msfPeps){
                            //MSFPSMPeptide msfPep = (MSFPSMPeptide) itr.next();

                            printer.println(msfPep.getConfidenceLevel() + 
                                            "\tA\t" + 
                                            msfPep.getProcessingNodeNumber() + "\t" +  
                                            msfPep.getSequence() + "\t" +  
                                            msfPep.getUniquePeptideSequenceID() + "\t" +  
                                            msfPep.toStringPepModifications() + "\t" +  
                                            msfPep.getActivationType() + "\t" + 
                                            msfPep.getSearchEngineRank() + "\t" +  
                                            //msfPep.getChannel114() + "\t" + 
                                            //msfPep.getChannel115() + "\t" + 
                                            //msfPep.getChannel116() + "\t" + 
                                            //msfPep.getChannel117() + "\t" + 
                                            msfPep.getScore() + "\t" +  
                                            msfPep.getXCorr() + "\t" +  
                                            //msfPep.getSpScore() + "\t" +  
                                            msfPep.getMissedCleavages() + "\t" + 
                                            msfPep.getPercentIsolationInterference() + "\t" +  
                                            msfPep.getIonInjectTime() + "\t" + 
                                            msfPep.getIntensity() + "\t" +  
                                            msfPep.getCharge() + "\t" + 
                                            msfPep.getMass() + "\t" + 
                                            msfPep.getMZ() + "\t" + 
                                            msfPep.getRetentionTime() + "\t" +  
                                            msfPep.getFirstScan() + "\t" + 
                                            msfPep.getLastScan() + "\t" + 
                                            msfPep.getMSLevel() + "\t" +
                                            msfPep.getMatchedIonsCount() + "\t" +  
                                            msfPep.getTotalIonsCount() + "\t" +  
                                            msfPep.getFileName() + "\t" + 
                                            msfPep.getAnnotation());
                        }
                        
                    }
                    break;
      
            } // end switch
            printer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MSF2TextFilePrinter.class.getName()).log(Level.SEVERE, null, ex);
        } // end try_catch       
    }
    
    public void print(MSFPSMProtein[] msfProts, HashMap<Integer,LinkedList<MSFPSMProtein>> proteinGrpID2MSFProteinsMap,
                            SearchDatabaseType dbtype, String outputFile) throws FileNotFoundException{       
        PrintWriter writer = new PrintWriter(outputFile);
        //print header
        writer.println("Protein_Accession\t" +
                        "Protein_Description\t" +
                        "Is_Master_Protein\t" +
                        "Protein_Group_Accession\t" + 
                        "Protein_Score\t" +
                        "Matched_Peptide_Sequence");
        // print body
        //Iterator<MSFPSMProtein> itr = msfProts.iterator();
        //while(itr.hasNext()){
        for(MSFPSMProtein prot:msfProts){
            //MSFPSMProtein prot = itr.next();
            String protGrpAcc = getProteinGroupAcc(prot.getProteinGroupID(),proteinGrpID2MSFProteinsMap);
            writer.println(prot.getAccession() + "\t" + 
                            prot.getCleanDescription() + "\t" +
                            prot.isMasterProtein() + "\t" + 
                            protGrpAcc + "\t" +
                            prot.getProteinScore() + "\t" +
                            prot.getMappedPeptideSequence());           
        }
        writer.close();
    }

    private String getProteinGroupAcc(int proteinGroupID, HashMap<Integer, LinkedList<MSFPSMProtein>> proteinGrpID2MSFProteinsMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String protGrpAcc = null;
        LinkedList<MSFPSMProtein> prots = proteinGrpID2MSFProteinsMap.get(proteinGroupID);
        Iterator<MSFPSMProtein> itr = prots.iterator();
        while(itr.hasNext()){
            MSFPSMProtein prot = itr.next();
            if(prot.isMasterProtein()){
                protGrpAcc = prot.getAccession();
            }
            break;
        }
        return protGrpAcc;
    }

    private String getNumberOfMappedProteins(LinkedList<MSFPSMProtein> mappedProts) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String num = "";
        if(mappedProts!=null){
            num = String.valueOf(mappedProts.size());
        }
        return num;
    }

    private String getMasterProteins(LinkedList<MSFPSMProtein> mappedProts) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String mPr = "";
        int index = 0;
        if(mappedProts != null){
            for(MSFPSMProtein mappedProt : mappedProts){
                if(mappedProt.isMasterProtein()){
                    //add to mPr
                    index++;
                    if(index==1){ //first Master Protein
                        mPr = mPr + mappedProt.getDescription();                      
                    } else {
                        mPr = mPr + "; " + mappedProt.getDescription(); 
                    }
                }     
            }
        }
        return mPr;
        
    }

    private String getMappedProteinsAsString(LinkedList<MSFPSMProtein> mappedProts) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String mPr = "";
        int index = 0;
        if(mappedProts != null){
            for(MSFPSMProtein mappedProt : mappedProts){
                 //add to mPr
                index++;
                if(index==1){ //first Master Protein
                    mPr = mPr + mappedProt.getDescription();                      
                } else {
                    mPr = mPr + "; " + mappedProt.getDescription(); 
                }               
            }
        }
        return mPr;
    }
    
}
