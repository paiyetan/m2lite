/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 */
package ios.printers;

import database.Database;
import database.Peptide;
import enumtypes.SearchDatabaseType;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import msfparser.MSFSearchModificationParam;
import msfproteins.MSFPSMProtein;
import msfpsms.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author paiyeta1
 */
public class MSF2PepXMLPrinter {
    
    HashMap<String, Double> aAcidMassTable;
    @SuppressWarnings("CallToThreadDumpStack")
    public void write(HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap,
                      HashMap<String,String> parametersMap, 
                      HashMap<String,String> modTermMap, 
                      HashMap<Integer,LinkedList<Integer>> spectID2PepIDsMap,
                      String pepXMLOutFile, 
                      //Database db,
                      String searchDBPath, 
                      SearchDatabaseType dbtype, 
                      boolean includeProteinsInferred,
                      HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap,
                      boolean tppCompatibleOutput) {
        
        aAcidMassTable = getAminoAcidMassTable();
        
        
        try {
            //throw new UnsupportedOperationException("Not yet implemented");
            //create header and root, and level 1 elements
            System.out.println(" Making '.pepXML'/'pep.xml' file...");
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            System.out.println(" Making msms_pipeline_analysis (root) node...");
            Element rootElement = doc.createElement("msms_pipeline_analysis");
            //SimpleDateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            String time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date());
            rootElement.setAttribute("date",time);
            rootElement.setAttribute("xmlns", "http://regis-web.systemsbiology.net/pepXML");
            rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
            rootElement.setAttribute("xsi:schemaLocation", "http://sashimi.sourceforge.net/schema_revision/pepXML/pepXML_v117.xsd");
            rootElement.setAttribute("summary_xml",pepXMLOutFile);
            //rootElement.setAttribute("raw_data_type", "raw");
            //rootElement.setAttribute("raw_data","");
           
            doc.appendChild(rootElement);
            //daughter elements
            /*
            <analysis_summary analysis="MyriMatch" version="2.1.138" time="2013-10-21T21:38:07"/>
            <msms_run_summary base_name="TCGA_114C_09-1664-01A-01_61-2094-01A-01_25-1312-01A-01_W_JHUZ_20130802_F1" raw_data_type="" raw_data="">
            */
            
            //analysis_summary/
            System.out.println(" Making analysis_summary node...");
            Element analysis_summary = doc.createElement("analysis_summary");
             analysis_summary.setAttribute("analysis", "Proteome Discoverer");
             analysis_summary.setAttribute("version", parametersMap.get("PDVersion"));// hard-coded but could be auto-retrieved from the SchemaInfo table [SchemaInfo.SoftwareVersion]
             analysis_summary.setAttribute("time", time);
            rootElement.appendChild(analysis_summary);
            
            
            //msms_run_summary
            System.out.println(" Making msms_run_summary node...");
            Element msms_run_summary = doc.createElement("msms_run_summary");
            // *******    get spectrum file properties *********
            String spectrumFile = parametersMap.get("SpectrumFileNames");
            String base_name = "";
            String raw_data = "";
            if(spectrumFile.endsWith("raw")){
                raw_data = ".raw";
                base_name = new File(spectrumFile).getName().replace(".raw", "");
            }else if(spectrumFile.endsWith("mzXML")){
                raw_data = ".mzXML";
                base_name = new File(spectrumFile).getName().replace(".mzXML", "");
            }else if(spectrumFile.endsWith("mzML")){
                raw_data = ".mzML";
                base_name = new File(spectrumFile).getName().replace(".mzML", "");
            }
            //**********************************************
            msms_run_summary.setAttribute("base_name", base_name);
            msms_run_summary.setAttribute("raw_data_type", "raw");
            msms_run_summary.setAttribute("raw_data", raw_data);
            rootElement.appendChild(msms_run_summary);
                //msms_run_summary/sample_enzyme
                Element sample_enzyme = doc.createElement("sample_enzyme");
                sample_enzyme.setAttribute("name", getEnzyme(parametersMap));
                sample_enzyme.setAttribute("independent", "false");
                sample_enzyme.setAttribute("fidelity", "specific");
            msms_run_summary.appendChild(sample_enzyme);
                    //msms_run_summary/sample_enzyme/specificity
                    Element specificity = doc.createElement("specificity");
                    specificity.setAttribute("sense", "C");
                    specificity.setAttribute("cut", "KR");
                    specificity.setAttribute("no_cut", "");
                    specificity.setAttribute("min_spacing", getMissedCleavages(parametersMap));//.get("MaxMissedCleavages"));
                sample_enzyme.appendChild(specificity);
                //msms_run_summary/search_summary
                Element search_summary = doc.createElement("search_summary");
                search_summary.setAttribute("base_name", pepXMLOutFile.replace(".pepXmL","").replace(".pep.xml",""));
                search_summary.setAttribute("search_engine", getSearchEngine(parametersMap).replace(" HT",""));
                search_summary.setAttribute("search_engine_version", parametersMap.get("PDVersion"));
                search_summary.setAttribute("precursor_mass_type", "monoisotopic");
                search_summary.setAttribute("fragment_mass_type", "monoisotopic");
                if(pepXMLOutFile.endsWith("pepXML"))
                    search_summary.setAttribute("out_data_type", "pepXML"); //nil
                else 
                    search_summary.setAttribute("out_data_type", "pep.xml"); //nil
                search_summary.setAttribute("out_data",""); //nil
           msms_run_summary.appendChild(search_summary);
                    //msms_run_summary/search_summary/search_database/
                    Element search_database = doc.createElement("search_database");
                    //search_database.setAttribute("local_path", getSearchedDB(searchDBPath,parametersMap)); 
                    //                                                          //retrieved from config file because MSF file specified 
                    //                                                          // database path is the indexed database... 
                    search_database.setAttribute("local_path", getSearchedDB(searchDBPath,parametersMap));
                    //search_database.setAttribute("database_name", parametersMap.get("DatabaseName"));
                    search_database.setAttribute("type", "AA");
                search_summary.appendChild(search_database);
                    //msms_run_summary/search_summary/enzymatic_search_constraint/
                    Element enzymatic_search_constraint = doc.createElement("enzymatic_search_constraint");
                    enzymatic_search_constraint.setAttribute("enzyme", getEnzyme(parametersMap)); //replace with auto generated value
                    enzymatic_search_constraint.setAttribute("max_num_internal_cleavages", getMissedCleavages(parametersMap));
                    enzymatic_search_constraint.setAttribute("min_number_termini", "");
                search_summary.appendChild(enzymatic_search_constraint);
                    //msms_run_summary/search_summary/aminoacid_modification(s)/
                    // Dynamic modifications
                    //ArrayList<MSFSearchModificationParam> dyn_modfxns = getSearchModificationParams(parametersMap,"DynMod"); // dynamic modifications
                    ArrayList<MSFSearchModificationParam> dyn_modfxns = getSearchModificationParams(parametersMap,"Dyn"); 
                       // due to version incompatibility, this was refactored to use the 'Dyn' string to select 
                       // possible parameter keys that represent Dynamic modifications in the MSF file...
                       // the 'Dyn' was specified because, from all examined MSF files, the Dyn string seems to 
                       // across all for soecifying dynamic modification.. The different keys observed include: DynamicModification1,
                       // DynModification_1, and DynMod_1 
                    Iterator<MSFSearchModificationParam> itr = null;
                    itr = dyn_modfxns.iterator();
                    while(itr.hasNext()){
                        MSFSearchModificationParam modfxn = itr.next();
                        String aminoacid = modfxn.getResidue();
                        double massdiff = modfxn.getDeltaMass();
                        double mass = calcModifiedResidueMass(massdiff, aminoacid);
                        String variable = "Y";
                        String description = modfxn.getName();
                        
                        if(aminoacid.equalsIgnoreCase("AnyN-Terminus")){
                            Element aminoacid_modification = doc.createElement("terminal_modification");
                            //aminoacid_modification.setAttribute("terminus", "n");
                            if(tppCompatibleOutput)
                               aminoacid_modification.setAttribute("terminus", "N"); 
                            else
                               aminoacid_modification.setAttribute("terminus", "n"); aminoacid_modification.setAttribute("massdiff", String.valueOf(massdiff));
                            aminoacid_modification.setAttribute("mass", String.valueOf(mass));
                            aminoacid_modification.setAttribute("variable", variable);
                            aminoacid_modification.setAttribute("description", description);
                            search_summary.appendChild(aminoacid_modification); 
                        } else { 

                            Element aminoacid_modification = doc.createElement("aminoacid_modification");
                            aminoacid_modification.setAttribute("aminoacid", aminoacid);
                            aminoacid_modification.setAttribute("massdiff", String.valueOf(massdiff));
                            aminoacid_modification.setAttribute("mass", String.valueOf(mass));
                            aminoacid_modification.setAttribute("variable", variable);
                            aminoacid_modification.setAttribute("description", description);
                            search_summary.appendChild(aminoacid_modification); 
                        }
                    }
                    // Static modifications
                    //ArrayList<MSFSearchModificationParam> st_modfxns = getSearchModificationParams(parametersMap,"StatMod"); // dynamic modifications
                    ArrayList<MSFSearchModificationParam> st_modfxns = getSearchModificationParams(parametersMap,"Stat"); // dynamic modifications
                       // Similar to Dynamic modifiations revision, due to version incompatibility, 
                       // this was refactored to use the 'Stat' string to select 
                       // possible parameter keys that represent Static modifications in the MSF file...
                       // the 'Stat' was specified because, from all examined MSF files, the Stat string seems to be common
                       // across all for soecifying Static modification.. The different keys observed include: StaticModification1,
                       // StaticMod_1, and StatMod_1 
                    itr = st_modfxns.iterator();
                    while(itr.hasNext()){
                        MSFSearchModificationParam modfxn = itr.next();
                        String aminoacid = modfxn.getResidue();
                        double massdiff = modfxn.getDeltaMass();
                        double mass = calcModifiedResidueMass(massdiff, aminoacid);
                        String variable = "N";
                        String description = modfxn.getName();
                        
                        if(aminoacid.equalsIgnoreCase("AnyN-Terminus")){
                            Element aminoacid_modification = doc.createElement("terminal_modification");
                            //aminoacid_modification.setAttribute("terminus", "n");
                            if(tppCompatibleOutput)
                               aminoacid_modification.setAttribute("terminus", "N"); 
                            else
                               aminoacid_modification.setAttribute("terminus", "n"); 
                            aminoacid_modification.setAttribute("massdiff", String.valueOf(massdiff));
                            aminoacid_modification.setAttribute("mass", String.valueOf(mass));
                            aminoacid_modification.setAttribute("variable", variable);
                            aminoacid_modification.setAttribute("description", description);
                            search_summary.appendChild(aminoacid_modification); 
                        } else { 
                        
                            Element aminoacid_modification = doc.createElement("aminoacid_modification");
                            aminoacid_modification.setAttribute("aminoacid", aminoacid);
                            aminoacid_modification.setAttribute("massdiff", String.valueOf(massdiff));
                            aminoacid_modification.setAttribute("mass", String.valueOf(mass));
                            aminoacid_modification.setAttribute("variable", variable);
                            aminoacid_modification.setAttribute("description", description);
                            search_summary.appendChild(aminoacid_modification);  
                        }
                    }
                    
                    //output Proteome Discoverers .MSF parameters matched against Myrimatch's
                    //msms_run_summary/search_summary/parameter(s)/
                    Element parameters1 = doc.createElement("parameter");
                    parameters1.setAttribute("name", "Config: CleavageRules");
                    parameters1.setAttribute("value", getEnzyme(parametersMap));
                search_summary.appendChild(parameters1);
                    //msms_run_summary/search_summary/parameter(s)/
                    Element parameters2 = doc.createElement("parameter");
                    parameters2.setAttribute("name", "Config: MaxMissedCleavages");
                    parameters2.setAttribute("value", getMissedCleavages(parametersMap));
                search_summary.appendChild(parameters2);
                    //msms_run_summary/search_summary/parameter(s)/
                    Element parameters3 = doc.createElement("parameter");
                    parameters3.setAttribute("name", "Config: ComputeXCorr");
                    parameters3.setAttribute("value", "");
                search_summary.appendChild(parameters3);
                //msms_run_summary/search_summary/parameter(s)/
                    Element parameters8 = doc.createElement("parameter");
                    parameters8.setAttribute("name", "Config: DecoyPrefix");
                    parameters8.setAttribute("value", "rev_");
                search_summary.appendChild(parameters8);
                    //msms_run_summary/search_summary/parameter(s)/
                    Element parameters4 = doc.createElement("parameter");
                    parameters4.setAttribute("name", "Config: OutputFormat");
                    parameters4.setAttribute("value", "pepXML");
                search_summary.appendChild(parameters4);
                    //msms_run_summary/search_summary/parameter(s)/
                    Element parameters5 = doc.createElement("parameter");
                    parameters5.setAttribute("name", "Config: ProteinDatabase");
                    //parameters5.setAttribute("value", getSearchedDB(searchDBPath,parametersMap));
                    parameters5.setAttribute("value", getSearchedDB(searchDBPath, parametersMap));//.get("DatabaseName"));
                search_summary.appendChild(parameters5);
                    //msms_run_summary/search_summary/parameter(s)/
                    Element parameters6 = doc.createElement("parameter");
                    parameters6.setAttribute("name", "SearchEngine: Name");
                    parameters6.setAttribute("value", "Proteome Discoverer: " + getSearchEngine(parametersMap));
                search_summary.appendChild(parameters6);
                    //msms_run_summary/search_summary/parameter(s)/
                    Element parameters7 = doc.createElement("parameter");
                    parameters7.setAttribute("name", "SearchEngine: Version");
                    parameters7.setAttribute("value", parametersMap.get("PDVersion"));
                search_summary.appendChild(parameters7);
                
         
                
                
                //msms_run_summary/spectrum_query
                Set<Integer> spectraIDs = spectID2PepIDsMap.keySet();
                ArrayList<Integer> spectraIDsArrList = new ArrayList(spectraIDs); //making arrayList for sorting
                Collections.sort(spectraIDsArrList); // sorts to spectraID in ascending order
                Iterator<Integer> spectItr = spectraIDsArrList.iterator();
                System.out.println(" Making spectrum_query objects...");
                System.out.println(" " + spectraIDs.size() + " spectra found...");
                int spectrumIndex = 0;
                while(spectItr.hasNext()){
                    spectrumIndex++;
                    System.out.println("  Making spectrum " + spectrumIndex + " objects...");
                    int spectraID = spectItr.next();
                    
                    // a representative peptideID
                    // a representative peptide to extract spectrum associated values
                            //int pepID = spectID2PepIDsMap.get(spectraID).get(0); 
                            //MSFPSMPeptide pep = pepID2MSFPeptideMap.get(pepID); 
                    // for a representative peptide, this implementation uses 
                    // the top-ranked peptide hit's spectrum-associated values
                    LinkedList<Integer> mappedPepIDs = spectID2PepIDsMap.get(spectraID);
                    LinkedList<MSFPSMPeptide> mappedPeps = extractMappedPeptides(mappedPepIDs, pepID2MSFPeptideMap);
                    
                    if(tppCompatibleOutput){
                        // rank Peptides by xCorr values
                        MSFPSMPeptideXCorrRankedQueue mappedPepsQueue = new MSFPSMPeptideXCorrRankedQueue(mappedPeps);
                        
                        /*
                         <spectrum_query spectrum="18187_REP2_4pmol_UPS2_IDA_2.comet.06004.06004.1" 
                         *             start_scan="6004" 
                         *             end_scan="6004" 
                         *             precursor_neutral_mass="599.328190" 
                         *             assumed_charge="1" 
                         *             index="1" 
                         *             retention_time_sec="452.5">
                         * 
                         * 
                         */
                        // the representative peptide should be at the front of the queue
                        MSFPSMPeptide pep = mappedPepsQueue.peek();

                        //spectrum == FileName.FirstScan.EndScan.Charge
                        String start_scan = String.valueOf(pep.getFirstScan());
                        String end_scan = String.valueOf(pep.getLastScan()) ;
                        String assumed_charge = String.valueOf(pep.getCharge());
                        String fileName = new File(pepXMLOutFile).getName();
                        //String fileName = parametersMap.get().getName();
                        String spectrum = fileName.replace(".pepXML", "") + "." + start_scan + "." + end_scan + "." + assumed_charge;
                        //String spectrumNativeID = "controllerType=0 controllerNumber=1 scan=" + start_scan;
                        String precursor_neutral_mass = String.valueOf(pep.getMass());

                        //String index;
                        String retention_time_sec = String.valueOf(pep.getRetentionTime());

                        Element spectrum_query = doc.createElement("spectrum_query");
                        spectrum_query.setAttribute("spectrum", spectrum);
                        //spectrum_query.setAttribute("spectrumNativeID", spectrumNativeID);
                        spectrum_query.setAttribute("start_scan", start_scan);
                        spectrum_query.setAttribute("end_scan", end_scan);
                        spectrum_query.setAttribute("precursor_neutral_mass", precursor_neutral_mass);
                        spectrum_query.setAttribute("assumed_charge", assumed_charge);
                        //spectrum_query.setAttribute("index", String.valueOf(pep.getFirstScan() - 1)); // from pepXML reviews indeces are typically a value lower than scan's nativeID
                        spectrum_query.setAttribute("index", String.valueOf(spectrumIndex));
                        spectrum_query.setAttribute("retention_time_sec", retention_time_sec);

                        msms_run_summary.appendChild(spectrum_query);
                            //msms_run_summary/spectrum_query/search_result/
                            //get all result
                            //get actual mapped Peptides and count true vs decoy maps
                            int truePeps = 0;
                            int decoyPeps = 0;
                            //Iterator<Integer> mappedPepIDsItr = mappedPepIDs.iterator(); 
                            //LinkedList<MSFPSMPeptide> mappedPepList = new LinkedList<MSFPSMPeptide>();
                            //while(mappedPepIDsItr.hasNext()){
                            //    int mappedPepID = mappedPepIDsItr.next();
                            //    MSFPSMPeptide mappedPep = pepID2MSFPeptideMap.get(mappedPepID);
                            MSFPSMPeptide[] mappedPepsQueueMappedPepsArr = mappedPepsQueue.getPriorityQueue();
                            for(MSFPSMPeptide mappedPep: mappedPepsQueueMappedPepsArr){
                                if(mappedPep.isDecoy()){
                                    decoyPeps++;
                                } else {
                                    truePeps++;
                                }
                                //mappedPepList.add(mappedPep);
                            }
                            System.out.println("   " + mappedPepsQueueMappedPepsArr.length + 
                                                    " peptides mapped to spectra " + spectraID);
                            System.out.println("    " + truePeps + " true mappings and " + decoyPeps + " decoy mappings...");

                            Element search_result = doc.createElement("search_result");
                            search_result.setAttribute("num_target_comparisons", String.valueOf(truePeps));
                            search_result.setAttribute("num_decoy_comparisons", String.valueOf(decoyPeps));
                            spectrum_query.appendChild(search_result);        
                                //msms_run_summary/spectrum_query/search_result/search_hit(s)

                                int hitIndex = 0;
                                for(int i = 0; i < mappedPepsQueueMappedPepsArr.length; i++){
                                    MSFPSMPeptide peptide = mappedPepsQueueMappedPepsArr[i];
                                    LinkedList<Peptide> pepInstances = pepID2PeptideInstancesMap.get(peptide.getPeptideID());
                                    if(pepInstances != null){
                                        hitIndex++;  
                                        String pepSequence = peptide.getSequence();
                                        Element search_hit = doc.createElement("search_hit");
                                        System.out.println("   setting hit " + hitIndex + 
                                                " of spectrum " + spectraID + "'s properties: " + pepSequence);
                                        /*
                                        * search_hit attributes sample: 
                                        *      hit_rank = //required
                                        *      peptide = //required
                                        *      peptide_prev_aa =  
                                        *      peptide_next_aa =  
                                        *      protein= //"gi|4502027|ref|NP_000468.1|" 
                                        *      num_tot_proteins="1" 
                                        *      calc_neutral_pep_mass="1436.8118653345" 
                                        *      massdiff="0.00268779951" //Mass(precursor ion) - Mass(peptide)
                                        *      num_tol_term="2" 
                                        *      num_missed_cleavages="0" 
                                        *      num_matched_ions="13" 
                                        *      tot_num_ions="19"
                                        * 
                                        */
                                        //search_hit.setAttribute("hit_rank", String.valueOf(peptide.getSearchEngineRank()));
                                        search_hit.setAttribute("hit_rank", String.valueOf(hitIndex));
                                        search_hit.setAttribute("peptide", pepSequence);

                                        System.out.println("    getting dB protein/peptide instances of " + (peptide.isDecoy()==false) + " hit " + hitIndex);
                                        System.out.println("     " + pepInstances.size() + " dB protein/peptide instances found");
                                        // -- for troubleshooting print protein peptide instances...
                                        for(Peptide pepI : pepInstances){
                                            System.out.println("       protein: " + 
                                                                        pepI.getMSFProteinParent().getDescription() + 
                                                                            ", peptide: " + 
                                                                                pepI.getSeqWithNeighbors());
                                        }
                                        Peptide representativePep = pepInstances.get(0); // actual peptide in this case, and not MSFPSMPeptide
                                        String seqWithNeighbors = representativePep.getSeqWithNeighbors();
                                        String prev_aa = String.valueOf(seqWithNeighbors.charAt(0));
                                        String next_aa = String.valueOf(seqWithNeighbors.charAt(seqWithNeighbors.length()-1));
                                        search_hit.setAttribute("peptide_prev_aa", prev_aa);
                                        search_hit.setAttribute("peptide_next_aa", next_aa);
                                        
                                        
                                        search_hit.setAttribute("protein", representativePep.getMSFProteinParent().getTPPDescription());
                                        search_hit.setAttribute("num_tot_proteins", String.valueOf(pepInstances.size()));

                                        MSFPSMMasses pepMasses = new MSFPSMMasses(peptide);
                                        search_hit.setAttribute("calc_neutral_pep_mass", String.valueOf(pepMasses.getTheoreticalMass()));
                                        search_hit.setAttribute("massdiff", String.valueOf(pepMasses.getDeltaMassDa()));

                                        search_hit.setAttribute("num_tol_term", String.valueOf(getNumberTrypticTerminal(seqWithNeighbors)));
                                        search_hit.setAttribute("num_missed_cleavages", String.valueOf(peptide.getMissedCleavages()));
                                        search_hit.setAttribute("num_matched_ions", String.valueOf(peptide.getMatchedIonsCount()));
                                        search_hit.setAttribute("tot_num_ions", String.valueOf(peptide.getTotalIonsCount()));
                                        search_result.appendChild(search_hit);
                                        //...search_hit/alternative_protein
                                        for(int j = 1; j < pepInstances.size(); j++){
                                            Peptide instance = pepInstances.get(j);
                                            Element alternative_protein = doc.createElement("alternative_protein");
                                            alternative_protein.setAttribute("protein", instance.getMSFProteinParent().getTPPDescription());
                                            search_hit.appendChild(alternative_protein);
                                        }
                                        //...search_hit/modification_info
                                        Element modification_info = doc.createElement("modification_info");
                                        double ntermModMass = 0;
                                        LinkedList<MSFPSMModification> modfxns = peptide.getModifications();
                                        Iterator<MSFPSMModification> modfxnItr = modfxns.iterator();
                                        while(modfxnItr.hasNext()){
                                            MSFPSMModification modfxn = modfxnItr.next();
                                            if(modfxn.isTerminal()){ //at the moment only implies N-Terminal
                                                ntermModMass = modfxn.getDeltaMass() + 1.0078250321;//add the value of water
                                                modification_info.setAttribute("mod_nterm_mass", String.valueOf(ntermModMass));
                                            } else {
                                                try{
                                                    String residue = String.valueOf(pepSequence.charAt(modfxn.getPosition()));
                                                    Element mod_aminoacid_mass = doc.createElement("mod_aminoacid_mass");
                                                    mod_aminoacid_mass.setAttribute("position", String.valueOf(modfxn.getPosition()+1));
                                                    //mod_aminoacid_mass.setAttribute("mass", String.valueOf(getModfResidueMass(residue,modfxn)));
                                                    if(residue != null){
                                                        mod_aminoacid_mass.setAttribute("mass", String.valueOf(calcModifiedResidueMass(modfxn.getDeltaMass(), 
                                                                                                                                residue)));
                                                    } else {
                                                        mod_aminoacid_mass.setAttribute("mass", "0");
                                                    }
                                                    //calcModifiedResidueMass(double massdiff, String aminoacid)
                                                    modification_info.appendChild(mod_aminoacid_mass);
                                                }catch(java.lang.StringIndexOutOfBoundsException ex){
                                                    System.out.println("peptide: " + pepSequence + 
                                                                       "Modification: " + modfxn.getModificationName() + 
                                                                       "ModifxnPosition: " + modfxn.getDeltaMass());
                                                    ex.printStackTrace();
                                                    System.exit(1);
                                                }
                                            }
                                        }
                                        search_hit.appendChild(modification_info);
                                        //...search_hit/search_score/

                                        //Element search_score = doc.createElement("search_score");
                                        //search_score.setAttribute("name", "xcorr");
                                        //search_score.setAttribute("value", String.valueOf(peptide.getXCorr()));
                                        //search_hit.appendChild(search_score);

                                        /*
                                         HashMap<String, Double> scoreNameToValueMap = peptide.getScoreNameToValueMap();
                                        Set<String> scoreNames = scoreNameToValueMap.keySet();
                                        for(String scoreName : scoreNames){
                                            Element search_score = doc.createElement("search_score");
                                            search_score.setAttribute("name", scoreName);
                                            search_score.setAttribute("value", 
                                                            String.valueOf(scoreNameToValueMap.get(scoreName)));
                                            search_hit.appendChild(search_score);
                                        }
                                        * 
                                        */
                                        // append scores manually instead (to ensure needed scores by TPP for downstream analyses are included)
                                        Element xCorrScoreNode = doc.createElement("search_score");
                                        xCorrScoreNode.setAttribute("name", "xcorr");
                                        xCorrScoreNode.setAttribute("value", String.valueOf(peptide.getXCorr()));
                                        search_hit.appendChild(xCorrScoreNode);
                                        
                                        Element deltaCNScoreNode = doc.createElement("search_score");
                                        deltaCNScoreNode.setAttribute("name", "deltacn");
                                        deltaCNScoreNode.setAttribute("value", String.valueOf(peptide.getDeltaCn()));
                                        search_hit.appendChild(deltaCNScoreNode);
                                        
                                        Element deltaCNStarScoreNode = doc.createElement("search_score");
                                        deltaCNStarScoreNode.setAttribute("name", "deltacnstar");
                                        deltaCNStarScoreNode.setAttribute("value", String.valueOf(peptide.getDeltaCnStar()));
                                        search_hit.appendChild(deltaCNStarScoreNode);
                                        
                                        Element spScoreNode = doc.createElement("search_score");
                                        spScoreNode.setAttribute("name", "spscore");
                                        spScoreNode.setAttribute("value", String.valueOf(peptide.getSpScore()));
                                        search_hit.appendChild(spScoreNode);
                                        
                                        Element spRankNode = doc.createElement("search_score");
                                        spRankNode.setAttribute("name", "sprank");
                                        spRankNode.setAttribute("value", String.valueOf(peptide.getSpRank()));
                                        search_hit.appendChild(spRankNode);
                                        
                                        Element expectNode = doc.createElement("search_score");
                                        expectNode.setAttribute("name", "expect");
                                        expectNode.setAttribute("value", String.valueOf(peptide.getExpectValue()));
                                        search_hit.appendChild(expectNode);
                                        
                                    }
                                }
                                
                    }else{

                        MSFPSMPeptideRankQueue mappedPepsQueue = new MSFPSMPeptideRankQueue(mappedPeps);
                        // the representative peptide should be at the front of the queue
                        MSFPSMPeptide pep = mappedPepsQueue.peek();

                        //spectrum == FileName.FirstScan.EndScan.Charge
                        String start_scan = String.valueOf(pep.getFirstScan());
                        String end_scan = String.valueOf(pep.getLastScan()) ;
                        String assumed_charge = String.valueOf(pep.getCharge());
                        String fileName = new File(pepXMLOutFile).getName();
                        String spectrum = fileName.replace(".pepXML", "") + "." + start_scan + "." + end_scan + "." + assumed_charge;
                        String spectrumNativeID = "controllerType=0 controllerNumber=1 scan=" + start_scan;
                        String precursor_neutral_mass = String.valueOf(pep.getMass());

                        //String index;
                        String retention_time_sec = String.valueOf(pep.getRetentionTime());

                        Element spectrum_query = doc.createElement("spectrum_query");
                        spectrum_query.setAttribute("spectrum", spectrum);
                        spectrum_query.setAttribute("spectrumNativeID", spectrumNativeID);
                        spectrum_query.setAttribute("start_scan", start_scan);
                        spectrum_query.setAttribute("end_scan", end_scan);
                        spectrum_query.setAttribute("precursor_neutral_mass", precursor_neutral_mass);
                        spectrum_query.setAttribute("assumed_charge", assumed_charge);
                        spectrum_query.setAttribute("index", String.valueOf(pep.getFirstScan() - 1)); // from pepXML reviews indeces are typically a value lower than scan's nativeID
                        spectrum_query.setAttribute("retention_time_sec", retention_time_sec);

                        msms_run_summary.appendChild(spectrum_query);
                            //msms_run_summary/spectrum_query/search_result/
                            //get all result
                            //get actual mapped Peptides and count true vs decoy maps
                            int truePeps = 0;
                            int decoyPeps = 0;
                            //Iterator<Integer> mappedPepIDsItr = mappedPepIDs.iterator(); 
                            //LinkedList<MSFPSMPeptide> mappedPepList = new LinkedList<MSFPSMPeptide>();
                            //while(mappedPepIDsItr.hasNext()){
                            //    int mappedPepID = mappedPepIDsItr.next();
                            //    MSFPSMPeptide mappedPep = pepID2MSFPeptideMap.get(mappedPepID);
                            MSFPSMPeptide[] mappedPepsQueueMappedPepsArr = mappedPepsQueue.getPriorityQueue();
                            for(MSFPSMPeptide mappedPep: mappedPepsQueueMappedPepsArr){
                                if(mappedPep.isDecoy()){
                                    decoyPeps++;
                                } else {
                                    truePeps++;
                                }
                                //mappedPepList.add(mappedPep);
                            }
                            System.out.println("   " + mappedPepsQueueMappedPepsArr.length + 
                                                    " peptides mapped to spectra " + spectraID);
                            System.out.println("    " + truePeps + " true mappings and " + decoyPeps + " decoy mappings...");

                            Element search_result = doc.createElement("search_result");
                            search_result.setAttribute("num_target_comparisons", String.valueOf(truePeps));
                            search_result.setAttribute("num_decoy_comparisons", String.valueOf(decoyPeps));
                            spectrum_query.appendChild(search_result);        
                                //msms_run_summary/spectrum_query/search_result/search_hit(s)

                                int hitIndex = 0;
                                for(int i = 0; i < mappedPepsQueueMappedPepsArr.length; i++){
                                    hitIndex++;
                                    MSFPSMPeptide peptide = mappedPepsQueueMappedPepsArr[i];
                                    LinkedList<Peptide> pepInstances = pepID2PeptideInstancesMap.get(peptide.getPeptideID());
                                    if(pepInstances != null){
                                        String pepSequence = peptide.getSequence();
                                        Element search_hit = doc.createElement("search_hit");
                                        System.out.println("   setting hit " + hitIndex + 
                                                " of spectrum " + spectraID + "'s properties: " + pepSequence);
                                        /*
                                        * search_hit attributes sample: 
                                        *      hit_rank = //required
                                        *      peptide = //required
                                        *      peptide_prev_aa =  
                                        *      peptide_next_aa =  
                                        *      protein= //"gi|4502027|ref|NP_000468.1|" 
                                        *      num_tot_proteins="1" 
                                        *      calc_neutral_pep_mass="1436.8118653345" 
                                        *      massdiff="0.00268779951" //Mass(precursor ion) - Mass(peptide)
                                        *      num_tol_term="2" 
                                        *      num_missed_cleavages="0" 
                                        *      num_matched_ions="13" 
                                        *      tot_num_ions="19"
                                        * 
                                        */
                                        search_hit.setAttribute("hit_rank", String.valueOf(peptide.getSearchEngineRank()));
                                        search_hit.setAttribute("peptide", pepSequence);

                                        System.out.println("    getting dB protein/peptide instances of " + (peptide.isDecoy()==false) + " hit " + hitIndex);
                                        System.out.println("     " + pepInstances.size() + " dB protein/peptide instances found");
                                        // -- for troubleshooting print protein peptide instances...
                                        for(Peptide pepI : pepInstances){
                                            System.out.println("       protein: " + 
                                                                        pepI.getMSFProteinParent().getDescription() + 
                                                                            ", peptide: " + 
                                                                                pepI.getSeqWithNeighbors());
                                        }
                                        Peptide representativePep = pepInstances.get(0); // actual peptide in this case, and not MSFPSMPeptide
                                        String seqWithNeighbors = representativePep.getSeqWithNeighbors();
                                        String prev_aa = String.valueOf(seqWithNeighbors.charAt(0));
                                        String next_aa = String.valueOf(seqWithNeighbors.charAt(seqWithNeighbors.length()-1));
                                        search_hit.setAttribute("peptide_prev_aa", prev_aa);
                                        search_hit.setAttribute("peptide_next_aa", next_aa);
                                        search_hit.setAttribute("protein", representativePep.getMSFProteinParent().getMyriMatchDescription());
                                        search_hit.setAttribute("num_tot_proteins", String.valueOf(pepInstances.size()));

                                        MSFPSMMasses pepMasses = new MSFPSMMasses(peptide);
                                        search_hit.setAttribute("calc_neutral_pep_mass", String.valueOf(pepMasses.getTheoreticalMass()));
                                        search_hit.setAttribute("massdiff", String.valueOf(pepMasses.getDeltaMassDa()));

                                        search_hit.setAttribute("num_tol_term", String.valueOf(getNumberTrypticTerminal(seqWithNeighbors)));
                                        search_hit.setAttribute("num_missed_cleavages", String.valueOf(peptide.getMissedCleavages()));
                                        search_hit.setAttribute("num_matched_ions", String.valueOf(peptide.getMatchedIonsCount()));
                                        search_hit.setAttribute("tot_num_ions", String.valueOf(peptide.getTotalIonsCount()));
                                        search_result.appendChild(search_hit);
                                        //...search_hit/alternative_protein
                                        for(int j = 1; j < pepInstances.size(); j++){
                                            Peptide instance = pepInstances.get(j);
                                            Element alternative_protein = doc.createElement("alternative_protein");
                                            alternative_protein.setAttribute("protein", instance.getMSFProteinParent().getMyriMatchDescription());
                                            search_hit.appendChild(alternative_protein);
                                        }
                                        //...search_hit/modification_info
                                        Element modification_info = doc.createElement("modification_info");
                                        double ntermModMass = 0;
                                        LinkedList<MSFPSMModification> modfxns = peptide.getModifications();
                                        Iterator<MSFPSMModification> modfxnItr = modfxns.iterator();
                                        while(modfxnItr.hasNext()){
                                            MSFPSMModification modfxn = modfxnItr.next();
                                            if(modfxn.isTerminal()){ //at the moment only implies N-Terminal
                                                ntermModMass = modfxn.getDeltaMass() + 1.0078250321;//add the value of water
                                                modification_info.setAttribute("mod_nterm_mass", String.valueOf(ntermModMass));
                                            } else {
                                                String residue = String.valueOf(pepSequence.charAt(modfxn.getPosition()));
                                                Element mod_aminoacid_mass = doc.createElement("mod_aminoacid_mass");
                                                mod_aminoacid_mass.setAttribute("position", String.valueOf(modfxn.getPosition()+1));
                                                //mod_aminoacid_mass.setAttribute("mass", String.valueOf(getModfResidueMass(residue,modfxn)));
                                                if(residue != null){
                                                    mod_aminoacid_mass.setAttribute("mass", String.valueOf(calcModifiedResidueMass(modfxn.getDeltaMass(), 
                                                                                                                            residue)));
                                                } else {
                                                    mod_aminoacid_mass.setAttribute("mass", "0");
                                                }
                                                //calcModifiedResidueMass(double massdiff, String aminoacid)
                                                modification_info.appendChild(mod_aminoacid_mass);
                                            }
                                        }
                                        search_hit.appendChild(modification_info);
                                        //...search_hit/search_score/

                                        //Element search_score = doc.createElement("search_score");
                                        //search_score.setAttribute("name", "xcorr");
                                        //search_score.setAttribute("value", String.valueOf(peptide.getXCorr()));
                                        //search_hit.appendChild(search_score);

                                        HashMap<String, Double> scoreNameToValueMap = peptide.getScoreNameToValueMap();
                                        Set<String> scoreNames = scoreNameToValueMap.keySet();
                                        for(String scoreName : scoreNames){
                                            Element search_score = doc.createElement("search_score");
                                            search_score.setAttribute("name", scoreName);
                                            search_score.setAttribute("value", 
                                                            String.valueOf(scoreNameToValueMap.get(scoreName)));
                                            search_hit.appendChild(search_score);
                                        }
                                    }
                                }

                    }
                }
                
                
            // ********************************************************************* //
            // write content into xml [.mzid] file
            System.out.println("\tMaking 'pepXML output' file...");
            
            /*
             * to indent output
             * //(1)
                TransformerFactory tf = TransformerFactory.newInstance();
                tf.setAttribute("indent-number", new Integer(2));
                //(2)
                Transformer t = tf.newTransformer();
                t.setOutputProperty(OutputKeys.INDENT, "yes");
                //(3)
                t.transform(new DOMSource(doc),
                new StreamResult(new OutputStreamWriter(out, "utf-8"));
             * 
             */
            
            System.out.println("...transforming DOMObjects...");
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", new Integer(3));
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                
            DOMSource source = new DOMSource(doc);
            //StreamResult result = new StreamResult(new File(mzidMLOutFile));
            StreamResult result = new StreamResult(new BufferedWriter(new FileWriter(pepXMLOutFile)));
            transformer.transform(source, result);
            System.out.println("...pepXML file saved!");
                  
                    
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(MSF2PepXMLPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(MSF2PepXMLPrinter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MSF2PepXMLPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } 
    }
    
    private ArrayList<MSFSearchModificationParam> getSearchModificationParams(HashMap<String,String> parametersMap,
                                                                            String pattern) {
        //throw new UnsupportedOperationException("Not yet implemented");
        ArrayList<MSFSearchModificationParam> mods = new ArrayList<MSFSearchModificationParam>();
        Set<String> keys = parametersMap.keySet();
        //Iterate through parameterMap keys, if a keymatches DynMod, get its value from the parameters map
        Iterator<String> itr = keys.iterator();
        while(itr.hasNext()){
            String key = itr.next();
            if(key.startsWith(pattern)){
                String mappedValue = parametersMap.get(key); // true modification value is represented by 
                            // modificationName/massDifference Da (modificationAminoAcid(s) separated by comma)    
                // check if the returned value contains '/' and the 'Da' character strings
                if(mappedValue.contains("/") && mappedValue.contains("Da")){
                    String[] valueArr = mappedValue.split("/"); //splits string to moficationName and massDiff+Residue(s)
                    String modName = valueArr[0].replaceAll("\\s+", ""); //removes all white spaces, get the modification name
                    String[] dMassNresidues = valueArr[1].split("Da");
                    double dMass = Double.parseDouble(dMassNresidues[0].replaceAll("\\s+",""));
                    String residues = dMassNresidues[1].replace("(","").replace(")","").replaceAll("\\s+","");
                    //in situations where more than one residues are associated with a modification,
                    String[] residueArr = residues.split(",");
                    for(String residue : residueArr){
                        mods.add(new MSFSearchModificationParam(modName,dMass,residue));
                    }
                }
            }
        }
        return mods;        
    }

    private double calcModifiedResidueMass(double massdiff, String aminoacid) {
        
        double modifiedMass = 0;
        //HashMap<String,Double> massTable = aAcidMassTable();
        double mass = 0;
        if(aminoacid.equalsIgnoreCase("AnyN-Terminus")){
            mass = 1.0078250321;
        } else {
            if(aAcidMassTable.get(aminoacid)!= null){
                mass = aAcidMassTable.get(aminoacid);
            }
        }
        double modifiedDoubleMass = mass + massdiff;
        modifiedMass = (modifiedDoubleMass);
        
        return modifiedMass;
    }
    
   private HashMap<String, Double> getAminoAcidMassTable() {
        //throw new UnsupportedOperationException("Not yet implemented");
        HashMap<String,Double> massTable = new HashMap<String,Double>();
        for(int i = 0; i < aASymbols.length; i++){
            massTable.put(aASymbols[i],aAMasses[i]);
        }       
        return massTable;
    }
    
    private static String[] aASymbols= { "A", "C", "D", "E", "F", "G", "H", "I", "K", "L", 
                                         "M", "N", "P", "Q", "R", "S", "T", "V", "W", "Y"};
    private static double[] aAMasses = { 71.037114,	// A;
                                            103.00919,	// C;
                                            115.02694,	// D;
                                            129.04259,	// E;
                                            147.06841,	// F;
                                            57.021464,	// G;
                                            137.05891,	// H;
                                            113.08406,	// I;
                                            128.09496,	// K;
                                            113.08406,	// L;
                                            131.04048,	// M;
                                            114.04293,	// N;
                                            97.052764,	// P;
                                            128.05858,	// Q;
                                            156.10111,	// R;
                                            87.032029,	// S;
                                            101.04768,	// T;
                                            99.068414,	// V;
                                            186.07931,	// W;
                                            163.06333,	// Y;
                                           };

    private String getEnzyme(HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String enz = parametersMap.get("Enzyme");
        enz = enz.split(" ")[0];
        return enz;
    }
    
    private int getNumberTrypticTerminal(String seqWithNeighbors) {
        int tTerminals = 0;
        if(seqWithNeighbors.startsWith("K")||seqWithNeighbors.startsWith("R")){
            tTerminals++;
        }
        if(seqWithNeighbors.charAt(seqWithNeighbors.length()-3)=='K' ||
                seqWithNeighbors.charAt(seqWithNeighbors.length()-3)=='R'){
            tTerminals++;
        }
        return tTerminals;
    }

    private LinkedList<MSFPSMPeptide> extractMappedPeptides(LinkedList<Integer> mappedPepIDs, 
                                                    HashMap<Integer, MSFPSMPeptide> pepID2MSFPeptideMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        LinkedList<MSFPSMPeptide> mappedPeps = new LinkedList<MSFPSMPeptide>();
        for(int mappedPepID: mappedPepIDs){
            MSFPSMPeptide mappedPep = pepID2MSFPeptideMap.get(mappedPepID);
            mappedPeps.add(mappedPep);
        }
        return mappedPeps;
    }

    private String getMissedCleavages(HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String mC = "";
        if(parametersMap.containsKey("MissedCleavages")){
            mC = parametersMap.get("MissedCleavages");           
        }else if(parametersMap.containsKey("MaxMissedCleavages")){
            mC = parametersMap.get("MaxMissedCleavages");  
        }       
        return mC;
    }

    private String getSearchEngine(HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String sE = "";
        if(parametersMap.containsKey("Mascot")){
            sE = parametersMap.get("Mascot");           
        }else if(parametersMap.containsKey("SequestNode")){
            sE = parametersMap.get("SequestNode");  
        }else if(parametersMap.containsKey("IseNode")){
            sE = parametersMap.get("IseNode");
        }            
        return sE;
    }

    private String getSearchedDB(String searchDBPath, HashMap<String, String> parametersMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String sDb = "";
        String sDbName = new File(searchDBPath).getName();
        String msfSpecifiedDBName = parametersMap.get("DatabaseName");
        if(sDbName.equalsIgnoreCase(msfSpecifiedDBName)){
            sDb = searchDBPath; // cross-checks to ascertain the database specified in config file....
        } else {
            System.out.println("      WARNING: Database specified in 'M2Lite.config' file\n" +
                               "         doesn't match that found in MSF file...");
            // clean-up possible PD configuration of database file name...
            msfSpecifiedDBName = msfSpecifiedDBName.replaceAll("\\(.*\\)", "");
            // test if modified db path is accessible...
            if(new File(msfSpecifiedDBName).exists())
                sDb = msfSpecifiedDBName;
            else 
                sDb = searchDBPath;
        }        
        return sDb;
    }
    
}
