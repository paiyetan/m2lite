/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package ios.printers;

import database.Database;
import database.Peptide;
import enumtypes.SearchDatabaseType;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import msfparser.MSFSearchModificationParam;
import msfproteins.MSFPSMProtein;
import msfpsms.MSFPSMMasses;
import msfpsms.MSFPSMModification;
import msfpsms.MSFPSMPeptide;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author paiyeta1
 */
public class MSF2AllMZIdentMLPrinter {
    
    public void write(HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap,
                      HashMap<String,String> parametersMap, 
                      HashMap<String,String> modTermMap, 
                      HashMap<Integer,LinkedList<Integer>> spectID2PepIDsMap,
                      String mzidMLOutFile, 
                      Database db,String searchDBPath, 
                      SearchDatabaseType dbtype, 
                      boolean includeProteinsInferred,
                      HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap) {
        try {
            //throw new UnsupportedOperationException("Not yet implemented");
            //create header and root, and level 1 elements
            System.out.println("\tMaking .mzid file...");
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("MzIdentML");
            /*
             *  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xsi:schemaLocation="http://psidev.info/psi/pi/mzIdentML/1.1 ../../../schema/mzIdentML1.1.0.xsd"
                xmlns="http://psidev.info/psi/pi/mzIdentML/1.1" id="12345" version="1.1.0"
                creationDate
             */
            //create root element's attributes and values
            Attr id = doc.createAttribute("id");
            Attr version = doc.createAttribute("version");
            Attr xmlns = doc.createAttribute("xmlns");
            Attr xmlUri = doc.createAttribute("xmlns:xsi");
            Attr schemaLocation = doc.createAttribute("xsi:schemaLocation");
            Attr creationDate = doc.createAttribute("creationDate");
            //setvalues
            /*
             * id, MPC_MzIdML
             * version, 1.1.0
             * xmnlns, http://psidev.info/psi/pi/mzIdentML/1.1
             * xmlns:xsi, http://www.w3.org/2001/XMLSchema-instance
             * xsi:schemaLocation, http://psidev.info/psi/pi/mzIdentML/1.1 http://www.psidev.info/files/mzIdentML1.1.0.xsd
             * creationDate: 2013-10-09T23:14:49
             * 
             */
            //http://www.psidev.info/sites/default/files/mzIdentML1.1.0.xsd
            id.setValue(String.valueOf(new Date().getTime()));
            version.setValue("1.1.0");
            xmlns.setValue("http://psidev.info/psi/pi/mzIdentML/1.1");
            xmlUri.setValue("http://www.w3.org/2001/XMLSchema-instance");
            schemaLocation.setValue("http://psidev.info/psi/pi/mzIdentML/1.1 http://www.psidev.info/sites/default/files/mzIdentML1.1.0.xsd");
            //DateFormat df = DateFormat.getDateInstance(DateFormat.LONG, Locale.US);
            creationDate.setValue(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(new Date())); 
            
            rootElement.setAttributeNode(id);
            rootElement.setAttributeNode(version);
            rootElement.setAttributeNode(creationDate);
            rootElement.setAttributeNode(schemaLocation);
            rootElement.setAttributeNode(xmlns);
            rootElement.setAttributeNode(xmlUri);
            
            
            doc.appendChild(rootElement);
           // Comment comment = doc.createComment("Auto-Generated by \'M2Lite\' on " + new Date().toString());
           // Element element = doc.getDocumentElement();
	   // element.getParentNode().insertBefore(comment, element); 
            
            
            /* level One elements
                cvList, 
                AnalysisSoftwareList, 
                +/- AnalysisSampleCollection
                Provider, 
                AuditCollection, 
                SequenceCollection,
                AnalysisCollection
                AnalysisProtocolCollection
                DataCollection
            * 
            */
            
            Element cvList = doc.createElement("cvList");
            Element AnalysisSoftwareList = doc.createElement("AnalysisSoftwareList");
            Element Provider = doc.createElement("Provider");
            Element AuditCollection = doc.createElement("AuditCollection");
            Element SequenceCollection = doc.createElement("SequenceCollection");
            Element AnalysisCollection = doc.createElement("AnalysisCollection");
            Element AnalysisProtocolCollection = doc.createElement("AnalysisProtocolCollection");
            Element DataCollection = doc.createElement("DataCollection");
           
            // ******************************************* //
            // cvList 
            // Used controlled vocabularies - these three usually are the only ones required thus can be hard-coded
            // ******************************************* //    
            //Create cvList elements
            System.out.println("\tMaking 'cvList' elements...");
            cvList.setAttribute("xmlns", "http://psidev.info/psi/pi/mzIdentML/1.1");
            
            Element cv1 = doc.createElement("cv");
            Element cv2 = doc.createElement("cv");
            Element cv3 = doc.createElement("cv");
            //Element cv4 = doc.createElement("cv");
            
            Attr cv1_id = doc.createAttribute("id");
            Attr cv1_uri = doc.createAttribute("uri");
            Attr cv1_version = doc.createAttribute("version");
            Attr cv1_fullName = doc.createAttribute("fullName");
            cv1_id.setValue("PSI-MS");
            cv1_uri.setValue("http://psidev.cvs.sourceforge.net/viewvc/psidev/psi/psi-ms/mzML/controlledVocabulary/psi-ms.obo");
            cv1_version.setValue("3.54.0");
            cv1_fullName.setValue("Proteomics Standards Initiative Mass Spectrometry Ontology");
            cv1.setAttributeNode(cv1_id);
            cv1.setAttributeNode(cv1_fullName);
            cv1.setAttributeNode(cv1_version);
            cv1.setAttributeNode(cv1_uri);
            
            Attr cv2_id = doc.createAttribute("id");
            Attr cv2_uri = doc.createAttribute("uri");
            Attr cv2_version = doc.createAttribute("version");
            Attr cv2_fullName = doc.createAttribute("fullName");
            cv2_id.setValue("UNIMOD");
            cv2_uri.setValue("http://www.unimod.org/obo/unimod.obo");
            cv2_version.setValue("1.2");
            cv2_fullName.setValue("UNIMOD");
            cv2.setAttributeNode(cv2_id);
            cv2.setAttributeNode(cv2_fullName);
            cv2.setAttributeNode(cv2_version);
            cv2.setAttributeNode(cv2_uri);
            
            Attr cv3_id = doc.createAttribute("id");
            Attr cv3_fullName = doc.createAttribute("fullName");
            Attr cv3_version = doc.createAttribute("version");
            Attr cv3_uri = doc.createAttribute("uri");
            cv3_id.setValue("UO");
            cv3_uri.setValue("http://obo.cvs.sourceforge.net/obo/obo/ontology/phenotype/unit.obo");
            cv3_version.setValue("1.2");
            cv3_fullName.setValue("UNIT-ONTOLOGY");
            cv3.setAttributeNode(cv3_id);
            cv3.setAttributeNode(cv3_fullName);
            cv3.setAttributeNode(cv3_version);
            cv3.setAttributeNode(cv3_uri);
            
            //apppend child nodes to cvList
            cvList.appendChild(cv1);
            cvList.appendChild(cv2);
            cvList.appendChild(cv3);
            
            //append cvList to mzIdentML
            rootElement.appendChild(cvList);
            
            // ******************************************* //
            // AnalysisSoftwareList
            // Software(s), sourced from PSI-MS CV 
            // ******************************************* //
            System.out.println("\tMaking 'AnalysisSoftwareList' elements...");
            // AnalysisSoftwareList/AnalysisSoftware
            Element AnalysisSoftware = doc.createElement("AnalysisSoftware");
            AnalysisSoftware.setAttribute("id","PDv1.3");
            AnalysisSoftware.setAttribute("name","Proteome Discoverer");
            AnalysisSoftware.setAttribute("version","1.3.0.339"); // hard-coded but could be auto-retrieved from the SchemaInfo table [SchemaInfo.SoftwareVersion]
                // AnalysisSoftwareList/AnalysisSoftware/ContactRole
                Element sofwareContactRole = doc.createElement("ContactRole");
                sofwareContactRole.setAttribute("contact_ref", "PD_OWNER");
                    // AnalysisSoftwareList/AnalysisSoftware/ContactRole/Role/
                    Element sofwareContactRoleRole = doc.createElement("Role");
                        // AnalysisSoftwareList/ContactRole/Role/cvParam
                        Element sofwareContactRoleRoleCVParam = doc.createElement("cvParam");
                        sofwareContactRoleRoleCVParam.setAttribute("cvRef", "PSI-MS");
                        sofwareContactRoleRoleCVParam.setAttribute("accession", "MS:1001267");
                        sofwareContactRoleRoleCVParam.setAttribute("name", "software vendor");
                    sofwareContactRoleRole.appendChild(sofwareContactRoleRoleCVParam);
                sofwareContactRole.appendChild(sofwareContactRoleRole);    
            AnalysisSoftware.appendChild(sofwareContactRole);
                // AnalysisSoftwareList/AnalysisSoftware/SoftwareName
                Element SoftwareName = doc.createElement("SoftwareName");
                    // AnalysisSoftwareList/AnalysisSoftware/SoftwareName/cvParam
                    Element softwareNameCVParam = doc.createElement("cvParam");
                    softwareNameCVParam.setAttribute("cvRef","PSI-MS");
                    softwareNameCVParam.setAttribute("accession","MS:1000650");
                    softwareNameCVParam.setAttribute("name","Proteome Discoverer");
                SoftwareName.appendChild(softwareNameCVParam);  
            AnalysisSoftware.appendChild(SoftwareName);
                
                        
            AnalysisSoftwareList.appendChild(AnalysisSoftware);
            rootElement.appendChild(AnalysisSoftwareList);
                      
            // ******************************************* //
            // Provider - hard-coded; may be retrieved through some interface
            // Provider of the document, can be hard-coded, with a reference to a Person in "AuditCollection"
            // ******************************************* //
            System.out.println("\tMaking 'Provider' document elements...");           
            Provider.setAttribute("xmlns", "http://psidev.info/psi/pi/mzIdentML/1.1");
            Provider.setAttribute("id", "Provider_1");
            
                // Provider/ContactRole/
                Element ContactRole = doc.createElement("ContactRole");
                ContactRole.setAttribute("contact_ref","DOC_OWNER");
                    // Provider/ContactRole/Role 
                    Element Role = doc.createElement("Role");
                        // Provider/ContactRole/Role /cvparam
                        Element roleCVParam = doc.createElement("cvParam");
                        roleCVParam.setAttribute("cvRef", "PSI-MS");
                        roleCVParam.setAttribute("accession", "MS:1001271");
                        roleCVParam.setAttribute("name", "researcher");
                    Role.appendChild(roleCVParam);
                ContactRole.appendChild(Role);
            Provider.appendChild(ContactRole);
            rootElement.appendChild(Provider);           
            
            /* ******************************************* //
              AuditCollection
              Minimally insert contact details in here for the Provider of 
              the document if known, otherwise provide dummy values
            // ******************************************* //
            */
            AuditCollection.setAttribute("xmlns", "http://psidev.info/psi/pi/mzIdentML/1.1");
            System.out.println("\tMaking 'AuditCollection' document elements...");
                //AuditCollection/Person
                Element Person = doc.createElement("Person");
                Person.setAttribute("firstName", "FNAME"); //dummy value may be sourced from input parameters
                Person.setAttribute("lastName", "LNAME");
                Person.setAttribute("id", "DOC_OWNER"); //referenced from ContactRole
                
                /*
                 * 
                    //AuditCollection/Person/Affiliation/
                    Element Affiliation = doc.createElement("Affiliation");
                    Affiliation.setAttribute("organization_ref","ORG_DOC_OWNER");
                Person.appendChild(Affiliation);
                * 
                */
                    //AuditCollection/Person/cvParam1/
                    Element cvParam1 = doc.createElement("cvParam");
                    cvParam1.setAttribute("cvRef", "PSI-MS");
                    cvParam1.setAttribute("accession", "MS:1000587");
                    cvParam1.setAttribute("name", "contact address");
                    cvParam1.setAttribute("value", "123 ABC Street, XY 78910");//dummay values
                Person.appendChild(cvParam1);
                    //AuditCollection/Person/cvParam2/
                    Element cvParam2 = doc.createElement("cvParam");
                    cvParam2.setAttribute("cvRef", "PSI-MS");
                    cvParam2.setAttribute("accession", "MS:1000588");
                    cvParam2.setAttribute("name", "contact URL");
                    cvParam2.setAttribute("value", "www.123mzid.com"); //dummy values
                Person.appendChild(cvParam2);
                    //AuditCollection/Person/cvParam3/
                    Element cvParam3 = doc.createElement("cvParam");
                    cvParam3.setAttribute("cvRef", "PSI-MS");
                    cvParam3.setAttribute("accession", "MS:1000589");
                    cvParam3.setAttribute("name", "contact email");
                    cvParam3.setAttribute("value", "mzid@123mzid.com"); // dummy values
                Person.appendChild(cvParam3);
                    //AuditCollection/Person/cvParam4/
                    Element cvParam4 = doc.createElement("cvParam");
                    cvParam4.setAttribute("cvRef", "PSI-MS");
                    cvParam4.setAttribute("accession", "MS:1000590");
                    cvParam4.setAttribute("name", "contact organization");
                    cvParam4.setAttribute("value", "(Not specified) Home institution of the contact person"); // dummy values
                Person.appendChild(cvParam4);
            AuditCollection.appendChild(Person);
                // *******************************
                // AuditCollection/Organization/
                // N.B. There can be as much as 3 Organizational elements or more; each representing
                // a. file owner's institution b. institution of origin of M2Lite, and c. Thermo Scientific's address 
                // ********************************
                Element Organization = doc.createElement("Organization");
                Organization.setAttribute("id","ORG_DOC_OWNER"); //dummy values
                Organization.setAttribute("name","PLACE_HOLDER"); //dummy values
                    //AuditCollection/Organization/cvParam1
                    Element orgCVParam1 = doc.createElement("cvParam");
                    orgCVParam1.setAttribute("cvRef", "PSI-MS");
                    orgCVParam1.setAttribute("accession", "MS:1000586");
                    orgCVParam1.setAttribute("name", "contact name");
                    orgCVParam1.setAttribute("value", "fname lname"); // dummy values
               Organization.appendChild(orgCVParam1);
                    //AuditCollection/Organization/cvParam2
                    Element orgCVParam2 = doc.createElement("cvParam");
                    orgCVParam2.setAttribute("cvRef", "PSI-MS");
                    orgCVParam2.setAttribute("accession", "MS:1000587");
                    orgCVParam2.setAttribute("name", "contact address");
                    orgCVParam2.setAttribute("value", "#123 ABC Street, XY891011"); // dummy values
               Organization.appendChild(orgCVParam2);
                    //AuditCollection/Organization/cvParam3
                    Element orgCVParam3 = doc.createElement("cvParam");
                    orgCVParam3.setAttribute("cvRef", "PSI-MS");
                    orgCVParam3.setAttribute("accession", "MS:1000588");
                    orgCVParam3.setAttribute("name", "contact URL");
                    orgCVParam3.setAttribute("value", "www.123mzid.com"); // dummy values
               Organization.appendChild(orgCVParam3);
                    //AuditCollection/Organization/cvParam4
                    Element orgCVParam4 = doc.createElement("cvParam");
                    orgCVParam4.setAttribute("cvRef", "PSI-MS");
                    orgCVParam4.setAttribute("accession", "MS:1000589");
                    orgCVParam4.setAttribute("name", "contact email");
                    orgCVParam4.setAttribute("value", "mzid@123mzid.com"); // dummy values
               Organization.appendChild(orgCVParam4);               
            AuditCollection.appendChild(Organization);
                
                // ********************************
                // M2Lite Owner
                Element Organization2 = doc.createElement("Organization");
                Organization2.setAttribute("id","M2Lite_OWNER"); 
                Organization2.setAttribute("name","Center for Biomarker Discovery and Translation, Dept of Pathology, Johns Hopkins Medical Inst"); 
                    //AuditCollection/Organization/cvParam1
                    Element org2CVParam1 = doc.createElement("cvParam");
                    org2CVParam1.setAttribute("cvRef", "PSI-MS");
                    org2CVParam1.setAttribute("accession", "MS:1000586");
                    org2CVParam1.setAttribute("name", "contact name");
                    org2CVParam1.setAttribute("value", "Paul Aiyetan"); 
               Organization2.appendChild(org2CVParam1);
                    //AuditCollection/Organization/cvParam2
                    Element org2CVParam2 = doc.createElement("cvParam");
                    org2CVParam2.setAttribute("cvRef", "PSI-MS");
                    org2CVParam2.setAttribute("accession", "MS:1000587");
                    org2CVParam2.setAttribute("name", "contact address");
                    org2CVParam2.setAttribute("value", "1550 Orleans Street, CRB II, Rm 3M 01-07"); // dummy values
               Organization2.appendChild(org2CVParam2);
                    //AuditCollection/Organization/cvParam3
                    Element org2CVParam3 = doc.createElement("cvParam");
                    org2CVParam3.setAttribute("cvRef", "PSI-MS");
                    org2CVParam3.setAttribute("accession", "MS:1000588");
                    org2CVParam3.setAttribute("name", "contact URL");
                    org2CVParam3.setAttribute("value", "https://bitbucket.org/paiyetan/m2lite"); // dummy values
               Organization2.appendChild(org2CVParam3);
                    //AuditCollection/Organization/cvParam4
                    Element org2CVParam4 = doc.createElement("cvParam");
                    org2CVParam4.setAttribute("cvRef", "PSI-MS");
                    org2CVParam4.setAttribute("accession", "MS:1000589");
                    org2CVParam4.setAttribute("name", "contact email");
                    org2CVParam4.setAttribute("value", "paiyeta1@jhmi.edu"); // dummy values
               Organization2.appendChild(org2CVParam4);               
            AuditCollection.appendChild(Organization2);
            
                // ********************************
                // Thermo
                Element Organization3 = doc.createElement("Organization");
                Organization3.setAttribute("id","PD_OWNER"); //dummy values
                Organization3.setAttribute("name","Thermo Fisher Scientific Inc."); //dummy values
                    //AuditCollection/Organization/cvParam1
                    Element org3CVParam1 = doc.createElement("cvParam");
                    org3CVParam1.setAttribute("cvRef", "PSI-MS");
                    org3CVParam1.setAttribute("accession", "MS:1000586");
                    org3CVParam1.setAttribute("name", "contact name");
                    org3CVParam1.setAttribute("value", "Mary Lopez"); // dummy values
               Organization3.appendChild(org3CVParam1);
                    //AuditCollection/Organization/cvParam2
                    Element org3CVParam2 = doc.createElement("cvParam");
                    org3CVParam2.setAttribute("cvRef", "PSI-MS");
                    org3CVParam2.setAttribute("accession", "MS:1000587");
                    org3CVParam2.setAttribute("name", "contact address");
                    org3CVParam2.setAttribute("value", "81 Wyman Street, Waltham, MA 02454, USA"); 
               Organization3.appendChild(org3CVParam2);
                    //AuditCollection/Organization/cvParam3
                    Element org3CVParam3 = doc.createElement("cvParam");
                    org3CVParam3.setAttribute("cvRef", "PSI-MS");
                    org3CVParam3.setAttribute("accession", "MS:1000588");
                    org3CVParam3.setAttribute("name", "contact URL");
                    org3CVParam3.setAttribute("value", "www.thermo.com"); // 
               Organization3.appendChild(org3CVParam3);
                    //AuditCollection/Organization/cvParam4
                    Element org3CVParam4 = doc.createElement("cvParam");
                    org3CVParam4.setAttribute("cvRef", "PSI-MS");
                    org3CVParam4.setAttribute("accession", "MS:1000589");
                    org3CVParam4.setAttribute("name", "contact email");
                    org3CVParam4.setAttribute("value", "mary.lopez@thermo.com"); // dummy values
               Organization3.appendChild(org3CVParam4);               
            AuditCollection.appendChild(Organization3);
            
            rootElement.appendChild(AuditCollection);
                       
            // ******************************************* //
            // SequenceCollection
            // ******************************************* //
            System.out.println("\tMaking 'SequenceCollection' document elements...");
            SequenceCollection.setAttribute("xmlns", "http://psidev.info/psi/pi/mzIdentML/1.1");
                // SequenceCollection/DBSequence/
                // List all protein sequences to be referenced from elsewhere
                // get all unique peptide [Unique by modifications]
                //System.out.println("\t extracting mapped 'msf' proteins...");
                ArrayList<MSFPSMProtein> mappedMSFProteins = 
                        getUniqueMappedMSFProteins(pepID2PeptideInstancesMap);
                //System.out.println("\t " + mappedMSFProteins.size() + " total msf proteins mapped...");
                
                Iterator<MSFPSMProtein> itr = mappedMSFProteins.iterator();
                String searchedDB = parametersMap.get("FastaDatabase");
                while(itr.hasNext()){
                    MSFPSMProtein protein = itr.next();
                //SequenceCollection/DBSequence/
                    Element DBSequence = doc.createElement("DBSequence");
                    //make DBSeq Attributes
                    /*
                    * @length
                    * @searchDatabaseRef
                    * @accession
                    * @id
                    * @name
                    */
                    DBSequence.setAttribute("id",
                            (new StringBuilder()).append("DBSeq_").append(protein.getMyriMatchDescription()).toString());
                    DBSequence.setAttribute("accession",protein.getMyriMatchDescription());
                    DBSequence.setAttribute("searchDatabase_ref",searchedDB);
                    //DBSequence.setAttribute("length",String.valueOf(protein.getSequence().length()));
                    
                    /*
                     * 
                     * (Optional parameters)
                    //SequenceCollection/DBSequence/Seq/                
                        Element DBSequenceSeqElement = doc.createElement("Seq");
                            DBSequenceSeqElement.appendChild(doc.createTextNode(protein.getSequence()));
                    DBSequence.appendChild(DBSequenceSeqElement);
                    */
                    
                    //Create the DBSequenceCVParamElement
                    //SequenceCollection/DBSequence/cvParam/  
                        Element DBSequenceCVParamElement = doc.createElement("cvParam");
                        DBSequenceCVParamElement.setAttribute("cvRef", "PSI-MS");
                        DBSequenceCVParamElement.setAttribute("accession", "MS:1001088");
                        DBSequenceCVParamElement.setAttribute("name", "protein description");
                        DBSequenceCVParamElement.setAttribute("value", protein.getCleanDescription());
                    DBSequence.appendChild(DBSequenceCVParamElement);
                    //Create the DBSequenceUserParamElement
                    //SequenceCollection/DBSequence/userParam/  
                    //    Element DBSequenceUserParamElement = doc.createElement("userParam");
                    //    DBSequenceUserParamElement.setAttribute("name", "protein score");
                    //    DBSequenceUserParamElement.setAttribute("value", String.valueOf(protein.getProteinScore()));
                    //DBSequence.appendChild(DBSequenceUserParamElement);
                    //append DBSequence element to Sequence Collection
                   
                SequenceCollection.appendChild(DBSequence);
                }
            
            //<Peptide/>List all peptide (unique) sequences and modifications to be referenced from elsewhere
            //This pretty much correponds to identified psms.
            Set<Integer> pepIDs = pepID2MSFPeptideMap.keySet();
            Iterator<Integer> itr2 = pepIDs.iterator();
            while(itr2.hasNext()){
                int peptideID = itr2.next();
                MSFPSMPeptide msfPep = pepID2MSFPeptideMap.get(peptideID);
                Element Peptide = doc.createElement("Peptide");
                Peptide.setAttribute("id", (new StringBuilder()).append("PEP_").append(msfPep.getPeptideID()).toString());
                        
                        
                Element PeptideSequence = doc.createElement("PeptideSequence");
                PeptideSequence.appendChild(doc.createTextNode(msfPep.getSequence()));
                Peptide.appendChild(PeptideSequence);
                
                LinkedList<MSFPSMModification> modfxns = msfPep.getModifications();
                for(int i = 0; i < modfxns.size(); i++){
                    Element Modification = doc.createElement("Modification");
                    MSFPSMModification modfxn = modfxns.get(i);
                    String modfxnLocation = String.valueOf(modfxn.getPosition()+1);
                    // ***** MyriMatch-like/IDPicker compartible object model ******
                    if(modfxn.isTerminal() & modfxn.getModificationName().equals("iTRAQ4plex"))
                        modfxnLocation = "0"; // that means it's a terminal modification and IDPicker would read it as location 0;
                    // ************************************************************* //
                    Modification.setAttribute("location", modfxnLocation);
                    Modification.setAttribute("residues", String.valueOf(msfPep.getSequence().charAt(modfxn.getPosition())));
                    Modification.setAttribute("avgMassDelta", String.valueOf(modfxn.getDeltaMass()));
                    Modification.setAttribute("monoisotopicMassDelta", String.valueOf(modfxn.getDeltaMass()));
                    Element ModCVParam = doc.createElement("cvParam");
                    if(modfxn.isTerminal()){
                        // ***** MyriMatch-like/IDPicker compartible object model ******
                        //<cvParam cvRef="MS" accession="MS:1001460" name="unknown modification" value=""/>
                        ModCVParam.setAttribute("cvRef", "PSI-MS");
                        ModCVParam.setAttribute("accession", "MS:1001460");
                        ModCVParam.setAttribute("name","unknown modification"); //myrimatch reads terminal modifications as unknown
                        ModCVParam.setAttribute("value","");
                        // ************************************************************* //
                    } else {
                        ModCVParam.setAttribute("cvRef", "UNIMOD");
                        ModCVParam.setAttribute("accession", (new StringBuilder()).append("UNIMOD:").append(modTermMap.get(modfxn.getModificationName())).toString());
                        ModCVParam.setAttribute("name", modfxn.getModificationName());
                        ModCVParam.setAttribute("value",""); //myriMatch-like IDPicker-compatible empty value
                    }
                    Modification.appendChild(ModCVParam);
                    Peptide.appendChild(Modification);
                }                
                SequenceCollection.appendChild(Peptide);
            }            
            //<PeptideEvidence/> List the location of all peptide sequences within protein sequences here, assuming digestion has 
            //                     taken place according to the specifed enzyme e.g. for full tryptic digestion,
            //                     insert, give the locations of all peptides listed above (termininating with R or K) 
            //                     with a pre= "R" or "K" or "-" for N-terminus 
            //                   A somewhat peptide to protein mapping
            Set<Integer> mappedIDs = pepID2PeptideInstancesMap.keySet();
            Iterator<Integer> itr3 = mappedIDs.iterator();
            HashMap<String, Element> peptideEvidences = new HashMap<String, Element>(); //using a map to speed up look up process.
            while(itr3.hasNext()){
                int peptideID = itr3.next();
                LinkedList<Peptide> pepInstances = pepID2PeptideInstancesMap.get(peptideID);
                boolean isDecoy = pepID2MSFPeptideMap.get(peptideID).isDecoy();
                //For each uniquely modified peptide,
                //get the mapped peptides(ProteinInstances) returned for the sequence and 
                //print the mapped proteins.
                if(pepInstances != null){
                    for(int i = 0; i < pepInstances.size(); i++){
                        Peptide pepInstance = pepInstances.get(i);
                        String refPeptideID = (new StringBuilder()).append("PEP_").append(peptideID).toString();
                        String referenceProtID = (new StringBuilder()).append("DBSeq_").append(pepInstance.getMSFProteinParent().getMyriMatchDescription()).toString();
                        String pepEvidenceID = (new StringBuilder()).append(referenceProtID).append("_").append(refPeptideID).toString();
                        String SequenceWithNeighbors = pepInstance.getSeqWithNeighbors();
                        
                        Element PeptideEvidence = doc.createElement("PeptideEvidence");
                        
                        PeptideEvidence.setAttribute("id", pepEvidenceID);                
                        PeptideEvidence.setAttribute("peptide_ref", refPeptideID);
                        PeptideEvidence.setAttribute("dBSequence_ref", referenceProtID);
                        PeptideEvidence.setAttribute("pre", String.valueOf(pepInstance.getSeqWithNeighbors().charAt(0)));
                        PeptideEvidence.setAttribute("post", String.valueOf(SequenceWithNeighbors.charAt(SequenceWithNeighbors.length()-1)));
                        PeptideEvidence.setAttribute("start", String.valueOf(pepInstance.getStart()));
                        PeptideEvidence.setAttribute("end", String.valueOf(pepInstance.getEnd()));
                        PeptideEvidence.setAttribute("isDecoy", String.valueOf(isDecoy));
                        if(peptideEvidences.containsKey(pepEvidenceID)==false){
                            SequenceCollection.appendChild(PeptideEvidence);
                            peptideEvidences.put(pepEvidenceID, PeptideEvidence);
                        }                      
                    }
                }
            }
            peptideEvidences = null; //free-up...
            rootElement.appendChild(SequenceCollection);
            // ******************************************* //
            // AnalysisCollection
            // ******************************************* //
            System.out.println("\tMaking 'AnalysisCollection' document elements...");
            
            Element SpectrumIdentification = doc.createElement("SpectrumIdentification");
            Element ProteinDetection = doc.createElement("ProteinDetection");
            
            //AnalysisCollection/SpectrumIdentification
            SpectrumIdentification.setAttribute("spectrumIdentificationList_ref", "SIL_1");
            SpectrumIdentification.setAttribute("spectrumIdentificationProtocol_ref", "Search_Protocol_1");
            SpectrumIdentification.setAttribute("id", "SpecIdent_1");
                //AnalysisCollection/SpectrumIdentification/InputSpectra
                Element spectIdentInputSpectra = doc.createElement("InputSpectra");
                spectIdentInputSpectra.setAttribute("spectraData_ref", "SpectraData_1");
            SpectrumIdentification.appendChild(spectIdentInputSpectra);
                //AnalysisCollection/SpectrumIdentification/SearchDatabaseRef
                Element specIdentSearchDatabaseRef = doc.createElement("SearchDatabaseRef");
                specIdentSearchDatabaseRef.setAttribute("searchDatabase_ref", parametersMap.get("FastaDatabase"));
            SpectrumIdentification.appendChild(specIdentSearchDatabaseRef);
                    
            AnalysisCollection.appendChild(SpectrumIdentification);
            
            // ********** skip ProteinDetection [default mode] ************ //
            // However, 
            if(includeProteinsInferred){
                //AnalysisCollection/ProteinDetection
                ProteinDetection.setAttribute("proteinDetectionList_ref", "");
                ProteinDetection.setAttribute("proteinDetectionProtocol_ref", "");
                ProteinDetection.setAttribute("id", "");
                
                // .... //
                
                AnalysisCollection.appendChild(ProteinDetection);
            }           
            rootElement.appendChild(AnalysisCollection);
                        
            
            // ******************************************* //
            // AnalysisProtocolCollection: Protocol used for spectrum identification, most typical parameters
            // ******************************************* //
            System.out.println("\tMaking 'AnalysisProtocolCollection' document elements...");
            
            Element SpectrumIdentificationProtocol = doc.createElement("SpectrumIdentificationProtocol");
            //Element ProteinDetectionProtocol = doc.createElement("ProteinDetectionProtocol");
            
            //AnalysisProtocolCollection/SpectrumIdentificationProtocol
            SpectrumIdentificationProtocol.setAttribute("analysisSoftware_ref", "PDv1.3");
            SpectrumIdentificationProtocol.setAttribute("id", "Search_Protocol_1");
                
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/SearchType
                Element specIdentProtSearchType = doc.createElement("SearchType");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/SearchType/cvParam
                    Element specIdentProtSearchTypeCVParam = doc.createElement("cvParam");
                    specIdentProtSearchTypeCVParam.setAttribute("accession", "MS:1001083");
                    specIdentProtSearchTypeCVParam.setAttribute("cvRef", "PSI-MS");
                    specIdentProtSearchTypeCVParam.setAttribute("name", "ms-ms search");
                specIdentProtSearchType.appendChild(specIdentProtSearchTypeCVParam);
            SpectrumIdentificationProtocol.appendChild(specIdentProtSearchType);
                
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams
                Element specIdentProtAdditionalSearchParams = doc.createElement("AdditionalSearchParams");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/cvParams
                    Element specIdentProtAddSearchPCVParams1 = doc.createElement("cvParam");
                    specIdentProtAddSearchPCVParams1.setAttribute("accession", "MS:1001211");
                    specIdentProtAddSearchPCVParams1.setAttribute("cvRef", "PSI-MS");
                    specIdentProtAddSearchPCVParams1.setAttribute("name", "parent mass type mono");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/AdditionalSearchParams/cvParams
                    Element specIdentProtAddSearchPCVParams2 = doc.createElement("cvParam");
                    specIdentProtAddSearchPCVParams2.setAttribute("accession", "MS:1001256");
                    specIdentProtAddSearchPCVParams2.setAttribute("cvRef", "PSI-MS");
                    specIdentProtAddSearchPCVParams2.setAttribute("name", "fragment mass type mono");
                specIdentProtAdditionalSearchParams.appendChild(specIdentProtAddSearchPCVParams1);
                specIdentProtAdditionalSearchParams.appendChild(specIdentProtAddSearchPCVParams2);
            SpectrumIdentificationProtocol.appendChild(specIdentProtAdditionalSearchParams);
                
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams
                Element specIdentProtModificationParams = doc.createElement("ModificationParams");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification
                    //getDynamicModifications
                    ArrayList<MSFSearchModificationParam> dModfxns = getSearchModificationParams(parametersMap,"DynMod");
                    //  for each dynamic modification returned, 
                    //  make a modification node with the attending properties
                    Iterator<MSFSearchModificationParam> modsItr = dModfxns.iterator();
                    while(modsItr.hasNext()){
                        MSFSearchModificationParam mod = modsItr.next();
                        Element specIdentProtSearchModification = doc.createElement("SearchModification");
                        
                        String residue = mod.getResidue();
                        if(residue.equalsIgnoreCase("AnyN-Terminus")){
                            residue = ".";
                        }
                        specIdentProtSearchModification.setAttribute("residues", residue);
                        specIdentProtSearchModification.setAttribute("massDelta", String.valueOf(mod.getDeltaMass()));
                        specIdentProtSearchModification.setAttribute("fixedMod", "false"); // dynamic
                            //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification/cvParam
                            Element specIdentProtSearchModCVParam = doc.createElement("cvParam");
                            specIdentProtSearchModCVParam.setAttribute("accession",(new StringBuilder()).append("UNIMOD:").append(modTermMap.get(mod.getName())).toString());
                            specIdentProtSearchModCVParam.setAttribute("cvRef", "UNIMOD");
                            specIdentProtSearchModCVParam.setAttribute("name", mod.getName());
                        specIdentProtSearchModification.appendChild(specIdentProtSearchModCVParam);
                    specIdentProtModificationParams.appendChild(specIdentProtSearchModification);
                       
                    }                    
                    //getStaticModifications;
                    ArrayList<MSFSearchModificationParam> sModfxns = getSearchModificationParams(parametersMap,"StatMod");
                    //  for each static modification returned, 
                    //  make a modification node with the attending properties
                    Iterator<MSFSearchModificationParam> statModsItr = sModfxns.iterator();
                    while(statModsItr.hasNext()){
                        MSFSearchModificationParam mod = statModsItr.next();
                        Element specIdentProtSearchModification = doc.createElement("SearchModification");
                        String residue = mod.getResidue();
                        if(residue.equalsIgnoreCase("AnyN-Terminus")){
                            residue = ".";
                        }
                        specIdentProtSearchModification.setAttribute("residues", residue);
                        specIdentProtSearchModification.setAttribute("massDelta", String.valueOf(mod.getDeltaMass()));
                        specIdentProtSearchModification.setAttribute("fixedMod", "true"); // dynamic
                            //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ModificationParams/SearchModification/cvParam
                            Element specIdentProtSearchModCVParam = doc.createElement("cvParam");
                            specIdentProtSearchModCVParam.setAttribute("accession", (new StringBuilder()).append("UNIMOD:").append(modTermMap.get(mod.getName())).toString());
                            specIdentProtSearchModCVParam.setAttribute("cvRef", "UNIMOD");
                            specIdentProtSearchModCVParam.setAttribute("name", mod.getName());
                        specIdentProtSearchModification.appendChild(specIdentProtSearchModCVParam);
                    specIdentProtModificationParams.appendChild(specIdentProtSearchModification);                     
                    }               
            SpectrumIdentificationProtocol.appendChild(specIdentProtModificationParams);   
                
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Enzymes
                Element spectIdentProtEnzymes = doc.createElement("Enzymes");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Enzymes
                    Element spectIdentProtEnzymesEnzyme = doc.createElement("Enzyme");
                    spectIdentProtEnzymesEnzyme.setAttribute("id","Enzyme_1"); // optionally increase the digit for subsequent enzymes reported
                    spectIdentProtEnzymesEnzyme.setAttribute("name",parametersMap.get("Enzyme"));
                    spectIdentProtEnzymesEnzyme.setAttribute("missedCleavages",parametersMap.get("MaxMissedCleavages"));
                spectIdentProtEnzymes.appendChild(spectIdentProtEnzymesEnzyme);
            SpectrumIdentificationProtocol.appendChild(spectIdentProtEnzymes);
                
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance
                Element specIdentProtFragmentTolerance = doc.createElement("FragmentTolerance");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
                    Element specIdentProtFragTolCVParam = doc.createElement("cvParam");
                    specIdentProtFragTolCVParam.setAttribute("accession", "MS:1001412");
                    specIdentProtFragTolCVParam.setAttribute("cvRef", "PSI-MS");
                    specIdentProtFragTolCVParam.setAttribute("unitCvRef", "UO");
                    specIdentProtFragTolCVParam.setAttribute("unitName", "dalton"); //may need to automatically figure this out from map
                    specIdentProtFragTolCVParam.setAttribute("unitAccession", "UO:0000221");
                    specIdentProtFragTolCVParam.setAttribute("value", getIonTolerance(parametersMap,"FragmentTolerance"));
                    specIdentProtFragTolCVParam.setAttribute("name", "search tolerance plus value");
                specIdentProtFragmentTolerance.appendChild(specIdentProtFragTolCVParam);
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
                    Element specIdentProtFragTolCVParam2 = doc.createElement("cvParam");
                    specIdentProtFragTolCVParam2.setAttribute("accession", "MS:1001413");
                    specIdentProtFragTolCVParam2.setAttribute("cvRef", "PSI-MS");
                    specIdentProtFragTolCVParam2.setAttribute("unitCvRef", "UO");
                    specIdentProtFragTolCVParam2.setAttribute("unitName", "dalton"); //may need to automatically figure this out from map
                    specIdentProtFragTolCVParam2.setAttribute("unitAccession", "UO:0000221");
                    specIdentProtFragTolCVParam2.setAttribute("value", getIonTolerance(parametersMap,"FragmentTolerance"));
                    specIdentProtFragTolCVParam2.setAttribute("name", "search tolerance minus value");
                specIdentProtFragmentTolerance.appendChild(specIdentProtFragTolCVParam2);
            SpectrumIdentificationProtocol.appendChild(specIdentProtFragmentTolerance);
                          
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/ParentTolerance
                Element specIdentProtParentTolerance = doc.createElement("ParentTolerance");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
                    Element specIdentProtParentTolCVParam = doc.createElement("cvParam");
                    specIdentProtParentTolCVParam.setAttribute("accession", "MS:1001412");
                    specIdentProtParentTolCVParam.setAttribute("cvRef", "PSI-MS");
                    specIdentProtParentTolCVParam.setAttribute("unitCvRef", "UO");
                    specIdentProtParentTolCVParam.setAttribute("unitName", "parts per million"); //may need to automatically figure this out from map
                    specIdentProtParentTolCVParam.setAttribute("unitAccession", "UO:0000169"); //may need to automatically figure this out from map
                    specIdentProtParentTolCVParam.setAttribute("value", getIonTolerance(parametersMap,"PeptideTolerance"));
                    specIdentProtParentTolCVParam.setAttribute("name", "search tolerance plus value");
                specIdentProtParentTolerance.appendChild(specIdentProtParentTolCVParam);
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/FragmentTolerance/cvParam
                    Element specIdentProtParentTolCVParam2 = doc.createElement("cvParam");
                    specIdentProtParentTolCVParam2.setAttribute("accession", "MS:1001413");
                    specIdentProtParentTolCVParam2.setAttribute("cvRef", "PSI-MS");
                    specIdentProtParentTolCVParam2.setAttribute("unitCvRef", "UO");
                    specIdentProtParentTolCVParam2.setAttribute("unitName", "parts per million"); //may need to automatically figure this out from map
                    specIdentProtParentTolCVParam2.setAttribute("unitAccession", "UO:0000169"); //may need to automatically figure this out from map
                    specIdentProtParentTolCVParam2.setAttribute("value", getIonTolerance(parametersMap, "PeptideTolerance"));
                    specIdentProtParentTolCVParam2.setAttribute("name", "search tolerance minus value");
                specIdentProtParentTolerance.appendChild(specIdentProtParentTolCVParam2);
             SpectrumIdentificationProtocol.appendChild(specIdentProtParentTolerance);
                
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Threshold - Threshold value must be included, even if no threshold. This can include FDR, p-value etc
                Element specIdentProtThreshold = doc.createElement("Threshold");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/Threshold/cvParam 
                    Element specIdentProtThresholdParamCVParam = doc.createElement("cvParam");
                    //specIdentProtThresholdParamCVParam.setAttribute("accession", "MS:1001364");
                    specIdentProtThresholdParamCVParam.setAttribute("accession", "MS:1001494");
                    specIdentProtThresholdParamCVParam.setAttribute("cvRef", "PSI-MS");
                    //specIdentProtThresholdParamCVParam.setAttribute("name", "pep:global FDR");
                    specIdentProtThresholdParamCVParam.setAttribute("name", "no threshold");
                    //specIdentProtThresholdParamCVParam.setAttribute("value", parametersMap.get("TargetFPRHigh"));
                specIdentProtThreshold.appendChild(specIdentProtThresholdParamCVParam);
            SpectrumIdentificationProtocol.appendChild(specIdentProtThreshold);
            
            /*
             * MS:1001494 (no threshold) only once
               MS:1001448 (pep:FDR threshold)
             */
                //AnalysisProtocolCollection/SpectrumIdentificationProtocol/MassTable
                Element specIdentProtMassTable = doc.createElement("MassTable");
                specIdentProtMassTable.setAttribute("id", "MT");
                specIdentProtMassTable.setAttribute("msLevel", "1 2 3");
                    //AnalysisProtocolCollection/SpectrumIdentificationProtocol/MassTable/Residue
                    MSFPSMMasses msfMasses = new MSFPSMMasses();
                    HashMap<Character,Double> massTable = msfMasses.getMassTable();
                    Set<Map.Entry<Character,Double>> mTMapEntries = massTable.entrySet();
                    Iterator<Map.Entry<Character,Double>> mTMapEntriesItr = mTMapEntries.iterator();
                    while(mTMapEntriesItr.hasNext()){
                        Map.Entry<Character,Double> mTMapEntry = mTMapEntriesItr.next();
                        char residue = mTMapEntry.getKey();
                        double monoisotopicMass = mTMapEntry.getValue();
                        //AnalysisProtocolCollection/SpectrumIdentificationProtocol/MassTable/Residue
                        Element specIdentProtMassTableResidue = doc.createElement("Residue");
                        specIdentProtMassTableResidue.setAttribute("code", String.valueOf(residue));
                        specIdentProtMassTableResidue.setAttribute("mass", String.valueOf(monoisotopicMass));
                        specIdentProtMassTable.appendChild(specIdentProtMassTableResidue);
                    }
            SpectrumIdentificationProtocol.appendChild(specIdentProtMassTable);    
                              
            AnalysisProtocolCollection.appendChild(SpectrumIdentificationProtocol);
            
            // ************* 'ProteinDetectionProtocol' ************************* //
            // by default however, if (includeProteinsInferred) is 'true',
            /*
            if(includeProteinsInferred){
                
                //module stump
                //AnalysisProtocolCollection/ProteinDetectionProtocol
                ProteinDetectionProtocol.setAttribute("analysisSoftware_ref", "PDv1.3");
                ProteinDetectionProtocol.setAttribute("id", "PDP_1"); //Protein Detection Protocol 1
                    //AnalysisProtocolCollection/ProteinDetectionProtocol/AnalysisParams
                    Element AnalysisParams = doc.createElement("AnalysisParams");
                        //AnalysisProtocolCollection/ProteinDetectionProtocol/AnalysisParams/cvParam
                        Element pDPAnalysisParCVParam = doc.createElement("cvParam");
                        pDPAnalysisParCVParam.setAttribute("cvRef", "PSI-MS");
                        pDPAnalysisParCVParam.setAttribute("accession", "MS:1001676");
                        pDPAnalysisParCVParam.setAttribute("name", "ProteomeDiscoverer:Maximum Protein References Per Peptide");
                        pDPAnalysisParCVParam.setAttribute("value", "100");
                    AnalysisParams.appendChild(pDPAnalysisParCVParam);
                ProteinDetectionProtocol.appendChild(AnalysisParams);
                    
                    Element Threshold = doc.createElement("Threshold");
                        Element thresholdCVParam = doc.createElement("cvParam");
                        thresholdCVParam.setAttribute("cvRef", "PSI-MS");
                        thresholdCVParam.setAttribute("accession", "MS:1001447");
                        thresholdCVParam.setAttribute("name", "prot:FDR threshold");
                        thresholdCVParam.setAttribute("value", "1.5"); //hard-coded
                    Threshold.appendChild(thresholdCVParam);
                 ProteinDetectionProtocol.appendChild(Threshold);                      
                        
              AnalysisProtocolCollection.appendChild(ProteinDetectionProtocol);
               
            }
            * 
            */
            
            rootElement.appendChild(AnalysisProtocolCollection);           
                    
            // ******************************************* //
            // DataCollection
            // ******************************************* //
            System.out.println("\tMaking 'DataCollection' document elements...");
            
            Element Inputs = doc.createElement("Inputs");
            Element AnalysisData = doc.createElement("AnalysisData");
            
            //DataCollection/Inputs
            //DataCollection/Inputs/SourceFile/
            Element SourceFile = doc.createElement("SourceFile"); //File converted to mzIdentML
            //SourceFile.setAttribute("location",msfPeps.get(1).getFileName().replaceAll(".raw", ".msf"));
            SourceFile.setAttribute("location",mzidMLOutFile.replaceAll(".mzid", ".msf"));
            //mzidMLOutFile
            
            SourceFile.setAttribute("id",new File(mzidMLOutFile.replaceAll(".mzid", ".msf")).getName());
            //DataCollection/Inputs/SourceFile/FileFormat
            Element FileFormat = doc.createElement("FileFormat");
            //DataCollection/Inputs/SourceFile/FileFormat/cvParam
            Element FileFormatCVParam = doc.createElement("cvParam");
            FileFormatCVParam.setAttribute("accession", "MS:1001564");
            FileFormatCVParam.setAttribute("cvRef", "PSI-MS");
            FileFormatCVParam.setAttribute("name", "Discoverer MSF");
            FileFormat.appendChild(FileFormatCVParam);
            SourceFile.appendChild(FileFormat);
            Inputs.appendChild(SourceFile);
            
            //DataCollection/Inputs/SearchDatabase
            Element SearchDatabase = doc.createElement("SearchDatabase");
            SearchDatabase.setAttribute("numDatabaseSequences", String.valueOf(db.getProteins().size()));
            SearchDatabase.setAttribute("location", searchDBPath); 
            SearchDatabase.setAttribute("id", parametersMap.get("FastaDatabase"));
            //DataCollection/Inputs/SearchDatabase/cvParam/@accession
            Element searchDBCVParam = doc.createElement("cvParam");
            searchDBCVParam.setAttribute("cvRef", "PSI-MS");
            searchDBCVParam.setAttribute("accession", "MS:1001195");
            searchDBCVParam.setAttribute("name", "decoy DB type reverse");
            SearchDatabase.appendChild(searchDBCVParam);
            //DataCollection/Inputs/SearchDatabase/FileFormat
            Element SearchDatabaseFileFormat = doc.createElement("FileFormat");
            Element SearchDatabaseFileFormatCVParam = doc.createElement("cvParam");
            SearchDatabaseFileFormatCVParam.setAttribute("cvRef", "PSI-MS");
            SearchDatabaseFileFormatCVParam.setAttribute("accession", "MS:1001348");
            SearchDatabaseFileFormatCVParam.setAttribute("name", "FASTA format");
            SearchDatabaseFileFormat.appendChild(SearchDatabaseFileFormatCVParam);
            SearchDatabase.appendChild(SearchDatabaseFileFormat);
            //DataCollection/Inputs/SearchDatabase/DatabaseName
            Element DatabaseName = doc.createElement("DatabaseName");
            Element DBNameUserParam = doc.createElement("userParam");
            DBNameUserParam.setAttribute("name", searchDBPath);
            DatabaseName.appendChild(DBNameUserParam);
            SearchDatabase.appendChild(DatabaseName);
            Inputs.appendChild(SearchDatabase);
            
            //DataCollection/Inputs/SpectraData
            //Iterator<MSFPSMPeptide> itr4 = msfPeps.iterator();
            //while(itr4.hasNext()){
                //MSFPSMPeptide msfPep = itr4.next();
                Element SpectraData = doc.createElement("SpectraData");
                //SpectraData.setAttribute("location",msfPep.getFileName());
                SpectraData.setAttribute("location",parametersMap.get("SpectrumFileNames"));
                SpectraData.setAttribute("id","SpectraData_1"); //spectra ID as the .RAW file name
                //DataCollection/Inputs/SpectraData/FileFormat
                Element SpectraDataFileFormat = doc.createElement("FileFormat");
                Element SpectraDataFileFormatCVParam = doc.createElement("cvParam");
                SpectraDataFileFormatCVParam.setAttribute("accession", "MS:1000563");
                SpectraDataFileFormatCVParam.setAttribute("cvRef", "PSI-MS");
                SpectraDataFileFormatCVParam.setAttribute("name", "Thermo RAW file");
                SpectraDataFileFormat.appendChild(SpectraDataFileFormatCVParam);
                SpectraData.appendChild(SpectraDataFileFormat);
                //DataCollection/Inputs/SpectraData/SpectrumIDFormat
                Element SpectrumIDFormat = doc.createElement("SpectrumIDFormat");
                Element SpectrumIDFormatCVParam = doc.createElement("cvParam");
                SpectrumIDFormatCVParam.setAttribute("accession", "MS:1000768");
                SpectrumIDFormatCVParam.setAttribute("cvRef", "PSI-MS");
                SpectrumIDFormatCVParam.setAttribute("name", "Thermo nativeID format");
                SpectrumIDFormat.appendChild(SpectrumIDFormatCVParam);
                SpectraData.appendChild(SpectrumIDFormat);
                Inputs.appendChild(SpectraData);               
            //}
             
            DataCollection.appendChild(Inputs);
            
            //DataCollection/AnalysisData/SpectrumIdentificationList
            Element SpectrumIdentificationList = doc.createElement("SpectrumIdentificationList");
            SpectrumIdentificationList.setAttribute("id", "SIL_1"); //SIL - Spectrum identification List.
            //Iterator<MSFPSMPeptide> itr5 = msfPeps.iterator();
            Set<Integer> spectraIDs = spectID2PepIDsMap.keySet();
            Iterator<Integer> itr5 = spectraIDs.iterator();
            
            while(itr5.hasNext()){
                //DataCollection/AnalysisData/SpectrumIdentificationList/SpectrumIdentificationResult
                int spectrumID = itr5.next();
                String spectrumIdResultID = (new StringBuilder()).append("SIR_").append(spectrumID).toString();
                MSFPSMPeptide sampleSpectraPeptide = null;
                if(spectID2PepIDsMap.get(spectrumID) != null){
                    sampleSpectraPeptide = pepID2MSFPeptideMap.get(spectID2PepIDsMap.get(spectrumID).get(0));
                    int spectraScanNumber = sampleSpectraPeptide.getFirstScan();
                    String SIRSpectrumID = (new StringBuilder()).append("controllerType=0 controllerNumber=1 scan=").append(spectraScanNumber).toString();
                    Element SpectrumIdentificationResult = doc.createElement("SpectrumIdentificationResult");
                    SpectrumIdentificationResult.setAttribute("id", spectrumIdResultID);
                    SpectrumIdentificationResult.setAttribute("spectrumID", SIRSpectrumID);
                    SpectrumIdentificationResult.setAttribute("spectraData_ref", "SpectraData_1");

                    //DataCollection/AnalysisData/SpectrumIdentificationList/SpectrumIdentificationResult/SpectrumIdentificationItem
                    LinkedList<Integer> mappedPeptideIDs = spectID2PepIDsMap.get(spectrumID);
                    for(int i = 0; i < mappedPeptideIDs.size(); i++){
                        int mappedPepID = mappedPeptideIDs.get(i);
                        MSFPSMPeptide msfPep = pepID2MSFPeptideMap.get(mappedPepID);
                        String referencePepID = (new StringBuilder()).append("PEP_").append(msfPep.getPeptideID()).toString();

                        Element SpectrumIdentificationItem = doc.createElement("SpectrumIdentificationItem");
                        boolean passThreshold = true; // default value for no threshold filtering...
                        MSFPSMMasses msfPepMasses = new MSFPSMMasses(msfPep);
                        
                        SpectrumIdentificationItem.setAttribute("id",(new StringBuilder()).append(spectrumIdResultID).append("_").append("SII_").append(i).toString());
                        SpectrumIdentificationItem.setAttribute("rank",String.valueOf(msfPep.getSearchEngineRank()));
                        SpectrumIdentificationItem.setAttribute("chargeState", String.valueOf(msfPep.getCharge()));
                        SpectrumIdentificationItem.setAttribute("peptide_ref",referencePepID);
                        SpectrumIdentificationItem.setAttribute("experimentalMassToCharge",String.valueOf(msfPepMasses.getObservedMassToCharge()));
                        SpectrumIdentificationItem.setAttribute("calculatedMassToCharge",String.valueOf(msfPepMasses.getTheoreticalMassToCharge()));
                        SpectrumIdentificationItem.setAttribute("passThreshold",String.valueOf(passThreshold));
                        SpectrumIdentificationItem.setAttribute("massTable_ref","MT");
                        

                        //Get peptide evidence references
                        LinkedList<Peptide> pepinstances = pepID2PeptideInstancesMap.get(msfPep.getPeptideID());
                        if(pepinstances != null){
                            for(int j = 0; j < pepinstances.size(); j++){
                                Peptide pepinstance = pepinstances.get(j);
                                String referenceProtID = (new StringBuilder()).append("DBSeq_").append(pepinstance.getMSFProteinParent().getMyriMatchDescription()).toString();
                                Element PeptideEvidenceRef = doc.createElement("PeptideEvidenceRef");
                                PeptideEvidenceRef.setAttribute("peptideEvidence_ref", 
                                        (new StringBuilder()).append(referenceProtID).append("_").append(referencePepID).toString());
                                SpectrumIdentificationItem.appendChild(PeptideEvidenceRef);
                            }
                        }
                        
                        //add PD derived scores
                        Element SpectrumIDSpScoreCVParam1 = doc.createElement("cvParam");
                        SpectrumIDSpScoreCVParam1.setAttribute("accession", "MS:1001155");
                        SpectrumIDSpScoreCVParam1.setAttribute("cvRef", "PSI-MS");
                        SpectrumIDSpScoreCVParam1.setAttribute("value", String.valueOf(msfPep.getXCorr()));
                        SpectrumIDSpScoreCVParam1.setAttribute("name", "SEQUEST:xcorr");

                        SpectrumIdentificationItem.appendChild(SpectrumIDSpScoreCVParam1);

                        Element SpectrumIDSpScoreCVParam2 = doc.createElement("cvParam");
                        SpectrumIDSpScoreCVParam2.setAttribute("accession", "MS:1001215");
                        SpectrumIDSpScoreCVParam2.setAttribute("cvRef", "PSI-MS");
                        SpectrumIDSpScoreCVParam2.setAttribute("value", String.valueOf(msfPep.getSpScore()));
                        SpectrumIDSpScoreCVParam2.setAttribute("name", "SEQUEST:PeptideSp");

                        SpectrumIdentificationItem.appendChild(SpectrumIDSpScoreCVParam2);                       
                        SpectrumIdentificationResult.appendChild(SpectrumIdentificationItem);
                    }
                    SpectrumIdentificationList.appendChild(SpectrumIdentificationResult);
                }               
            }
            AnalysisData.appendChild(SpectrumIdentificationList);
            
            // ***************** Skipping Proteins [default] *********************** //
            // however,
            if(includeProteinsInferred){
                //include proteing inferred from msf file data in output xml [.mzid] file
                // AnalysisData/ProteinDetectionList/
                //Element ProteinDetectionList = doc.createElement("ProteinDetectionList");
                //ProteinDetectionList.setAttribute("id", "PDL_1");
  
            }
                      
            DataCollection.appendChild(AnalysisData);
            rootElement.appendChild(DataCollection);
 
            // ********************************************************************* //
            // write content into xml [.mzid] file
            System.out.println("\tMaking '.mzid' output file...");
            
            /*
             * to indent output
             * //(1)
                TransformerFactory tf = TransformerFactory.newInstance();
                tf.setAttribute("indent-number", new Integer(2));
                //(2)
                Transformer t = tf.newTransformer();
                t.setOutputProperty(OutputKeys.INDENT, "yes");
                //(3)
                t.transform(new DOMSource(doc),
                new StreamResult(new OutputStreamWriter(out, "utf-8"));
             * 
             */
            
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", new Integer(3));
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                
            DOMSource source = new DOMSource(doc);
            //StreamResult result = new StreamResult(new File(mzidMLOutFile));
            StreamResult result = new StreamResult(new BufferedWriter(new FileWriter(mzidMLOutFile)));
            transformer.transform(source, result);
            System.out.println("...mzid file saved!");
 
        } catch (TransformerException ex) {
            Logger.getLogger(MSF2AllMZIdentMLPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MSF2AllMZIdentMLPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (IOException ex) {
            Logger.getLogger(MSF2AllMZIdentMLPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(MSF2AllMZIdentMLPrinter.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        
    }

    

    private ArrayList<MSFSearchModificationParam> getSearchModificationParams(HashMap<String,String> parametersMap,
                                                                            String pattern) {
        //throw new UnsupportedOperationException("Not yet implemented");
        ArrayList<MSFSearchModificationParam> mods = new ArrayList<MSFSearchModificationParam>();
        Set<String> keys = parametersMap.keySet();
        //Iterate through parameterMap keys, if a keymatches DynMod, get its value from the parameters map
        Iterator<String> itr = keys.iterator();
        while(itr.hasNext()){
            String key = itr.next();
            if(key.startsWith(pattern)){
                String mappedValue = parametersMap.get(key);
                String[] valueArr = mappedValue.split("/");
                String modName = valueArr[0].replaceAll("\\s+", ""); //removes all white spaces, get the modification name
                String[] dMassNresidue = valueArr[1].split("Da");
                double dMass = Double.parseDouble(dMassNresidue[0].replaceAll("\\s+",""));
                String residue = dMassNresidue[1].replace("(","").replace(")","").replaceAll("\\s+","");
                
                mods.add(new MSFSearchModificationParam(modName,dMass,residue));
            }
        }
        return mods;        
    }

    private String getIonTolerance(HashMap<String, String> parametersMap, String key) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String ionTolerance = 
                parametersMap.get(key).replace("Da","").replace("ppm","").replaceAll("\\s+","");       
        return ionTolerance;
    }

    private ArrayList<MSFPSMProtein> getUniqueMappedMSFProteins(HashMap<Integer, LinkedList<Peptide>> pepID2PeptideInstancesMap) {
        ArrayList<MSFPSMProtein> mappedProts = new ArrayList<MSFPSMProtein>();
        
        Set<Map.Entry<Integer,LinkedList<Peptide>>> mappings = pepID2PeptideInstancesMap.entrySet();
        Iterator<Map.Entry<Integer, LinkedList<Peptide>>> itr = mappings.iterator();
        while(itr.hasNext()){
            Map.Entry<Integer, LinkedList<Peptide>> pepID2Instances = itr.next();
            LinkedList<Peptide> instances = pepID2Instances.getValue();
            Iterator<Peptide> itr2 = instances.iterator();
            while(itr2.hasNext()){
                Peptide pep = itr2.next();
                MSFPSMProtein msfProt = pep.getMSFProteinParent();
                if(inList(msfProt, mappedProts)==false){
                    mappedProts.add(msfProt);
                }
            }
        }       
        return mappedProts;
    }

    private boolean inList(MSFPSMProtein msfProt, ArrayList<MSFPSMProtein> mappedProts) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean in = false;
        Iterator<MSFPSMProtein> itr = mappedProts.iterator();
        while(itr.hasNext()){
            MSFPSMProtein msfProt2 = itr.next();
            if(msfProt2.getDescription().equals(msfProt.getDescription())){
                in = true;
                return in;                
            }
        }        
        return in;
    }
    
}
