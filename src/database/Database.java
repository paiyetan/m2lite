/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 */
package database;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author paiyeta1
 */
public class Database {
    
    private ArrayList<Protein> proteins;
    
    public Database(String fullPath) throws FileNotFoundException, IOException{
        proteins = new ArrayList<Protein>();
        readDB(fullPath);
        
    }
    
    private void readDB(String dbPath) throws FileNotFoundException, IOException {
        System.out.println("\t\tReading-in local file Database...");
        BufferedReader in = new BufferedReader(new FileReader(new File(dbPath)));
        String line;
        //read everyLine into an arrayList of Strings
        //parse ArrayList to Arrays
        //by accessing arrays read line and components       
        ArrayList<String> dbLines = new ArrayList<String>();
        while((line = in.readLine())!=null){
            dbLines.add(line);                       
        }
        readDBHelper(dbLines);
              
    }
    
    private void readDBHelper(ArrayList<String> dbLines){
        String[] lines = new String[dbLines.size()];
        Iterator<String> itr = dbLines.iterator();
        int line = 0;
        while(itr.hasNext()){
            String dbLine = itr.next();
            lines[line] = dbLine;
            line++;
        }
        readDBHelper2(lines);
    }
    
    
    private void readDBHelper2(String[] dbLines){
        for(int i = 0; i < dbLines.length; i++){
            if (dbLines[i].charAt(0)=='>') { //that's a header
                String titleLine = dbLines[i];
                int j = i+1;
                String protein_seq = dbLines[j];
                while(( (j+1) != dbLines.length) && (dbLines[j+1].charAt(0)!='>')){
                    protein_seq = protein_seq + dbLines[j+1];
                    j++;
                }
                proteins.add(new Protein(titleLine, protein_seq));
                /*
                if(proteins.size()%5000==0){
                    System.out.println("\t\t"+proteins.size()+" protein entries read");
                }
                * 
                */
                
            }
        }
        System.out.println("\t\t"+proteins.size()+" protein entries read");
    }
  
    public ArrayList<Protein> getProteins(){
        return proteins;
    }    
}
