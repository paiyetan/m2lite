/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package pepscan;

import database.Peptide;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import msfproteins.MSFPSMProtein;
import msfpsms.MSFPSMPeptide;

/**
 *
 * @author paiyeta1
 */
public class MSFProtsScanner {
    
    HashMap<Integer,LinkedList<Peptide>> pepID2PeptideInstancesMap;
    ArrayList<MSFPSMProtein> uniqMappedMSFProteins; 
   
    public MSFProtsScanner(ArrayList<Integer> uniqPepIDs, 
                            MSFPSMProtein[] msfProts, 
                            HashMap<Integer,String> pepID2SeqMap) {
        pepID2PeptideInstancesMap = new HashMap<Integer,LinkedList<Peptide>>();
        uniqMappedMSFProteins = setUniqueMappedMSFProteins(msfProts);
        ArrayList<Integer> scannedPepIDs = new ArrayList<Integer>();
        Iterator<Integer> itr = uniqPepIDs.iterator();
        while(itr.hasNext()){
            int pepID2ScanFor = itr.next();
            if(scannedPepIDs.contains(pepID2ScanFor)==false){
                scan1(pepID2ScanFor, pepID2SeqMap);
                scannedPepIDs.add(pepID2ScanFor);
            }
        }        
    }

    public MSFProtsScanner(ArrayList<Integer> uniqPepIDs, 
                            MSFPSMProtein[] msfProts, 
                            MSFPSMProtein[] decoyMSFProts, 
                            HashMap<Integer, MSFPSMPeptide> pepID2MSFPeptideMap) {
        
        pepID2PeptideInstancesMap = new HashMap<Integer,LinkedList<Peptide>>();
        uniqMappedMSFProteins = setUniqueMappedMSFProteins(msfProts, decoyMSFProts);
        ArrayList<Integer> scannedPepIDs = new ArrayList<Integer>();
        Iterator<Integer> itr = uniqPepIDs.iterator();
        while(itr.hasNext()){
            int pepID2ScanFor = itr.next();
            if(scannedPepIDs.contains(pepID2ScanFor)==false){
                scan2(pepID2ScanFor, pepID2MSFPeptideMap);
                scannedPepIDs.add(pepID2ScanFor);
            }
        }  
    }

    private void scan2(int pepID2ScanFor, HashMap<Integer,MSFPSMPeptide> pepID2MSFPeptideMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        int peptideID = pepID2ScanFor;
        String peptideSequence = pepID2MSFPeptideMap.get(peptideID).getSequence();
        LinkedList<Peptide> peptideInstances = new LinkedList<Peptide>();
        
        Iterator<MSFPSMProtein> itr = uniqMappedMSFProteins.iterator();
        while(itr.hasNext()){
            MSFPSMProtein msfProt = itr.next();
            if(msfProt.getSequence().contains(peptideSequence)){
                Peptide peptide = msfProt.getPeptideInstance(peptideSequence);
                peptideInstances.add(peptide);
            }
        }
        pepID2PeptideInstancesMap.put(pepID2ScanFor, peptideInstances);
    }
    
    private void scan1(int pepID2ScanFor, HashMap<Integer,String> pepID2SeqMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        int peptideID = pepID2ScanFor;
        String peptideSequence = pepID2SeqMap.get(peptideID);
        LinkedList<Peptide> peptideInstances = new LinkedList<Peptide>();
        
        Iterator<MSFPSMProtein> itr = uniqMappedMSFProteins.iterator();
        while(itr.hasNext()){
            MSFPSMProtein msfProt = itr.next();
            if(msfProt.getSequence().contains(peptideSequence)){
                Peptide peptide = msfProt.getPeptideInstance(peptideSequence);
                peptideInstances.add(peptide);
            }
        }
        pepID2PeptideInstancesMap.put(pepID2ScanFor, peptideInstances);
    }
    
   
    private ArrayList<MSFPSMProtein> setUniqueMappedMSFProteins(MSFPSMProtein[] msfProts) {
        //throw new UnsupportedOperationException("Not yet implemented");
        System.out.println("Of " + msfProts.length + " MSFProteins..");
        ArrayList<MSFPSMProtein> uniqs = new ArrayList<MSFPSMProtein>();
        //Iterator<MSFPSMProtein> itr = msfProts.iterator();
        //while(itr.hasNext()){
        for(MSFPSMProtein prot: msfProts){
            //MSFPSMProtein prot = itr.next();
            if(!inUniqs(prot,uniqs)){
                uniqs.add(prot);
            }
        }       
        System.out.println("..." + uniqs.size() + " unique MSFProteins were found...");
        return uniqs;
    }

    private boolean inUniqs(MSFPSMProtein prot, ArrayList<MSFPSMProtein> uniqs) {
        //throw new UnsupportedOperationException("Not yet implemented");
        boolean in = false;
        Iterator<MSFPSMProtein> itr = uniqs.iterator();
        while(itr.hasNext()){
            MSFPSMProtein uprot = itr.next();
            if(uprot.getSequence().equalsIgnoreCase(prot.getSequence())){
                in = true;
                break;
            }
        }  
        return in;
    }

    private ArrayList<MSFPSMProtein> setUniqueMappedMSFProteins(MSFPSMProtein[] msfProts, 
                                                                MSFPSMProtein[] decoyMSFProts) {
        
        System.out.println("\tOf " + msfProts.length + " actual and " + decoyMSFProts.length + " decoy MSFProteins..");
        //Iterator<MSFPSMProtein> itr = null;
        
        ArrayList<MSFPSMProtein> uniqs = new ArrayList<MSFPSMProtein>();
        //itr = msfProts.iterator();
        //while(itr.hasNext()){
        for( MSFPSMProtein prot : msfProts){
            //MSFPSMProtein prot = itr.next();
            if(inUniqs(prot,uniqs)==false){
                uniqs.add(prot);
            }
        } 
        
        for( MSFPSMProtein prot : decoyMSFProts){
             //MSFPSMProtein prot = itr.next();
            if(inUniqs(prot,uniqs)==false){
                uniqs.add(prot);
            }
        }  
        System.out.println(" ..." + uniqs.size() + " unique MSFProtein(s) were found...");
        return uniqs;
    }
    
     public HashMap<Integer, LinkedList<Peptide>> getPepID2PeptideInstancesMap() {
        return pepID2PeptideInstancesMap;
    }

    public ArrayList<MSFPSMProtein> getUniqueMappedMSFProteins() {
        return uniqMappedMSFProteins;
    }

    
    
}
