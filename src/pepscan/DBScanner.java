/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
        Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package pepscan;

import database.Database;
import database.Peptide;
import database.Protein;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class DBScanner {    
    
    HashMap<String,LinkedList<Peptide>> pepSeq2PeptideInstancesMap;
    ArrayList<Protein> uniqueMappedProteins; 

    public DBScanner(ArrayList<String> peps2ScanFor, Database db) {
        pepSeq2PeptideInstancesMap = new HashMap<String,LinkedList<Peptide>>();
        uniqueMappedProteins = new ArrayList<Protein>();
        Iterator<String> itr = peps2ScanFor.iterator();
        while(itr.hasNext()){
            String pep2ScanFor = itr.next();
            scan(pep2ScanFor,db);
        }       
    }
 
    private void scan(String pep2ScanFor, Database db) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String peptideSequence = pep2ScanFor;
        LinkedList<Peptide> peptideInstances = new LinkedList<Peptide>();
        
        ArrayList<Protein> proteins = db.getProteins();
        Iterator<Protein> itr = proteins.iterator();
        
        while(itr.hasNext()){
            Protein protein = itr.next();
            if(protein.getSequence().contains(peptideSequence)){
                Peptide peptide = protein.getPeptideInstance(peptideSequence);
                peptideInstances.add(peptide);
                if(!uniqueMappedProteins.contains(protein)){
                    uniqueMappedProteins.add(protein);
                }
            }
        }
        pepSeq2PeptideInstancesMap.put(pep2ScanFor, peptideInstances);
    }

    public HashMap<String, LinkedList<Peptide>> getPepSeq2PeptideInstancesMap() {
        return pepSeq2PeptideInstancesMap;
    }

    public ArrayList<Protein> getUniqueMappedProteins() {
        return uniqueMappedProteins;
    }
    
    
    

}
