/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfpsms;

import ios.readers.MSFRoutPSMPeptidesFileReader;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfparser.M2Lite;
import msfparser.MSFModificationsExtractor;
import msfparser.MSFReporterIonsQuanExtractor;
import rconnect.RCaller;

/**
 *
 * @author paiyeta1
 * 
 * Introduced paradigm:
 *  
    - make output file
    - make Query string
    - make command (rcmd)
    - execute R
        rcaller.execute(rcmd);
    - read back
        
 * 
 * 
 */
public class MSFPSMPeptidesExtractor {
    
    private MSFPSMPeptide[] msfPeps;
    private HashMap<String,Integer> scoreMap;
    // Map peptide to corresponding scores 
    private HashMap<Integer,LinkedList<MSFPSMPeptideScore>> pscoreMap;
            
    @SuppressWarnings("CallToThreadDumpStack")
    public MSFPSMPeptidesExtractor(String msfFile, boolean includeiTRAQ4plexQuan, String rQueryScriptLibrary) {
        
        //msfPeps = new ArrayList<MSFPSMPeptide>();
        scoreMap = new HashMap<String,Integer>(); //score map, a map of ScoreName to corresponding ScoreID
        pscoreMap = new HashMap<Integer,LinkedList<MSFPSMPeptideScore>>();//peptide Score Map, a map of peptideID's to 
    
        //create a tmp in present working directory
        String homeDir = System.getProperty("user.home");
        String tmp = homeDir + File.separator + "tmp";
        
        if(!new File(tmp).exists()){
            new File(tmp).mkdirs();
        }
        
        //String rFilePath = "C:/Users/paiyeta1/Libraries/R/local/msfParser_RLibs/dBQuery.R";
        String rFilePath = rQueryScriptLibrary + File.separator + "MSFPSMPeptidesDBQuery.R";
        RCaller rcaller = new RCaller(); 
    //1. Get msf Peptides
        System.out.println("\tQuerying file for PSM assigned peptides' properties...");    
        //make output file
        String msfRoutPeptidesOutputFile = tmp + File.separator + "msfRoutPeptides";
        //make command; note that in this implementation MSF file DB query string is delegated to R-Helper scripts
        String rcmd = "Rscript " + rFilePath + " " + msfFile + " " + msfRoutPeptidesOutputFile;
        //execute R
        rcaller.execute(rcmd);
        //read back
        MSFRoutPSMPeptidesFileReader reader = new MSFRoutPSMPeptidesFileReader();
        msfPeps = reader.read(msfRoutPeptidesOutputFile);
        
        //new File(msfRoutPeptidesOutputFile).delete();
        
        
        // *************************************************************************** //
        // retrieve modification attributes/properties
        MSFModificationsExtractor modExtr = new MSFModificationsExtractor(msfFile,tmp,rQueryScriptLibrary);
        //A. retrieve Non-Terminal modifications attributes/properties
        HashMap<Integer,LinkedList<MSFPSMModification>> peptideID2ModificationsMap = 
                modExtr.getPeptideID2ModificationsMap();
        //B. retrieve terminal modifications attributes/properties
        HashMap<Integer,LinkedList<MSFPSMModification>> peptideID2TermModificationMap = 
                modExtr.getPeptideID2TermModificationMap();

        // ********************************************************* //
        // Get Scores - XCorr and SpScore +/- Probability
        
        Connection connection = null;  
        Statement statement = null; 
        ResultSet resultSet = null;
        try {  
            Class.forName("org.sqlite.JDBC"); //Deprecated in recent JDBC implementation
            System.out.println("\tEstablishing SQLite DB connection to file...");    
            connection = DriverManager.getConnection("jdbc:sqlite:" + msfFile);  
            statement = connection.createStatement();
            System.out.println("\tQuerying file for Scores identifiers...");    
            String query = "SELECT " +
                            "ScoreID," +
                            "ScoreName " + 
                            "FROM ProcessingNodeScores;";
            resultSet = statement.executeQuery(query);
            // Make map of ScoreID to corresponding ScoreName
            System.out.println("\tRetrieving Scores identifiers' results..."); 
            int noOfResultsRetrieved = 0;
            while(resultSet.next()){
                noOfResultsRetrieved++;
                int ScoreID = resultSet.getInt("ScoreID");
                String ScoreName = resultSet.getString("ScoreName");
                scoreMap.put(ScoreName,ScoreID);                   
            }
            System.out.println("\t\t" + noOfResultsRetrieved + " Scores identifiers' results retrieved...");
            statement.close();
            resultSet.close(); 

            // 5b. Get peptides with correspoding scores
            statement = connection.createStatement();
            System.out.println("\tQuerying file for Peptide Scores...");    
            query = "SELECT " +
                            "PeptideID," +
                            "ScoreID, " + 
                            "ScoreValue " +
                            "FROM PeptideScores;";
            resultSet = statement.executeQuery(query);

            System.out.println("\tRetrieving Peptide Scores' results..."); 
            noOfResultsRetrieved = 0;
            int noOfPeptide2ScoresMapped = 0;
            while(resultSet.next()){
                noOfResultsRetrieved++;
                int PeptideID = resultSet.getInt("PeptideID");
                int ScoreID = resultSet.getInt("ScoreID");
                double ScoreValue = resultSet.getDouble("ScoreValue");
                if(pscoreMap.containsKey(PeptideID)==false){
                    LinkedList<MSFPSMPeptideScore> scoreList = new LinkedList<MSFPSMPeptideScore>();
                    scoreList.add(new MSFPSMPeptideScore(ScoreID,ScoreValue));        
                    pscoreMap.put(PeptideID,scoreList);
                    noOfPeptide2ScoresMapped++;
                    
                }else{
                    LinkedList<MSFPSMPeptideScore> scoreList = pscoreMap.remove(PeptideID);
                    scoreList.add(new MSFPSMPeptideScore(ScoreID,ScoreValue));
                    pscoreMap.put(PeptideID,scoreList);
                    //noOfPeptide2ScoresMapped++;                    
                }
            }
            System.out.println("\t\t" + noOfPeptide2ScoresMapped + " PeptideID to Scores' mapped...");
            System.out.println("\t\t" + noOfResultsRetrieved + " Peptide Scores' results retrieved...");
            statement.close();
            resultSet.close(); 
            



            // ****************************************************************************
            //6. Set missing Attributes of MSFPSMPeptides
            // Update MSFPeptides's properties in ArrayList
            //Iterator<MSFPSMPeptide> itr = msfPeps.iterator();
            System.out.println("\tUpdating peptides results' properties...");
            
            ArrayList<MSFReporterIonQuanResult> reporterIonResults = null;
            if(includeiTRAQ4plexQuan){
                MSFReporterIonsQuanExtractor qExtr = new MSFReporterIonsQuanExtractor(msfFile,tmp,rQueryScriptLibrary);
                reporterIonResults = qExtr.getReporterIonResults();  
                
                for(int i = 0; i < msfPeps.length; i ++){
                //while (itr.hasNext()){
                    
                    //MSFPSMPeptide msfPep = (MSFPSMPeptide) itr.next();
                    MSFPSMPeptide msfPep = msfPeps[i];
                    int msfPepID = msfPep.getPeptideID();
                    // update Modification Attributes
                    msfPep.setModifications(extractPepModfxns(msfPepID,peptideID2ModificationsMap,peptideID2TermModificationMap));
                    // set Scores
                    if(scoreMap.containsKey("SpScore")){
                        msfPep.setScore("SpScore",scoreMap,pscoreMap);
                        //msfPep.scoreNameToValueMap("spscore", );
                    }
                    if(scoreMap.containsKey("XCorr")){
                        msfPep.setScore("XCorr",scoreMap,pscoreMap);
                        //msfPep.scoreNameToValueMap("xcorr",);
                    }
                    if(scoreMap.containsKey("ProbabilityScore")){
                        msfPep.setScore("ProbabilityScore",scoreMap,pscoreMap);
                        //msfPep.scoreNameToValueMap("probabilityscore",);
                    }
                    if(scoreMap.containsKey("IonScore")){
                        msfPep.setScore("IonScore",scoreMap,pscoreMap);
                        //msfPep.scoreNameToValueMap("ionscore",);
                    }



                    // update Quan[tification] Attribute(s)
                    double channel114 = extractChannelQuan(msfPep,reporterIonResults,1);
                    double channel115 = extractChannelQuan(msfPep,reporterIonResults,2);
                    double channel116 = extractChannelQuan(msfPep,reporterIonResults,3);
                    double channel117 = extractChannelQuan(msfPep,reporterIonResults,4);

                    msfPep.setChannel114(channel114);
                    msfPep.setChannel115(channel115);
                    msfPep.setChannel116(channel116);
                    msfPep.setChannel117(channel117);

                }                                        
            } else { 
                
                for(int i = 0; i < msfPeps.length; i ++){
                //while (itr.hasNext()){

                    MSFPSMPeptide msfPep = msfPeps[i];;
                    int msfPepID = msfPep.getPeptideID();
                    // update Modification Attributes
                    msfPep.setModifications(extractPepModfxns(msfPepID,peptideID2ModificationsMap,peptideID2TermModificationMap));
                    // set Scores
                    if(scoreMap.containsKey("SpScore"))
                        msfPep.setScore("SpScore",scoreMap,pscoreMap);
                    if(scoreMap.containsKey("XCorr"))
                        msfPep.setScore("XCorr",scoreMap,pscoreMap);
                    if(scoreMap.containsKey("ProbabilityScore"))
                        msfPep.setScore("ProbabilityScore",scoreMap,pscoreMap);
                    if(scoreMap.containsKey("IonScore"))
                        msfPep.setScore("IonScore",scoreMap,pscoreMap);
                }
            }
        } catch (Exception e) { 
            e.printStackTrace();
        }  
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(M2Lite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //new File(tmp).deleteOnExit(); //delete tmp directory;

    } 
    
    private double extractChannelQuan(MSFPSMPeptide msfPep, ArrayList<MSFReporterIonQuanResult> reporterIonResults,
            int channel) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double channelQuan = 0.00;
        Iterator<MSFReporterIonQuanResult> itr = reporterIonResults.iterator();
        while(itr.hasNext()){
            MSFReporterIonQuanResult qResult = itr.next();
            if((msfPep.getSpectrumID() == qResult.getSearchSpectrumID()) && (qResult.getQuanChannelID() == channel)){
                channelQuan = qResult.getHeight();
                //System.out.println("\t\t\t" + qResult.getHeight());
                break;
            }
        }      
        return channelQuan;
    }

    
    private LinkedList<MSFPSMModification> extractPepModfxns(int msfPepID, 
                                HashMap<Integer, LinkedList<MSFPSMModification>> peptideID2ModificationsMap, 
                                    HashMap<Integer, LinkedList<MSFPSMModification>> peptideID2TermModificationMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        LinkedList<MSFPSMModification> modifications = new LinkedList<MSFPSMModification>();
        if (peptideID2TermModificationMap.containsKey(msfPepID)){
            LinkedList<MSFPSMModification> mappedTermModfxn = peptideID2TermModificationMap.get(msfPepID);
            Iterator<MSFPSMModification> itr = mappedTermModfxn.iterator();
            while(itr.hasNext()){
                MSFPSMModification termModfxn = itr.next();
                modifications.add(termModfxn);
            }
        }
        
        if (peptideID2ModificationsMap.containsKey(msfPepID)){
            LinkedList<MSFPSMModification> mappedModfxn = peptideID2ModificationsMap.get(msfPepID);
            Iterator<MSFPSMModification> itr2 = mappedModfxn.iterator();
            while(itr2.hasNext()){
                MSFPSMModification modfxn = itr2.next();
                modifications.add(modfxn);
            }
        }
        return modifications;
    }

    public MSFPSMPeptide[] getMsfPeps() {
        return msfPeps;
    }
    
    
    
}

