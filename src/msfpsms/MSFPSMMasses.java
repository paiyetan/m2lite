/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfpsms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
//import qcClasses.Peak;

/**
 *
 * @author paiyeta1
 */
public class MSFPSMMasses {
    
    //private Peak peak;
    private double observedMass;
    private double theoreticalMass;
    private double observedMassToCharge;
    private double theoreticalMassToCharge;
    private HashMap<Character,Double> massTable;
    
    
    public MSFPSMMasses(MSFPSMPeptide msfPep){
        //peak = getMSFPSMPeptidePeak(msfPep);
        setMassTable();
        observedMass = msfPep.getMass();
        theoreticalMass = getMSFPSMPeptideTheoreticalMass(msfPep);
        setObservedMassToCharge(msfPep);
        setTheoreticalMassToCharge(msfPep);
        
    }
    public MSFPSMMasses(){
        setMassTable();
    }
    //mz = (M + zA)/z 
    // where M = mass, z = charges, A = mass of adduct providing charge; 
    // ref: http://www.enovatia.com/downloads/presentations/ProMass_training_slides.pdf

    private double getMSFPSMPeptideTheoreticalMass(MSFPSMPeptide msfPep) {
        //throw new UnsupportedOperationException("Not yet implemented");
        String sequence = msfPep.getSequence();
        double sequenceMass = calculateMass(sequence);
        //add modifications masses
        LinkedList modfxns = msfPep.getModifications();
        double summedMdfxnsMass = getModificationsMass(modfxns);
        double theoreticalM = sequenceMass + summedMdfxnsMass;       
        return(theoreticalM);              
    }
    
    /*
     * The following code is all for calculating peptide masses.
     */
    private double calculateMass(String sequence){
        String str = sequence.toUpperCase();
       double mass = 0.0;
       for(int i = 0; i < str.length(); i++ ){
           char residue = str.charAt(i);
           if(massTable.get(residue)!= null){
               mass = mass + massTable.get(residue);
           }
       }
       mass = mass + 18.01056;//1.007825 + 15.994910 + 1.007825; 
       return mass;
    }

    private double getModificationsMass(LinkedList modfxns) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double mass = 0.0;
        Iterator<MSFPSMModification> itr = modfxns.iterator();
        while(itr.hasNext()){
            MSFPSMModification mod = itr.next();
            mass = mass + mod.getDeltaMass();
        }
        return mass;
    }

    //massdiff
    public double getDeltaMassDa() {
        // Mass(precursor ion) - Mass(peptide) http://sashimi.sourceforge.net/schema_revision/pepXML/Docs/pepXML_v18.html#element_search_hit_Link02E06C60
        return(theoreticalMass - observedMass);
    }

    public double getDeltaMassPPM() {
        double deltaMassPPM = (getDeltaMassDa()/theoreticalMass )* 1000000;       
        return deltaMassPPM;
    }

    public double getObservedMass() {
        return observedMass;
    }
    
    private HashMap<Character,Double> setMassTable(){
        massTable = new HashMap<Character,Double>();
        double[] aAMasses = {
                                71.0371137878,	// A;
                                103.0091844778,	// C;
                                115.026943032,	// D;
                                129.0425930962,	// E;
                                147.0684139162,	// F;
                                57.0214637236,	// G;
                                137.0589118624,	// H;
                                113.0840639804,	// I;
                                128.0949630177,	// K;
                                113.0840639804,	// L;
                                131.0404846062,	// M;
                                114.0429274472,	// N;
                                97.052763852,	// P;
                                128.0585775114,	// Q;
                                156.1011110281,	// R;
                                87.0320284099,	// S;
                                101.0476784099,	// T;
                                150.9536355878, // U; Selenocysteine
                                99.0684139162,	// V;
                                186.0793129535,	// W;
                                163.06333285383 // Y
                            };
        char[] aASymbols = {'A', 
                            'C',
                            'D',
                            'E', 
                            'F',
                            'G',
                            'H',
                            'I',
                            'K',
                            'L',
                            'M',
                            'N',
                            'P', 
                            'Q',
                            'R',
                            'S',
                            'T',
                            'U',
                            'V',
                            'W',
                            'Y'
                            };
        
        for(int i = 0; i < aASymbols.length; i++){
            massTable.put(aASymbols[i], aAMasses[i]);
        }
        return massTable;
    }

    /*
    public Peak getPeak() {
        return peak;
    }
    * 
    */

    public double getTheoreticalMass() {
        return theoreticalMass;
    }

    private void setObservedMassToCharge(MSFPSMPeptide msfPep) {
        //mz = (M + zA)/z 
        // where M = mass, z = charges, A = mass of adduct providing charge; 
        // ref: http://www.enovatia.com/downloads/presentations/ProMass_training_slides.pdf
        
        // proton mass = 1.67262178 × 10^-27
        // One amu (or dalton) is approximately 1.7 X 10^-24g.
        // Hydrogen mass = 1.007825 
        /*
        double mass = msfPep.getMass();
        double z =  (double) msfPep.getCharge();
        observedMassToCharge = (mass + (z * 1.007825))/z;
        * 
        */
        observedMassToCharge = msfPep.getMZ();
    }

    private void setTheoreticalMassToCharge(MSFPSMPeptide msfPep) {
        //throw new UnsupportedOperationException("Not yet implemented");
        double mass = getMSFPSMPeptideTheoreticalMass(msfPep);
        double z =  (double) msfPep.getCharge();
        theoreticalMassToCharge = (mass + (z * 1.007825))/z;
    }

    public double getObservedMassToCharge() {
        return observedMassToCharge;
    }

    public double getTheoreticalMassToCharge() {
        return theoreticalMassToCharge;
    }

    public HashMap<Character, Double> getMassTable() {
        return massTable;
    }  
    
}
