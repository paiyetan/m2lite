/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfpsms;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class MSFPSMPeptideXCorrRankedQueue {

    private MSFPSMPeptide[] priorityQueue;
    private int size;
    
    public MSFPSMPeptideXCorrRankedQueue(LinkedList<MSFPSMPeptide> mappedPepList) {
        priorityQueue = new MSFPSMPeptide[mappedPepList.size()];
        for(int i = 0; i < priorityQueue.length; i++){
            priorityQueue[i] = mappedPepList.get(i);
        }
        prioritize(true);
        setDeltaCNs();
        setSpRanks();
        setDeltaCNStars();
        size = priorityQueue.length; // starting size of queue 
        //empty = false;
    }
    
    private void prioritize(boolean verbose) {
        // throw new UnsupportedOperationException("Not yet implemented");
        /*
        for(int i = 0; i < priorityQueue.length - 1; i++){
            if(priorityQueue[i+1].getSearchEngineRank() < priorityQueue[i].getSearchEngineRank()){
                MSFPSMPeptide tmp = priorityQueue[i];
                priorityQueue[i] = priorityQueue[i+1];
                priorityQueue[i+1] = tmp;
                for(int j = i; j > 0; j--){
                    if(priorityQueue[j].getSearchEngineRank() < priorityQueue[j-1].getSearchEngineRank()){
                        MSFPSMPeptide tmp2 = priorityQueue[j-1];
                        priorityQueue[j-1] = priorityQueue[j];
                        priorityQueue[j] = tmp2;
                    }
                }
            }
            
        }
        * 
        */
        // in this case prioritize such that MSFPeptides with higher XCorr values come to the front 
        for(int i = (priorityQueue.length - 1); i > 0; i--){
            if(priorityQueue[i].getXCorr() > priorityQueue[i-1].getXCorr()){
                MSFPSMPeptide tmp = priorityQueue[i];
                priorityQueue[i] = priorityQueue[i-1];
                priorityQueue[i-1] = tmp;
                for(int j = i; j < (priorityQueue.length - 1); j++){
                    if(priorityQueue[j].getXCorr() < priorityQueue[j+1].getXCorr()){
                        MSFPSMPeptide tmp2 = priorityQueue[j+1];
                        priorityQueue[j+1] = priorityQueue[j];
                        priorityQueue[j] = tmp2;
                    }
                }
            }
            
        }
        if(verbose){
            // *** Debugging code
            System.out.println("   MSFPSMPeptide(s) prioritized by xcorr: ");
            for(int i = 0; i < priorityQueue.length; i++){
                System.out.println("    " + priorityQueue[i].getSequence() + ", " + 
                                           (priorityQueue[i].isDecoy()==false) + " hit, " +
                                            priorityQueue[i].getXCorr());
            }
        }
        
    }
    
    
    public void add(MSFPSMPeptide qcFObj){
        //Inserts the specified element into this priority queue
        MSFPSMPeptide[] updatedPriorityQueue = new MSFPSMPeptide[priorityQueue.length + 1];
        System.arraycopy(priorityQueue, 0, updatedPriorityQueue, 0, priorityQueue.length);
        updatedPriorityQueue[updatedPriorityQueue.length] = qcFObj;
        priorityQueue = updatedPriorityQueue;
        prioritize(false);
        size++;
    }
    
    public MSFPSMPeptide peek(){
        // Retrieves, but does not remove, the head of this queue, or returns null if this queue is empty.
        MSFPSMPeptide qcFObj = priorityQueue[0];
        return qcFObj;
    }
    
    /*
    public MSFPSMPeptide poll(){
        // Retrieves and removes the head of this queue, or returns null if this queue is empty.
        MSFPSMPeptide qcFObj = priorityQueue[0];
        // update queue
        MSFPSMPeptide[] updatedPriorityQueue = new MSFPSMPeptide[priorityQueue.length - 1];
        System.arraycopy(priorityQueue, 1, updatedPriorityQueue, 0, updatedPriorityQueue.length);
        priorityQueue = updatedPriorityQueue;
        size--;
        return qcFObj;
        
    }
    * 
    */
    
    /*
     * 
     * 
    public void remove(){
        // Removes a single instance of the specified element from this queue, if it is present.
        
    }
    
    public int size(){
        // Returns the number of elements in this collection.
        return size;
    }
    
    public boolean contains(){
        // Returns true if this queue contains the specified element.
        boolean contain = false;
        
        return contain;
    }
    
    public boolean offer(QuanCountFileObject qcFObj){
        // Inserts the specified element into this priority queue.
        
        return true;
    }
    * 
    */
    
    public boolean isEmpty(){
        boolean empty;
        if(size > 0){
            empty = false;
        } else{
            empty = true;
        }
        return empty;
    }

    public MSFPSMPeptide[] getPriorityQueue() {
        return priorityQueue;
    }

    private void setDeltaCNs() {
        //throw new UnsupportedOperationException("Not yet implemented");
        // DeltaCn (a relative goodness of fit value) is calculated by the formulae
                // (XCorr_1 - XCorr_2)/XCorr1. Its value is often derived after PSMs matching to a 
                // spectrum are sorted and ordered according to their respective XCorr values...
        System.out.println("   Setting XCorr-ranked spectrum mapped MSFPSMPeptide(s) deltaxcorr: ");
        for(int i=0; i < (priorityQueue.length-1); i++){ // iterate to the last but one, because the last MSFPSMPeptide 
                                                         // get the [default] deltacn of 1.000...
            double deltacn = 0;
            double xcorr1 = priorityQueue[i].getXCorr();
            double xcorr2 = priorityQueue[i+1].getXCorr();
            double xcorrdiff = (xcorr1 - xcorr2);
            if(xcorrdiff > 0){ // 
                deltacn = xcorrdiff/xcorr1 ;
            }else{
                deltacn = 0; // quite rarely should this occur - there is no relative difference PSM and proceeding spectrum... 
            }
            priorityQueue[i].setDeltaCn(deltacn);
        }
    }
    
    private void setDeltaCNStars() {
        //throw new UnsupportedOperationException("Not yet implemented");
        // DeltaCn (a relative goodness of fit value) is calculated by the formulae
                // (XCorr_1 - XCorr_2)/XCorr1. While XCorr2 is not a homologous peptide sequence.
                // Its value is often derived after PSMs matching to a 
                // spectrum are sorted and ordered according to their respective XCorr values...
        System.out.println("   Setting XCorr-ranked spectrum mapped MSFPSMPeptide(s) deltaxcorrstar: ");
        for(int i=0; i < (priorityQueue.length-1); i++){ // iterate to the last but one, because the last MSFPSMPeptide 
                                                         // get the [default] deltacn of 0.000, and has no other to compare
                                                         // its sequence with...
            double deltacnstar = 0.000;
            double xcorr1 = priorityQueue[i].getXCorr();
            double xcorrdiff = 0.000;
            //double xcorr2 = 0.000;
            String peptideSequence = priorityQueue[i].getSequence();
            // get next non-homologous peptide sequence...
            String nextNHPepSequence = null;
            for(int j = i+1; j <= (priorityQueue.length-1); j++){
                nextNHPepSequence =  priorityQueue[j].getSequence();
                if(nextNHPepSequence.equalsIgnoreCase(peptideSequence)==false){
                    double xcorr2 = priorityQueue[j].getXCorr();
                    xcorrdiff = (xcorr1 - xcorr2);
                    break;
                }
            }
            
            if(xcorrdiff > 0){ // quite rarely should this occur 
                deltacnstar = xcorrdiff/xcorr1 ;
            }else{
                deltacnstar = 0.000; // there is no relative difference PSM and proceeding spectrum... 
            }
            priorityQueue[i].setDeltaCnStar(deltacnstar);
        }
    }
    
    private void setSpRanks(){
        // in this case spRanks are dependent on the search engine computed, estimated spcores
        System.out.println("   Setting SpScore-ranked spectrum mapped MSFPSMPeptide(s) sprank: ");
        LinkedList<Double> spscores = new LinkedList<Double>();
        // get a list of all unique spscores....
        for(int i = 0; i < priorityQueue.length; i++){
            double spscore = priorityQueue[i].getSpScore();
            if(spscores.contains(spscore)==false){
                spscores.add(spscore);
            }
        }
        // sort the list...
        Collections.sort(spscores);//sorts spscores in ascending order...
        // map scores to associated ranks...
        HashMap<Double,Integer> scoresToRank = new HashMap<Double,Integer>();
        int rank = 1;
        for(int i = spscores.size()-1; i >= 0; i--){
            scoresToRank.put(spscores.get(i), rank);
            rank++;
        }
        // set ranks...
        for(int i = 0; i < priorityQueue.length; i++){
            double spscore = priorityQueue[i].getSpScore();
            // score's associated rank...
            int associatedRank = scoresToRank.get(spscore);
            priorityQueue[i].setSpRank(associatedRank);
        }
    }
    
    
}
