/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfpsms;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class MSFPSMPeptide{
        // General attributes
    private int ProcessingNodeNumber; 
    private int PeptideID;
    private int SpectrumID;
    private int TotalIonsCount;
    private int MatchedIonsCount;
    private int ConfidenceLevel;
    private int SearchEngineRank;
    private String Sequence;
    private String Annotation;
    private int UniquePeptideSequenceID;
    private int MissedCleavages;

    private double RetentionTime;
    private int FirstScan;
    private int LastScan;
    
    private int Charge;
    private double Intensity;
    private double Mass;
    
    private double MZ;
    
    private float PercentIsolationInterference;
    private int IonInjectTime;

    private int MSLevel;
    private int ActivationType;

    private String FileName;
    
    
        
    // Quan[tification] Attribute(s)
    private double Channel114;
    private double Channel115;
    private double Channel116;
    private double Channel117;           
    
    // Modification Attributes
    private LinkedList<MSFPSMModification> Modifications;
    
    // Score Attributes
    private double Score = 0; //Spectrum Scores
    private double SpScore = 0;
    private double XCorr = 0;
    
    // other scores
    private double IonScore = 0;
    private double ProbabilityScore = 0;
    
    private double DeltaCn = 0;
                // DeltaCn (a relative goodness of fit value) is calculated by the formulae
                // (XCorr_1 - XCorr_2)/XCorr1. Its value is often derived after PSMs matching to a 
                // spectrum are sorted and ordered according to their respective XCorr values...
                
                // defaults to zero - in which case it remains so if this.MSFPSMPeptide happen to be the 
                // only hit for a spectrum wherein you cannot compute a deltaCN; "...it most likely means that the spectrum was really bad. 
                // So I don’t not think you will lose any identifications..." stated Dr. Alexy Nesvizhskii.
    
    private double expectValue = 0; // defaults to 0.000 in the current implementation...
    private double DeltaCnStar = 0; //defaults to 0.000 in the current implementation...
    private int SpRank = 1; // a SpRank in appears to be dependent on the SpScores reported for all PSMs matching to a 
                            // spectrum. 
    /*
     * NOTE:
     * DeltaCn, [possible DeltaCnStar as well], and SpRank are set after MSFPSMPeptide objects are mapped to spectr(a)um
     */
    
    
    //a decoy/actual database spectrum match
    private boolean decoy = false;
    
    
    HashMap <String, Double> scoreNameToValueMap; 

    
   

    public MSFPSMPeptide(int ProcessingNodeNumber, int PeptideID, int SpectrumID, 
            int TotalIonsCount, int MatchedIonsCount, int ConfidenceLevel, int SearchEngineRank, 
            String Sequence, String Annotation, int UniquePeptideSequenceID, int MissedCleavages, 
            double RetentionTime, int FirstScan, int LastScan, double Score, int Charge, double Intensity, 
            double Mass, float PercentIsolationInterference, int IonInjectTime, 
            int MSLevel, int ActivationType, String FileName) {
        
        this.ProcessingNodeNumber = ProcessingNodeNumber;
        this.PeptideID = PeptideID;
        this.SpectrumID = SpectrumID;
        this.TotalIonsCount = TotalIonsCount;
        this.MatchedIonsCount = MatchedIonsCount;
        this.ConfidenceLevel = ConfidenceLevel;
        this.SearchEngineRank = SearchEngineRank;
        this.Sequence = Sequence;
        this.Annotation = Annotation;
        this.UniquePeptideSequenceID = UniquePeptideSequenceID;
        this.MissedCleavages = MissedCleavages;
        this.RetentionTime = RetentionTime;
        this.FirstScan = FirstScan;
        this.LastScan = LastScan;
        this.Score = Score;
        this.Charge = Charge;
        this.Intensity = Intensity;
        this.Mass = Mass;
        this.PercentIsolationInterference = PercentIsolationInterference;
        this.IonInjectTime = IonInjectTime;
        this.MSLevel = MSLevel;
        this.ActivationType = ActivationType;
        this.FileName = FileName;  
        scoreNameToValueMap = new HashMap <String, Double>(); 
        
    }
     public MSFPSMPeptide(int ProcessingNodeNumber, int PeptideID, int SpectrumID, 
            int TotalIonsCount, int MatchedIonsCount, int ConfidenceLevel, int SearchEngineRank, 
            String Sequence, String Annotation, int UniquePeptideSequenceID, int MissedCleavages, 
            double RetentionTime, int FirstScan, int LastScan, int Charge, double Intensity, 
            double Mass, double mz, float PercentIsolationInterference, int IonInjectTime, 
            int MSLevel, int ActivationType, String FileName) {
         
         this(ProcessingNodeNumber, PeptideID, SpectrumID, 
              TotalIonsCount,  MatchedIonsCount, ConfidenceLevel,  SearchEngineRank, 
              Sequence,  Annotation, UniquePeptideSequenceID, MissedCleavages, 
              RetentionTime, FirstScan, LastScan, 0.0, Charge, Intensity, 
              Mass,  PercentIsolationInterference, IonInjectTime, 
              MSLevel, ActivationType, FileName);
         this.MZ = mz;
     }
   

    public int getActivationType() {
        return ActivationType;
    }

    public String getAnnotation() {
        return Annotation;
    }

    public int getCharge() {
        return Charge;
    }

    public int getConfidenceLevel() {
        return ConfidenceLevel;
    }

    public String getFileName() {
        return FileName;
    }

    public int getFirstScan() {
        return FirstScan;
    }

    public double getIntensity() {
        return Intensity;
    }

    public int getIonInjectTime() {
        return IonInjectTime;
    }

    public int getLastScan() {
        return LastScan;
    }

    public int getMSLevel() {
        return MSLevel;
    }

    public double getMass() {
        return Mass;
    }

    public double getMZ() {
        return MZ;
    }
    
    public int getMatchedIonsCount() {
        return MatchedIonsCount;
    }

    public int getMissedCleavages() {
        return MissedCleavages;
    }

    public int getPeptideID() {
        return PeptideID;
    }

    public float getPercentIsolationInterference() {
        return PercentIsolationInterference;
    }

    public int getProcessingNodeNumber() {
        return ProcessingNodeNumber;
    }

    public double getRetentionTime() {
        return RetentionTime;
    }

    public double getScore() {
        return Score;
    }

    public int getSearchEngineRank() {
        return SearchEngineRank;
    }

    public String getSequence() {
        return Sequence;
    }

    public int getSpectrumID() {
        return SpectrumID;
    }

    public int getTotalIonsCount() {
        return TotalIonsCount;
    }

    public int getUniquePeptideSequenceID() {
        return UniquePeptideSequenceID;
    }

    public boolean isDecoy() {
        return decoy;
    }
    
    // ************** public setters *********************** //
    
    public void setDecoy(boolean decoy){
        this.decoy = decoy; 
    }
    
    public void setChannel114(double Channel114) {
        this.Channel114 = Channel114;
    }

    public void setChannel115(double Channel115) {
        this.Channel115 = Channel115;
    }

    public void setChannel116(double Channel116) {
        this.Channel116 = Channel116;
    }

    public void setChannel117(double Channel117) {
        this.Channel117 = Channel117;
    }

    public void setModifications(LinkedList<MSFPSMModification> Modifications) {
        this.Modifications = Modifications;
    }
    
    // ******** updated getters *********** //

    public double getChannel114() {
        return Channel114;
    }

    public double getChannel115() {
        return Channel115;
    }

    public double getChannel116() {
        return Channel116;
    }

    public double getChannel117() {
        return Channel117;
    }

    public LinkedList<MSFPSMModification> getModifications() {
        return Modifications;
    }

    public double getSpScore() {
        return SpScore;
    }

    public double getXCorr() {
        return XCorr;
    }
    
    public double getIonScore(){
        return IonScore;
    }
    
    public double getProbabilityScore(){
        return ProbabilityScore;
    }
    
    public double getDeltaCn(){
        return DeltaCn;
    }

    public double getDeltaCnStar() {
        return DeltaCnStar;
    }

    public int getSpRank() {
        return SpRank;
    }

    public double getExpectValue() {
        return expectValue;
    }
    
    /*
     * An alternate way to retrieve values included in MSF files...
     * Scores and score values returned may include those of 
     * XCorr, SpScore, ProbabilityScore, and/or IonScore
     * 
     * @return scoreNameToValueMap
     */
    public HashMap<String, Double> getScoreNameToValueMap() {
        return scoreNameToValueMap;
    }
    
    
    
    // *********** toStrings ************** //
    public String toStringPepModifications(){
        // format == Residue-Position-(-ModificationName-);
        String modifications = "";
        Iterator<MSFPSMModification> itr = Modifications.iterator();
        int numberOfModifications = Modifications.size();
        while(itr.hasNext()){            
            MSFPSMModification modfxn = itr.next();
            String modfxnName = modfxn.getModificationName();
            int position = modfxn.getPosition();
            if (modfxn.isTerminal()){
                modifications = modifications + "N-Term(" + modfxnName + ")";
                numberOfModifications--;
                if(numberOfModifications > 0){
                    modifications = modifications + ";";
                }
            } else {
                modifications = modifications + this.Sequence.charAt(position) + 
                                        (position + 1) + "(" + modfxnName + ")"; //thermo indexes position on sequence from 0
                numberOfModifications--;
                if(numberOfModifications > 0){
                    modifications = modifications + ";";
                }
            }
        }
        return modifications;
    }
    
   
    /*
     * sets the PSM SpScore, XCorr, ProbabilityScore and IonScore
     * @return null
     */
    public void setScore(String scoreType, HashMap<String, Integer> scoreMap, 
                                HashMap<Integer, LinkedList<MSFPSMPeptideScore>> pscoreMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        try{
            int scoreID = scoreMap.get(scoreType);
            LinkedList<MSFPSMPeptideScore> pscores = pscoreMap.get(PeptideID); 
            if(pscores != null){ // it implies that the map contains mapped scores to the corresponding peptideID
                Iterator<MSFPSMPeptideScore> itr  = pscores.iterator();
            
                while(itr.hasNext()){
                    MSFPSMPeptideScore pscore = itr.next();
                    if(pscore.getScoreID()==scoreID){
                        double scoreValue = pscore.getScoreValue();
                        if(scoreType.equalsIgnoreCase("SpScore")){
                            SpScore = scoreValue;
                        }
                        if(scoreType.equalsIgnoreCase("XCorr")){
                            XCorr = scoreValue;
                        }
                        if(scoreType.equalsIgnoreCase("ProbabilityScore")){
                            ProbabilityScore = scoreValue;
                        }
                        if(scoreType.equalsIgnoreCase("IonScore")){
                            IonScore = scoreValue;
                        }
                        //if(scoreType.equalsIgnoreCase("XCorr")){
                        //    XCorr = scoreValue;
                        //}
                    }
                }
                if(scoreMap.containsKey("SpScore")){
                        //msfPep.setScore("SpScore",scoreMap,pscoreMap);
                        scoreNameToValueMap.put("spscore", SpScore);
                }
                if(scoreMap.containsKey("XCorr")){
                    //msfPep.setScore("XCorr",scoreMap,pscoreMap);
                    scoreNameToValueMap.put("xcorr",XCorr);
                }
                if(scoreMap.containsKey("ProbabilityScore")){
                    //msfPep.setScore("ProbabilityScore",scoreMap,pscoreMap);
                    scoreNameToValueMap.put("probabilityscore", ProbabilityScore);
                }
                if(scoreMap.containsKey("IonScore")){
                    //msfPep.setScore("IonScore",scoreMap,pscoreMap);
                    scoreNameToValueMap.put("ionscore",IonScore);
                }

            } //else { // implies perhaps there isn't a peptide to scores mapped in the HashMap object, also possible that the map explicitly maps the key to null
            //    SpScore = 0;
            //    XCorr = 0;
            //}
        } catch (NullPointerException ex){
            ex.getCause();
            ex.printStackTrace();
        }
    }
    
    public void setDeltaCn(double deltaCn){
        DeltaCn = deltaCn;
    }

    public void setDeltaCnStar(double deltaCnStar) {
        this.DeltaCnStar = deltaCnStar;
    }

    public void setSpRank(int spRank) {
        this.SpRank = spRank;
    }

    public void setEValue(double eVal) {
        this.expectValue = eVal;
    }
    
       
    public boolean equals(MSFPSMPeptide msfPep){
        boolean isEqual = false;
        if(this.getSequence().equals(msfPep.getSequence()) && 
                this.toStringPepModifications().equals(msfPep.toStringPepModifications())){
            isEqual = true;
        }
        return isEqual;
    }
    
    public double getExperimetalMassToCharge(){
        double mz = 0;
        //mz = [mass + massOfcharge(charge)]/charge
        mz =  (mz + (1.007825 * ((double) Charge)))/(double) Charge;
        return mz;
    }
    
}
