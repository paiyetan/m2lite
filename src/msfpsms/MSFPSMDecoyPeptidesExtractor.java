/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 * 
 * Note that in the DecoyPeptideExtractor contrary to regular PeptideExtractor, there is no 
 * quantification retrieval as this was not found to be reported/mapped with decoy assignments 
 * in the MSF tables.
 * 
 */
package msfpsms;

import ios.readers.MSFRoutPSMPeptidesFileReader;
import java.io.File;
import java.sql.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import msfparser.M2Lite;
import msfparser.MSFDecoyModificationsExtractor;
import rconnect.RCaller;

/**
 *
 * @author paiyeta1
 */
public class MSFPSMDecoyPeptidesExtractor {
    
    private MSFPSMPeptide[] decoyMSFPeps;
    private HashMap<String,Integer> decoyScoreMap;
    //Map peptide to corresponding scores 
    private HashMap<Integer,LinkedList<MSFPSMPeptideScore>> decoyPscoreMap;
    
    @SuppressWarnings("CallToThreadDumpStack")
    public MSFPSMDecoyPeptidesExtractor(String msfFile, String rQueryScriptLibrary){
        
        //decoyMSFPeps = new ArrayList<MSFPSMPeptide>();
        decoyScoreMap = new HashMap<String,Integer>();
        decoyPscoreMap = new HashMap<Integer,LinkedList<MSFPSMPeptideScore>>();
    
        //create a tmp in present working directory
        String homeDir = System.getProperty("user.home");
        String tmp = homeDir + File.separator + "tmp";
        
        if(!new File(tmp).exists()){
            new File(tmp).mkdirs();
        }
        
        //String rFilePath = "C:/Users/paiyeta1/Libraries/R/local/msfParser_RLibs/dBQuery.R";
        String rFilePath = rQueryScriptLibrary + File.separator + "MSFPSMDecoyPeptidesDBQuery.R";
        RCaller rcaller = new RCaller(); 
    //1. Get msf Peptides
        System.out.println("\tQuerying file for PSM assigned decoy peptides' properties...");    
        //make output file
        String msfRoutPeptidesOutputFile = tmp + File.separator + "msfRoutPeptides_decoy";
        //make Query string - delegated to R ....
        //make command
        String rcmd = "Rscript " + rFilePath + " " + msfFile + " " + msfRoutPeptidesOutputFile;
        //execute R
        rcaller.execute(rcmd);
        //read back
        MSFRoutPSMPeptidesFileReader reader = new MSFRoutPSMPeptidesFileReader();
        decoyMSFPeps = reader.read(msfRoutPeptidesOutputFile);
        
        //new File(msfRoutPeptidesOutputFile).delete();
        setMSFPepDecoyStatus();
            
        // *************************************************************************** //
        // retrieve modification attributes/properties
        MSFDecoyModificationsExtractor modExtr = new MSFDecoyModificationsExtractor(msfFile,tmp,rQueryScriptLibrary);
        //A. retrieve Non-Terminal modifications attributes/properties
        HashMap<Integer,LinkedList<MSFPSMModification>> decoyPepID2ModificationsMap = 
                modExtr.getDecoyPeptideID2ModificationsMap();
        //B. retrieve terminal modifications attributes/properties
        HashMap<Integer,LinkedList<MSFPSMModification>> decoyPepID2TermModificationMap = 
                modExtr.getDecoyPeptideID2TermModificationMap();

        // ********************************************************* //
        // Get Scores - XCorr and SpScore +/- Probability
        
        Connection connection = null;  
        Statement statement = null; 
        ResultSet resultSet = null;
        try {  
            Class.forName("org.sqlite.JDBC"); //Deprecated in recent JDBC implementation
            System.out.println("\tEstablishing SQLite DB connection to file...");    
            connection = DriverManager.getConnection("jdbc:sqlite:" + msfFile);  
            statement = connection.createStatement();
            System.out.println("\tQuerying file for Scores identifiers...");    
            String query = "SELECT " +
                            "ScoreID," +
                            "ScoreName " + 
                            "FROM ProcessingNodeScores;";
            resultSet = statement.executeQuery(query);
            // Make map of ScoreID to corresponding ScoreName
            System.out.println("\tRetrieving Scores identifiers' results..."); 
            int noOfResultsRetrieved = 0;
            while(resultSet.next()){
                noOfResultsRetrieved++;
                int ScoreID = resultSet.getInt("ScoreID");
                String ScoreName = resultSet.getString("ScoreName");
                decoyScoreMap.put(ScoreName,ScoreID);                   
            }
            System.out.println("\t\t" + noOfResultsRetrieved + " Scores identifiers' results retrieved...");
            statement.close();
            resultSet.close(); 

            // 5b. Get peptides with correspoding scores
            statement = connection.createStatement();
            System.out.println("\tQuerying file for Peptide_decoy Scores...");    
            query = "SELECT " +
                            "PeptideID," +
                            "ScoreID, " + 
                            "ScoreValue " +
                            "FROM PeptideScores_decoy;";
            resultSet = statement.executeQuery(query);

            System.out.println("\tRetrieving Peptide Scores' results..."); 
            noOfResultsRetrieved = 0;
            int noOfPeptide2ScoresMapped = 0;
            while(resultSet.next()){
                noOfResultsRetrieved++;
                int PeptideID = resultSet.getInt("PeptideID");
                int ScoreID = resultSet.getInt("ScoreID");
                double ScoreValue = resultSet.getDouble("ScoreValue");
                if(decoyPscoreMap.containsKey(PeptideID)==false){
                    LinkedList<MSFPSMPeptideScore> scoreList = new LinkedList<MSFPSMPeptideScore>();
                    scoreList.add(new MSFPSMPeptideScore(ScoreID,ScoreValue));        
                    decoyPscoreMap.put(PeptideID,scoreList);
                    noOfPeptide2ScoresMapped++;
                    
                }else{
                    LinkedList<MSFPSMPeptideScore> scoreList = decoyPscoreMap.remove(PeptideID);
                    scoreList.add(new MSFPSMPeptideScore(ScoreID,ScoreValue));
                    decoyPscoreMap.put(PeptideID,scoreList);
                    //noOfPeptide2ScoresMapped++;                    
                }
            }
            System.out.println("\t\t" + noOfPeptide2ScoresMapped + " PeptideID to Scores' mapped...");
            System.out.println("\t\t" + noOfResultsRetrieved + " Peptide Scores' results retrieved...");
            statement.close();
            resultSet.close(); 



            // ****************************************************************************
            //6. Set missing Attributes of MSFPSMPeptides
            // Update MSFPeptides's properties in ArrayList
            //Iterator<MSFPSMPeptide> itr = decoyMSFPeps.iterator();
            System.out.println("\tUpdating peptides results' properties...");
            
            for(int i = 0; i < decoyMSFPeps.length; i ++){
            //while (itr.hasNext()){

                //MSFPSMPeptide msfPep = (MSFPSMPeptide) itr.next();
                MSFPSMPeptide msfPep = decoyMSFPeps[i];
                int msfPepID = msfPep.getPeptideID();
                // update Modification Attributes
                msfPep.setModifications(extractPepModfxns(msfPepID,decoyPepID2ModificationsMap,decoyPepID2TermModificationMap));
                // set Scores
                if(decoyScoreMap.containsKey("SpScore"))
                    msfPep.setScore("SpScore",decoyScoreMap,decoyPscoreMap); //include spScore if if available in map
                if(decoyScoreMap.containsKey("XCorr"))
                    msfPep.setScore("XCorr",decoyScoreMap,decoyPscoreMap); //include XCorr if available in map...
                if(decoyScoreMap.containsKey("ProbabilityScore"))
                        msfPep.setScore("ProbabilityScore",decoyScoreMap,decoyPscoreMap);
                if(decoyScoreMap.containsKey("IonScore"))
                        msfPep.setScore("IonScore",decoyScoreMap,decoyPscoreMap);
            }
            
        } catch (Exception e) { 
            e.printStackTrace();
        }  
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(M2Lite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //new File(tmp).deleteOnExit(); //delete tmp directory;

        
    }
    
    private LinkedList<MSFPSMModification> extractPepModfxns(int msfPepID, 
                                HashMap<Integer, LinkedList<MSFPSMModification>> peptideID2ModificationsMap, 
                                    HashMap<Integer, LinkedList<MSFPSMModification>> peptideID2TermModificationMap) {
        //throw new UnsupportedOperationException("Not yet implemented");
        LinkedList<MSFPSMModification> modifications = new LinkedList<MSFPSMModification>();
        if (peptideID2TermModificationMap.containsKey(msfPepID)){
            LinkedList<MSFPSMModification> mappedTermModfxn = peptideID2TermModificationMap.get(msfPepID);
            Iterator<MSFPSMModification> itr = mappedTermModfxn.iterator();
            while(itr.hasNext()){
                MSFPSMModification termModfxn = itr.next();
                modifications.add(termModfxn);
            }
        }       
        if (peptideID2ModificationsMap.containsKey(msfPepID)){
            LinkedList<MSFPSMModification> mappedModfxn = peptideID2ModificationsMap.get(msfPepID);
            Iterator<MSFPSMModification> itr2 = mappedModfxn.iterator();
            while(itr2.hasNext()){
                MSFPSMModification modfxn = itr2.next();
                modifications.add(modfxn);
            }
        }
        return modifications;
    }

    public MSFPSMPeptide[] getDecoyMSFPeps() {
        return decoyMSFPeps;
    }

    private void setMSFPepDecoyStatus() {
        //Iterator<MSFPSMPeptide> itr = decoyMSFPeps.iterator();
        //while(itr.hasNext()){
        for(MSFPSMPeptide decoyPep : decoyMSFPeps){
            //MSFPSMPeptide decoyPep = itr.next();
            decoyPep.setDecoy(true);
        }
    }
    

    
}
