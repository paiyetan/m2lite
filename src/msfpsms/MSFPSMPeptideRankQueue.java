/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfpsms;

import java.util.LinkedList;

/**
 *
 * @author paiyeta1
 */
public class MSFPSMPeptideRankQueue {
    
    private MSFPSMPeptide[] priorityQueue;
    private int size;

    public MSFPSMPeptideRankQueue(LinkedList<MSFPSMPeptide> mappedPepList) {
        
        priorityQueue = new MSFPSMPeptide[mappedPepList.size()];
        for(int i = 0; i < priorityQueue.length; i++){
            priorityQueue[i] = mappedPepList.get(i);
        }
        prioritize(true);
        size = priorityQueue.length; // starting size of queue 
        //empty = false;
    }
    
    
    private void prioritize(boolean verbose) {
        // throw new UnsupportedOperationException("Not yet implemented");
        for(int i = 0; i < priorityQueue.length - 1; i++){
            if(priorityQueue[i+1].getSearchEngineRank() < priorityQueue[i].getSearchEngineRank()){
                MSFPSMPeptide tmp = priorityQueue[i];
                priorityQueue[i] = priorityQueue[i+1];
                priorityQueue[i+1] = tmp;
                for(int j = i; j > 0; j--){
                    if(priorityQueue[j].getSearchEngineRank() < priorityQueue[j-1].getSearchEngineRank()){
                        MSFPSMPeptide tmp2 = priorityQueue[j-1];
                        priorityQueue[j-1] = priorityQueue[j];
                        priorityQueue[j] = tmp2;
                    }
                }
            }
            
        }
        if(verbose){
            // *** Debugging code
            System.out.println("   MSFPSMPeptide(s) prioritized: ");
            for(int i = 0; i < priorityQueue.length; i++){
                System.out.println("    " + priorityQueue[i].getSequence() + ", " + 
                                        (priorityQueue[i].isDecoy()==false) + " hit, " +
                                        priorityQueue[i].getSearchEngineRank());
            }
        }
        
    }
    
    
    public void add(MSFPSMPeptide qcFObj){
        //Inserts the specified element into this priority queue
        MSFPSMPeptide[] updatedPriorityQueue = new MSFPSMPeptide[priorityQueue.length + 1];
        System.arraycopy(priorityQueue, 0, updatedPriorityQueue, 0, priorityQueue.length);
        updatedPriorityQueue[updatedPriorityQueue.length] = qcFObj;
        priorityQueue = updatedPriorityQueue;
        prioritize(false);
        size++;
    }
    
    public MSFPSMPeptide peek(){
        // Retrieves, but does not remove, the head of this queue, or returns null if this queue is empty.
        MSFPSMPeptide qcFObj = priorityQueue[0];
        return qcFObj;
    }
    
    public MSFPSMPeptide poll(){
        // Retrieves and removes the head of this queue, or returns null if this queue is empty.
        MSFPSMPeptide qcFObj = priorityQueue[0];
        // update queue
        MSFPSMPeptide[] updatedPriorityQueue = new MSFPSMPeptide[priorityQueue.length - 1];
        System.arraycopy(priorityQueue, 1, updatedPriorityQueue, 0, updatedPriorityQueue.length);
        priorityQueue = updatedPriorityQueue;
        size--;
        return qcFObj;
        
    }
    
    /*
     * 
     * 
    public void remove(){
        // Removes a single instance of the specified element from this queue, if it is present.
        
    }
    
    public int size(){
        // Returns the number of elements in this collection.
        return size;
    }
    
    public boolean contains(){
        // Returns true if this queue contains the specified element.
        boolean contain = false;
        
        return contain;
    }
    
    public boolean offer(QuanCountFileObject qcFObj){
        // Inserts the specified element into this priority queue.
        
        return true;
    }
    * 
    */
    
    public boolean isEmpty(){
        boolean empty;
        if(size > 0){
            empty = false;
        } else{
            empty = true;
        }
        return empty;
    }

    public MSFPSMPeptide[] getPriorityQueue() {
        return priorityQueue;
    }
    
    
    
    
    
}
