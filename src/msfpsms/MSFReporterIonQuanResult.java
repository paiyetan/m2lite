/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfpsms;

/**
 *
 * @author paiyeta1
 */
public class MSFReporterIonQuanResult {
    
    private int ProcessingNodeNumber;
    private int QuanChannelID;
    private int SpectrumID;
    private double Mass;
    private double Height;
    private int SearchSpectrumID;

    public MSFReporterIonQuanResult(int ProcessingNodeNumber, int QuanChannelID, int SpectrumID, 
                                        double Mass, double Height, int SearchSpectrumID) {
        this.ProcessingNodeNumber = ProcessingNodeNumber;
        this.QuanChannelID = QuanChannelID;
        this.SpectrumID = SpectrumID;
        this.Mass = Mass;
        this.Height = Height;
        this.SearchSpectrumID = SearchSpectrumID;
    }

    public double getHeight() {
        return Height;
    }

    public double getMass() {
        return Mass;
    }

    public int getProcessingNodeNumber() {
        return ProcessingNodeNumber;
    }

    public int getQuanChannelID() {
        return QuanChannelID;
    }

    public int getSpectrumID() {
        return SpectrumID;
    }

    public int getSearchSpectrumID() {
        return SearchSpectrumID;
    }
    
}
