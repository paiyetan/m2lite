/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfproteins;

import enumtypes.SearchDatabaseType;
import ios.readers.MSFRoutPSMProteinsFileReader;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import rconnect.RCaller;

/**
 *
 * @author paiyeta1
 */
public class MSFPSMProteinsExtractor {
    
    private MSFPSMProtein[] psmProteins;
    private SearchDatabaseType dbtype;
    private ArrayList<Integer> groupIDs;
    
                
    public MSFPSMProteinsExtractor(String msfFile, SearchDatabaseType dbtype, String rQueryScriptLibrary){
        //psmProteins = new ArrayList<MSFPSMProtein>();
        this.dbtype = dbtype;
        //proteinGrpID2ProteinGrpAccMap = 
        
        /*
         *  - make output file
            - make Query string
            - make command (rcmd)
            - execute R
                rcaller.execute(rcmd);
            - read back
         * 
         */
        
         // create a tmp in present working directory for file swaps
        String homeDir = System.getProperty("user.home");
        String tmp = homeDir + File.separator + "tmp";
        
        if(!new File(tmp).exists()){
            new File(tmp).mkdirs();
        }
        // make R output file(s)
        String msfRoutPSMProteinsFile = tmp + File.separator + "msfRoutPSMProteins";
        // make sql query string
        // make rcommands
        //String rFilePath = "C:/Users/paiyeta1/Libraries/R/local/msfParser_RLibs/dBQuery.R";
        String rFilePath = rQueryScriptLibrary + File.separator + "MSFPSMProteinsDBQuery.R";
        RCaller rcaller = new RCaller();
        //make command
        String rcmd = "Rscript " + rFilePath + " " + msfFile + " " + msfRoutPSMProteinsFile;
        //execute R
        System.out.println("\tQuerying file for PSM assigned peptides proteins' properties...");    
        rcaller.execute(rcmd);
        //read back
        MSFRoutPSMProteinsFileReader reader = new MSFRoutPSMProteinsFileReader();
        System.out.println("\tRetrieving PSM assigned peptides proteins' results..."); 
        psmProteins = reader.read(msfRoutPSMProteinsFile,dbtype); //i. populate into Protein ArrayList +/- a HashMap that maps Peptides (identified peptides) to psmProteins
         
        //delete tmp msfRoutPSMProteinsFile file
        //new File(msfRoutPSMProteinsFile).delete();
        
        
        //System.out.println("\tRetrieving protein groups...");
        //setProteinGroupIDs();
        //System.out.println("\t" + groupIDs.size() + " protein groups found"); 
        
        
        //new File(tmp).deleteOnExit(); //delete tmp directory;
    }
    
    
    public HashMap<Integer, LinkedList<MSFPSMProtein>> getPeptideID2ProteinsMap() {
        HashMap<Integer, LinkedList<MSFPSMProtein>> peptideID2ProteinsMap = 
                new HashMap<Integer, LinkedList<MSFPSMProtein>>();
        //Iterator<MSFPSMProtein> itr = psmProteins.iterator();
        //while(itr.hasNext()){
        for(MSFPSMProtein protein : psmProteins){
            //MSFPSMProtein protein = itr.next();
            int PeptideID = protein.getMappedPeptideID();
            if(peptideID2ProteinsMap.containsKey(PeptideID)){
                // retrieve/remove the mapping - retrieve the peptideProteins LinkedList,
                LinkedList<MSFPSMProtein> mappedPeptideProteins = peptideID2ProteinsMap.remove(PeptideID);
                if(mappedPeptideProteins.contains(protein)==false){
                    mappedPeptideProteins.add(protein);
                }
                // reinsert mapping with updated mappedPeptideProteins
                peptideID2ProteinsMap.put(PeptideID, mappedPeptideProteins);                       
            } else {
                LinkedList<MSFPSMProtein> peptideProteins = new LinkedList<MSFPSMProtein>();
                peptideProteins.add(protein);
                peptideID2ProteinsMap.put(PeptideID, peptideProteins);
                        
            }    
        }       
        return peptideID2ProteinsMap;
    }
    
    //PD associated grouping//
    // ---- this is deprecated as it is assumed to be of less importance if downstream 
    // non-Proteome Discoverer peptide analysis is considered 
    
    /*
     *     
    private void setProteinGroupIDs(){
        groupIDs = new ArrayList<Integer>();
        //Iterator<MSFPSMProtein> itr = psmProteins.iterator();
        //while(itr.hasNext()){
        for(MSFPSMProtein protein : psmProteins){
            //MSFPSMProtein protein = itr.next();
            if(protein.isMasterProtein() && groupIDs.contains(protein.getProteinGroupID())==false)
                groupIDs.add(protein.getProteinGroupID());           
        }       
    }
    * 
    */
    
    /*
    //get proteinGrpID2ProteinGrpAccMap
    public HashMap<Integer,LinkedList<MSFPSMProtein>> getProteinGrpID2MSFProteinsMap(){
        HashMap<Integer,LinkedList<MSFPSMProtein>> proteinGrpID2MSFProteinsMap = 
                new HashMap<Integer,LinkedList<MSFPSMProtein>>();
        //GetProtein GroupIDs
        //for each unique groupID, get mapped proteins and insert in map
        Iterator<Integer> itr2 = groupIDs.iterator();
        while(itr2.hasNext()){
            int groupID = itr2.next();
            LinkedList<MSFPSMProtein> mappedProteins = getProtGroupIDMappedProteins(groupID);
            proteinGrpID2MSFProteinsMap.put(groupID, mappedProteins);           
        }       
        return proteinGrpID2MSFProteinsMap;       
    }
    
    private LinkedList<MSFPSMProtein> getProtGroupIDMappedProteins(int groupID) {
        //throw new UnsupportedOperationException("Not yet implemented");
        LinkedList<MSFPSMProtein> mappedProteins = new LinkedList<MSFPSMProtein>();
        //Iterator<MSFPSMProtein> itr = psmProteins.iterator();
        //while(itr.hasNext()){
        for(MSFPSMProtein protein : psmProteins){
            //MSFPSMProtein protein = itr.next();
            if(groupID == protein.getProteinGroupID()){
                mappedProteins.add(protein); 
            }
        }
        return mappedProteins;
    }
    
    public ArrayList<MSFPSMProtein> getGroupProteins(){
        ArrayList<MSFPSMProtein> grpProts = new ArrayList<MSFPSMProtein>();
        //Iterator<MSFPSMProtein> itr = psmProteins.iterator();
        //while(itr.hasNext()){
        for(MSFPSMProtein protein : psmProteins){
            //MSFPSMProtein protein = itr.next();
            if(protein.isMasterProtein()){
                grpProts.add(protein);
            }
        }
        return grpProts;
    }
    * 
    */
    
    public MSFPSMProtein[] getProteins() {
        return psmProteins;
    }

    public SearchDatabaseType getDbtype() {
        return dbtype;
    }

    public ArrayList<Integer> getGroupIDs() {
        return groupIDs;
    }        
}
    

