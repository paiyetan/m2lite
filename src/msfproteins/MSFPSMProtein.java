/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 * 
 *         Copyright (c) 2013, 
        Paul Aiyetan
        Department of Pathology, 
        Johns Hopkins University,
        Baltimore, MD 21231
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions are met:
         
            * Redistributions of source code must retain the above copyright
                notice, this list of conditions and the following disclaimer.
            * Redistributions in binary form must reproduce the above copyright
                notice, this list of conditions and the following disclaimer in the
                documentation and/or other materials provided with the distribution.
            * Neither the name of the Johns Hopkins University nor the
                names of its contributors may be used to endorse or promote products
                derived from this software without specific prior written permission.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
        WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
        DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
        DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
        (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
        LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
        ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
        (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
        SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 
 */
package msfproteins;

import database.Peptide;
import enumtypes.SearchDatabaseType;

/**
 *
 * @author paiyeta1
 */
public class MSFPSMProtein {
    
    private int ProteinID;
    private String Description;
    private String Sequence; //may not need the sequence or perhaps may for comparison
    private int ProteinGroupID;
    private boolean IsMasterProtein;
    private double ProteinScore;
    private SearchDatabaseType dbtype;
    private String mappedPeptideSequence;
    private int mappedPeptideID;

    public MSFPSMProtein(int ProteinID, String Description, String Sequence, 
            int ProteinGroupID, int IsMasterProtein) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.Sequence = Sequence;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
    }

    public MSFPSMProtein(int ProteinID, String Description, int ProteinGroupID, 
            int IsMasterProtein) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
    }

    public MSFPSMProtein(int ProteinID, String Description, int ProteinGroupID, 
            int IsMasterProtein, double ProteinScore) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
    }

    public MSFPSMProtein(int ProteinID, String Description, String Sequence, 
            int ProteinGroupID, int IsMasterProtein, double ProteinScore) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.Sequence = Sequence;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
    }

    public MSFPSMProtein(int ProteinID, String Description, int ProteinGroupID, 
            int IsMasterProtein, double ProteinScore, SearchDatabaseType dbtype) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
        this.dbtype = dbtype;
    }

    public MSFPSMProtein(int ProteinID, String Description, String Sequence, 
            int ProteinGroupID, int IsMasterProtein, double ProteinScore, SearchDatabaseType dbtype) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.Sequence = Sequence;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
        this.dbtype = dbtype;
    }

    public MSFPSMProtein(int ProteinID, String Description, int ProteinGroupID, int IsMasterProtein, double ProteinScore, SearchDatabaseType dbtype, String mappedPeptideSequence) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
        this.dbtype = dbtype;
        this.mappedPeptideSequence = mappedPeptideSequence;
    }

    public MSFPSMProtein(int ProteinID, String Description, String Sequence, int ProteinGroupID, int IsMasterProtein, double ProteinScore, SearchDatabaseType dbtype, String mappedPeptideSequence) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.Sequence = Sequence;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
        this.dbtype = dbtype;
        this.mappedPeptideSequence = mappedPeptideSequence;
    }

    public MSFPSMProtein(int ProteinID, String Description, int ProteinGroupID, int IsMasterProtein, 
            double ProteinScore, SearchDatabaseType dbtype, String mappedPeptideSequence, int mappedPeptideID) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.ProteinGroupID = ProteinGroupID;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
        this.dbtype = dbtype;
        this.mappedPeptideSequence = mappedPeptideSequence;
        this.mappedPeptideID = mappedPeptideID;
    }
    
    public MSFPSMProtein(int ProteinID, String Description, int ProteinGroupID, String Sequence, int IsMasterProtein, 
            double ProteinScore, SearchDatabaseType dbtype, String mappedPeptideSequence, int mappedPeptideID) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.ProteinGroupID = ProteinGroupID;
        this.Sequence = Sequence;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
        this.dbtype = dbtype;
        this.mappedPeptideSequence = mappedPeptideSequence;
        this.mappedPeptideID = mappedPeptideID;
    }

    public MSFPSMProtein(int ProteinID, String Description, String Sequence, int IsMasterProtein, double ProteinScore, SearchDatabaseType dbtype, String mappedPeptideSequence, int mappedPeptideID) {
        this.ProteinID = ProteinID;
        this.Description = Description;
        this.Sequence = Sequence;
        setIsMasterProtein(IsMasterProtein);
        this.ProteinScore = ProteinScore;
        this.dbtype = dbtype;
        this.mappedPeptideSequence = mappedPeptideSequence;
        this.mappedPeptideID = mappedPeptideID;
    }
    
    
        
    private void setIsMasterProtein(int IsMasterProtein) {
        //throw new UnsupportedOperationException("Not yet implemented");
        if (IsMasterProtein==1){
            this.IsMasterProtein = true;          
        } else {
            this.IsMasterProtein = false;
        }
    }

    public String getDescription() {
        return Description;
    }

    public boolean isMasterProtein() {
        return IsMasterProtein;
    }

    public int getProteinGroupID() {
        return ProteinGroupID;
    }

    public int getProteinID() {
        return ProteinID;
    }

    public String getSequence() {
        return Sequence;
    }
    
    public String getAccession(){ //derived from the Description which in actual sense is the fastA title header
        String acc = "";
        String[] headerArr = this.Description.split("\\|");
        
        switch(dbtype){
            case IPIv387:  
                acc = headerArr[0].replace(">",""); 
                break;
            case REFSEQ:
                String gi = headerArr[0];
                gi = gi.replaceAll(">","");
                acc = gi + "|" + headerArr[1];
                break;
            case UNIPRO:
                headerArr = Description.split("\\s");
                String sp = headerArr[0];
                acc = sp.replaceAll(">","");;
                break;
            default:
                acc = this.Description;
        }
        return(acc);
    }
    
    public String getCleanDescription(){
        String cleanDescription = "";
        switch(dbtype){
            
            case IPIv387:  
                //cleanDescription = headerArr[0]; 
                String title = null;
                char[] c = Description.toCharArray();
                for(int i = 0; i < c.length-8; i++){
                    if(Description.substring(i,i+6).matches("Tax.+")){
                        title = Description.substring(i);
                        break;
                    } 
                }
                //System.out.println(title+"\n");
                String[] titleArr = title.split("\\s");
                String name = titleArr[2];
                for(int i = 3; i < titleArr.length; i++){
                    name = name + " " + titleArr[i];
                }
                cleanDescription = name;
                break;
            
            case REFSEQ:
                String[] headerArr = this.Description.split("\\|");
                cleanDescription = headerArr[headerArr.length-1];
                break;
            
            case UNIPRO:
                String[] headerArr2 = this.Description.split("\\s");
                cleanDescription = Description.replace(headerArr2[0],"");
                break;
            
            default:
                cleanDescription = this.Description;
                break;
        }
        return cleanDescription;
    }
    
    public Peptide getPeptideInstance(String peptideSequence){
        Peptide peptide = null;
        // ensure peptide sequence is found in protein sequence
        if(Sequence.contains(peptideSequence)){
            int startCharIndex = Sequence.indexOf(peptideSequence);
            int endCharIndex = startCharIndex + (peptideSequence.length()-1);
            char nTermNeighbor;
            char cTermNeighbor;
            String seqWithNeighbors;
            // get nTerminal neighbor 
            if(startCharIndex > 0){
                nTermNeighbor = Sequence.charAt(startCharIndex - 1);
            } else {
                nTermNeighbor = '-';
            }
            // get cTerminal neighbor
            if(endCharIndex < (Sequence.length()-1)){
                cTermNeighbor = Sequence.charAt(endCharIndex + 1);
            } else {
                cTermNeighbor = '-';
            }
            // assign sequence with neighbors
            seqWithNeighbors = nTermNeighbor + "." + peptideSequence + "." + cTermNeighbor; 
            peptide = new Peptide(peptideSequence, this, startCharIndex + 1, 
                                        endCharIndex + 1, seqWithNeighbors);
        }
        return peptide;
    }
    

    public double getProteinScore() {
        return ProteinScore;
    }

    public SearchDatabaseType getDbtype() {
        return dbtype;
    }
    
    public String getMappedPeptideSequence() {
        return mappedPeptideSequence;
    }

    public int getMappedPeptideID() {
        return mappedPeptideID;
    }
    
    public boolean equals(MSFPSMProtein prot){
        boolean equal = false;
        if(prot.getProteinID() == this.ProteinID){
            equal = true;
        }
        return equal;
    }
    
    public void reverseSequence(){
        char[] reverseStringArray = new char[Sequence.length()];
        for(int i = Sequence.length() - 1, j = 0; i != -1; i--, j++ ){
            reverseStringArray[j] = Sequence.charAt(i);
        }
        Sequence = new String(reverseStringArray);       
    }

    public void appendRev() {
        Description = "rev_" + Description.replace(">", "");
    }

    public String getMyriMatchDescription(){ 
        // a myri-match pepXML description format
        // only for RefSeq acc at the moment
        String desc = "";
        String description = Description.replace(">", "");
        desc = description.substring(0, description.lastIndexOf("|")+1);
        return desc;       
    }
    
    public String getTPPDescription(){ 
        // a TPP pepXML description format
        // only for RefSeq acc at the moment
        String desc = "";
        String description = Description.replace(">", "");
        //desc = Description.substring(0, Description.lastIndexOf("|")+1);
        String[] descArr = description.split("\\s");
        return descArr[0];       
    }
    
}
